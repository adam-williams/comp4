#!/bin/bash
echo "Starting development server tools..."
php artisan serve --host=0.0.0.0 &
sass --watch resources/sass/:public/assets/css/ &
tsc -w --out public/assets/js/app.js resources/typescript/main.ts
echo "Cleaning up..."
