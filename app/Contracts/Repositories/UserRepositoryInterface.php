<?php
namespace AdamWilliams\DMLT\Contracts\Repositories;
use AdamWilliams\DMLT\Eloquent\Entities\User;

/**
 * Contract that allows controllers to lookup users.
 * @package AdamWilliams\DMLT\Contracts\Repositories
 */
interface UserRepositoryInterface {
    /**
     * @param int $id The user's id.
     * @return User|null
     */
    public function getUserById($id);
}