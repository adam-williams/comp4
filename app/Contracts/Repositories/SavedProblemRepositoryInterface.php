<?php
namespace AdamWilliams\DMLT\Contracts\Repositories;

/**
 * Contract that allows controllers to lookup saved problems.
 * @package AdamWilliams\DMLT\Contracts\Repositories
 */
interface SavedProblemRepositoryInterface {
    public function createSavedProblem($guid, $data);
}
