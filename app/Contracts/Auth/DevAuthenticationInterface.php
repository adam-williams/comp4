<?php
namespace AdamWilliams\DMLT\Contracts\Auth;

/**
 * Interface to be implemented by the session-based auth implementation class.
 * @package AdamWilliams\DMLT\Http\Auth
 */
interface DevAuthenticationInterface {
    public function isAuthenticated();
    public function setAuthenticated($authed=true);
} 