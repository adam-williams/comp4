<?php
namespace AdamWilliams\DMLT\Eloquent\Repositories;

use AdamWilliams\DMLT\Contracts\Repositories\UserRepositoryInterface;
use AdamWilliams\DMLT\Eloquent\Entities\User;

/**
 * Eloquent implementation of UserRepositoryInterface.
 * @package AdamWilliams\DMLT\Eloquent\Repositories
 */
class EloquentUserRepository implements UserRepositoryInterface {

    /**
     * @param int $id The user's id./
     * @return User|null
     */
    public function getUserById($id) {
        return User::whereId($id)->first();
    }
}