<?php namespace AdamWilliams\DMLT\Eloquent\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder;

/**
 * \AdamWilliams\DMLT\Eloquent\Entities\Statistic
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\$related[] $morphedByMany
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $key
 * @property integer $value
 * @method static Builder|Statistic whereId($value)
 * @method static Builder|Statistic whereCreatedAt($value)
 * @method static Builder|Statistic whereUpdatedAt($value)
 * @method static Builder|Statistic whereKey($value)
 * @method static Builder|Statistic whereValue($value)
 */
class Statistic extends Model {
    public static $DEFINED_TYPES = ["problems_processed", "problems_failed", "iterations_performed", "constraints_converted", "tableaux_created"];
    public static $PROBLEMS_PROCESSED = "problems_processed";
    public static $PROBLEMS_FAILED = "problems_failed";
    public static $ITERATIONS_PERFORMED = "iterations_performed";
    public static $CONSTRAINTS_CONVERTED = "constraints_converted";
    public static $TABLEAUX_CREATED = "tableaux_created";

    public static function incrementStatistic($type, $num=1) {
        $entry = Statistic::whereKey($type)->first();
        if ($entry === null) {
            throw new ModelNotFoundException("No such statistic");
        } else {
            $entry->value = $entry->value + $num;
        }
        $entry->save();
        return $entry->value;
    }
}