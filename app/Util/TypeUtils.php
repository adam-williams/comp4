<?php


namespace AdamWilliams\DMLT\Util;


use AdamWilliams\DMLT\Exceptions\InvalidTypeException;

class TypeUtils {

    /**
     * Helper function to ensure all items in an array are of a type.
     * @param array $array The array of items to check.
     * @param string $type The **fully-qualified** expected type.
     * @throws InvalidTypeException If an item is not of the expected type.
     */
    public static function checkTypeOfArray(array $array, $type) {
        foreach ($array as $obj) {
            if (get_class($obj) !== $type) {
                throw new InvalidTypeException($type, get_class($obj));
            }
        }
    }

    /**
     * Helper function to ensure a variable is of a type.
     * @param object $item The object to check
     * @param string $type The **fully-qualified** expected type.
     * @throws InvalidTypeException
     * @internal param array $array The array of items to check.
     */
    public static function checkTypeOfVariable($item, $type) {
        if ($item === null || get_class($item) !== $type) {
            throw new InvalidTypeException($type, get_class($item));
        }
    }

} 