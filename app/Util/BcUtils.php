<?php


namespace AdamWilliams\DMLT\Util;


class BcUtils {

    /**
     * @author http://uk3.php.net/manual/en/function.bcscale.php#114796
     * @param string $number The number to round off.
     * @param int $scale The number of places to round it to.
     * @return string The new value.
     */
    public static function bcRound($number, $scale = 0) {
        $sign = '';
        if ($scale < 0) {
            $scale = 0;
        }
        if (bccomp('0', $number, 64) == 1) {
            $sign = '-';
        }
        $increment = $sign . '0.' . str_repeat('0', $scale) . '5';
        $number = bcadd($number, $increment, $scale + 1);
        return bcadd($number, '0', $scale);
    }

    /**
     * @author http://uk3.php.net/manual/en/function.bcscale.php#107259
     * @param string $input The input to strip extraneous zeroes from.
     * @return string The new number.
     */
    public static function bcRemoveTrailingZeroes($input) {
        $patterns = array('/[\.][0]+$/', '/([\.][0-9]*[1-9])([0]*)$/');
        $replaces = array('', '$1');
        return preg_replace($patterns, $replaces, $input);
    }

    /**
     * @author http://stackoverflow.com/a/1653826
     * @param string $number The number to apply the ceil function to.
     * @return string The number rounded up.
     */
    public static function bcCeil($number) {
        if (strpos($number, '.') !== false) {
            if (preg_match("~\\.[0]+$~", $number)) return self::bcRound($number, 0);
            if ($number[0] != '-') return bcadd($number, 1, 0);
            return bcsub($number, 0, 0);
        }
        return $number;
    }

    /**
     * @author http://stackoverflow.com/a/1653826
     * @param string $number The number to apply the floor function to.
     * @return string The number rounded down.
     */
    public static function bcFloor($number) {
        if (strpos($number, '.') !== false) {
            if (preg_match("~\\.[0]+$~", $number)) return self::bcRound($number, 0);
            if ($number[0] != '-') return bcadd($number, 0, 0);
            return bcsub($number, 1, 0);
        }
        return $number;
    }
} 