<?php


namespace AdamWilliams\DMLT\Util;


class DateUtil {

    /**
     * @param int $time The time in seconds.
     * @return string Human-readable time elapsed.
     * @author http://stackoverflow.com/a/15551771 (aram90)
     */
    public static function getTimeElapsed($time) {
        $names = array("seconds", "minutes", "hours", "days", "months", "years");
        $values = array(1, 60, 3600, 24 * 3600, 30 * 24 * 3600, 365 * 24 * 3600);
        for ($i = count($values) - 1; $i > 0 && $time < $values[$i]; $i--) ;
        if ($i == 0) {
            return intval($time / $values[$i]) . " " . $names[$i];
        } else {
            $t1 = intval($time / $values[$i]);
            $t2 = intval(($time - $t1 * $values[$i]) / $values[$i - 1]);
            if ($t2 == 0) {
                return "$t1 " . $names[$i];
            }
            return "$t1 " . $names[$i] . ", $t2 " . $names[$i - 1];
        }
    }

} 