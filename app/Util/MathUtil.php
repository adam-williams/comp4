<?php


namespace AdamWilliams\DMLT\Util;


use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveFunction;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackConstraint;

class MathUtil {

    public static function getStringFromVariablePair($value, $identifier, $spacing = true) {
        if ($value == 0) {
            $final = "";
        } elseif ($value == 1 || $value == -1) {
            $final = $identifier;
        } else {
            $final = abs($value) . $identifier;
        }
        if ($value > 0) {
            $final = "+ " . $final;
        }
        if ($value < 0) {
            $final = "- " . $final;
        }
        return ($spacing) ? $final : str_replace(" ", "", $final);
    }

    /**
     * Gets ordinal suffix (e.g. st, th) for number.
     * @author Andrew Kozak - http://stackoverflow.com/a/8346173
     * @param int $n The number
     * @return string
     */
    public static function getOrdinalSuffix($n) {
        return date('S', mktime(1, 1, 1, 1, ((($n >= 10) + ($n >= 20) + ($n == 0)) * 10 + $n % 10)));
    }

    public static function getStringForMultipleVariables($pairs, $incPlus = false) {
        $final = "";
        $first = true;
        $replacements = [" ", "+"];
        if ($incPlus) {
            $replacements = [];
        }

        foreach ($pairs as $identifier => $value) {
            if ($first) {
                $final .= str_replace($replacements, "", self::getStringFromVariablePair($value, $identifier)) . " ";
            } else {
                $final .= self::getStringFromVariablePair($value, $identifier) . " ";
            }
            $first = false;
        }
        return substr($final, 0, (!$incPlus) ? -1 : strlen($final));
    }

    public static function convertObjectiveFunctionToString(ObjectiveFunction $func, $withOld = false, $withoutNew = false) {
        $pairs = [];
        $positivePairs = [];
        foreach ($func->getVariablePairs() as $pair) {
            $positivePairs[$pair->getVariable()->getIdentifier()] = $pair->getValue();
        }

        foreach ($func->getVariablePairs() as $pair) {
            $pairs[$pair->getVariable()->getIdentifier()] = $pair->getValue() * -1;
        }
        $fullString = $func->getIdentifier()->getIdentifier() . " " . self::getStringForMultipleVariables($pairs, true) . "= 0";
        if ($withOld) {
            return $func->getIdentifier()->getIdentifier() . " = " . self::getStringForMultipleVariables($positivePairs) . ((!$withoutNew) ? (" → " . $fullString) : "");
        }
        return $fullString;
    }

    public static function convertSlackToString(SlackConstraint $slack, $withOld = false, $withoutOld = false) {
        $str = "";
        $pairs = [];
        foreach ($slack->getVariables() as $pair) {
            $pairs[$pair->getVariable()->getIdentifier()] = $pair->getValue();
        }
        $str = MathUtil::getStringForMultipleVariables($pairs);
        $str .= " " . MathUtil::getStringFromVariablePair(1, $slack->getSlackVariable()->getIdentifier()) . " = " . $slack->getValue();
        if ($withOld) {
            $str = MathUtil::getStringForMultipleVariables($pairs) . " ≤ " . $slack->getValue() . ((!$withoutOld) ? (" → " . $str) : "");
        }
        return $str;
    }
} 
