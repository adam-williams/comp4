<?php


namespace AdamWilliams\DMLT\Util;

/**
 * This class is used to convert decimals to fractions.
 * E.g. 0.333333333 = 1/3.
 *
 * This implementation is based on research by John Kennedy of the Santa Monica College, California.
 *
 * @package AdamWilliams\DMLT\Util
 */
class DecToFracUtil {

    private $decimal;
    private $Z;
    private $D = 1;
    private $oldD = 0;
    private $i = 1;
    private $N = null;
    private $scale;

    /**
     * @param mixed $decimal The decimal to convert.
     */
    public function __construct($decimal, $scale=9) {
        $this->decimal = BcUtils::bcRound($decimal, $scale);
        $this->Z = $decimal;
        $this->computeN();
        $this->scale = $scale;
    }

    public function convert($html=false) {
        if ((string)(int)$this->decimal == $this->decimal) {
            return (string)(int)$this->decimal;
        }

        while (!$this->isCloseEnough($this->decimal, bcdiv($this->N, $this->D, $this->scale+1))) {
            $this->i++;
            if ($this->i > 20) {
                return $this->decimal;
            }
            $this->computeZ();
            $this->computeD();
            $this->computeN();
        }
        if ($html) {
            return "<sup>" . BcUtils::bcRound($this->N,0) . "</sup>/<sub>" . BcUtils::bcRound($this->D,0) . "</sub>";
        } else {
            return BcUtils::bcRound($this->N,0) . "/" . BcUtils::bcRound($this->D,0);
        }
    }

    private function computeD() {
        $oldD = $this->D;
        $olderD = $this->oldD;
        $zFloor = BcUtils::bcFloor(BcUtils::bcRound($this->Z, $this->scale+1));
        $this->D = bcadd(bcmul($this->D, $zFloor, $this->scale+1), $this->oldD, $this->scale+1);
        $this->oldD = $oldD;
    }

    private function computeN() {
        $this->N = BcUtils::bcRound($this->decimal * $this->D, 0);
    }

    private function computeZ() {
        $zFloor = BcUtils::bcFloor(BcUtils::bcRound($this->Z, $this->scale+1));
        $oldZ = $this->Z;
        if (bcsub($oldZ, $zFloor, $this->scale) == 0) {
            throw new \Exception("Hit zero. " . $this->Z . " with original " . $this->decimal);
        }
        $this->Z = bcdiv(1, bcsub($oldZ, $zFloor, $this->scale), $this->scale+1);

    }

    private function isCloseEnough($left, $right) {
        return (bccomp($left, $right, $this->scale-1) == 0);
    }
}
