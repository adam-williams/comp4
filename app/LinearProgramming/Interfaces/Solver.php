<?php


namespace AdamWilliams\DMLT\LinearProgramming\Interfaces;


use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;

interface Solver {
    public function solve(Problem $problem);
} 