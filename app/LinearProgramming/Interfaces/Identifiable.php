<?php
namespace AdamWilliams\DMLT\LinearProgramming\Interfaces;

/**
 * Represents an object with an identifier.
 * In the context of linear programming, this could be a decision variable or objective function.
 * @package AdamWilliams\DMLT\Interfaces
 */
interface Identifiable {
    public function getIdentifier();
}