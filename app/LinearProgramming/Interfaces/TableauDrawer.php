<?php


namespace AdamWilliams\DMLT\LinearProgramming\Interfaces;


use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;

interface TableauDrawer {
    public function drawTableau(Tableau $tableau);
}