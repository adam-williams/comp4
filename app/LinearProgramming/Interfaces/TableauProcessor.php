<?php


namespace AdamWilliams\DMLT\LinearProgramming\Interfaces;


use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;

interface TableauProcessor {
    /**
     * @param Problem $problem The problem to act upon.
     * @return Problem
     */
    public function fire(Problem $problem);

    /**
     * @param TableauProcessor $tableauProcessor The next processor to call in the CoR.
     * @return void
     */
    public function setNextStep(TableauProcessor $tableauProcessor);
}