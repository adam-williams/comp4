<?php


namespace AdamWilliams\DMLT\LinearProgramming\Interfaces;
use AdamWilliams\DMLT\LinearProgramming\Entities\Constraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackConstraint;

/**
 * An implementation class will take a Constraint and return a new SlackConstraint.
 * @package AdamWilliams\DMLT\LinearProgramming\Interfaces
 */
interface ConstraintSlackConverter {
    /**
     * @param Constraint $constraint The constraint to convert to a slack constraint.
     * @param string $identifier identifier for the slack variable (e.g. r, s)
     * @return SlackConstraint
     */
    public function convertConstraintToSlack(Constraint $constraint, $identifier);
}