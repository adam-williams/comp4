<?php


namespace AdamWilliams\DMLT\LinearProgramming\Entities;
use AdamWilliams\DMLT\Util\TypeUtils;

/**
 * Models a constraint which already includes a slack variable.
 * @package AdamWilliams\DMLT\LinearProgramming\Entities
 */
class SlackConstraint {
    /**
     * @var VariableCoefficientPair[]
     */
    private $variables;

    /**
     * @var SlackVariable
     */
    private $slack;
    /**
     * @var string
     */
    private $value;

    /**
     * @param VariableCoefficientPair[] $variables
     * @param $slackIdentifier
     * @param string $value RHS
     * @throws \AdamWilliams\DMLT\Exceptions\InvalidTypeException
     */
    public function __construct(array $variables, $slackIdentifier, $value) {
        TypeUtils::checkTypeOfArray($variables, "AdamWilliams\\DMLT\\LinearProgramming\\Entities\\VariableCoefficientPair");
        $this->variables = $variables;
        $this->slack = new SlackVariable($slackIdentifier);
        $this->value = $value;
    }

    /**
     * @return SlackVariable
     */
    public function getSlackVariable() {
        return $this->slack;
    }

    /**
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @return VariableCoefficientPair[]
     */
    public function getVariables() {
        return $this->variables;
    }
}