<?php


namespace AdamWilliams\DMLT\LinearProgramming\Entities;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\Identifiable;

/**
 * Class that represents an objective function.
 * @package AdamWilliams\DMLT\LinearProgramming\Entities
 */
class ObjectiveFunction implements Identifiable {

    /**
     * @var VariableCoefficientPair[]
     */
    private $variables;
    /**
     * @var ObjectiveVariable
     */
    private $identifier;
    /**
     * @var int ObjectiveFunctionType constant.
     */
    private $functionType;

    /**
     * @param VariableCoefficientPair[] $variables The variables involved.
     * @param string $identifier Letter/string identifier for the function.
     * @param int $functionType ObjectiveFunctionType constant.
     */
    public function __construct(array $variables, $identifier, $functionType) {
        $this->variables = $variables;
        $this->identifier = new ObjectiveVariable($identifier);
        $this->functionType = $functionType;
    }

    /**
     * @return ObjectiveVariable
     */
    public function getIdentifier() {
        return $this->identifier;
    }

    /**
     * @return int
     */
    public function getFunctionType() {
        return $this->functionType;
    }

    /**
     * @return VariableCoefficientPair[]
     */
    public function getVariablePairs() {
        return $this->variables;
    }
}