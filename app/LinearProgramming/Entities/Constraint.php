<?php


namespace AdamWilliams\DMLT\LinearProgramming\Entities;
use AdamWilliams\DMLT\Util\TypeUtils;

/**
 * Models a constraint in the linear programming problem.
 * Constraints must be in the form of:
 * v1 + v1 + v3 <=/>= value
 *
 * This **does not** mean we don't support >/< constraint types
 * or constraints with variables on the RHS because they can and
 * should be transformed first.
 * @package AdamWilliams\DMLT\LinearProgramming\Entities
 */
class Constraint {

    /**
     * @var VariableCoefficientPair[]
     */
    private $variables;

    /**
     * @var int ConstraintType constant
     */
    private $constraintType;

    /**
     * @var string
     */
    private $value;

    /**
     * @param array $variables
     * @param $constraintType
     * @param string $value RHS
     * @throws \AdamWilliams\DMLT\Exceptions\InvalidTypeException
     */
    public function __construct(array $variables, $constraintType, $value) {
        TypeUtils::checkTypeOfArray($variables, "AdamWilliams\\DMLT\\LinearProgramming\\Entities\\VariableCoefficientPair");
        $this->variables = $variables;
        $this->constraintType = $constraintType;
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getConstraintType() {
        return $this->constraintType;
    }

    /**
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @return VariableCoefficientPair[]
     */
    public function getVariables() {
        return $this->variables;
    }
}