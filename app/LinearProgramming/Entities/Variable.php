<?php
namespace AdamWilliams\DMLT\LinearProgramming\Entities;

use AdamWilliams\DMLT\LinearProgramming\Interfaces\Identifiable;

/**
 * This class is used to represent an individual decision variable.
 * @package AdamWilliams\DMLT\LinearProgramming\Entities
 */
class Variable implements Identifiable {
    /**
     * @var string
     */
    private $identifier;

    public function __construct($identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function __toString() {
        return "Variable with identifier " . $this->identifier;
    }
}