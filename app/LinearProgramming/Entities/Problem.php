<?php


namespace AdamWilliams\DMLT\LinearProgramming\Entities;
use AdamWilliams\DMLT\LinearProgramming\Solver\OptimumValue;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use Symfony\Component\Console\Helper\Table;

/**
 * Represents a linear programming problem.
 * @package AdamWilliams\DMLT\LinearProgramming\Entities
 */
class Problem {
    /**
     * @var SlackConstraint[] The array of SlackConstraints.
     */
    private $constraints;
    /**
     * @var ObjectiveFunction The objective function to maximize/minimize.
     */
    private $objectiveFunction;

    /**
     * @var Tableau[]
     */
    private $tableaux;

    /**
     * @var bool Whether or not the problem has been solved.
     */
    private $finished = false;

    /**
     * OptimumValue[] The array of optimum values.
     */
    private $optimum;

    /**
     * @var int Num of iterations.
     */
    private $iterations=0;

    private $failed;

    private $failMessage;

    private $reduced = false;

    private $lastPivotValue = 0;

    /**
     * @param SlackConstraint[] $constraints
     * @param ObjectiveFunction $objectiveFunction
     */
    public function __construct(array $constraints, ObjectiveFunction $objectiveFunction) {
        $this->constraints = $constraints;
        $this->objectiveFunction = $objectiveFunction;
    }

    /**
     * @return SlackConstraint[] The array of constraints that must be enforced.
     */
    public function getConstraints() {
        return $this->constraints;
    }

    /**
     * @return ObjectiveFunction The objective function for the problem.
     */
    public function getObjectiveFunction() {
        return $this->objectiveFunction;
    }

    /**
     * @return \AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau[] The tableaux involved.
     */
    public function getTableaux() {
        return $this->tableaux;
    }

    /**
     * Adds a tableau to the list of tableaux.
     * @param Tableau $tableau The tableau to add to the list.
     * @return void
     */
    public function addTableau(Tableau $tableau) {
        $tableau->setIterationId($this->iterations+1);
        $this->tableaux[] = $tableau;
    }

    /**
     * @return mixed
     */
    public function getFinished() {
        return $this->finished;
    }

    /**
     * @param mixed $finished
     */
    public function setFinished($finished) {
        $this->finished = $finished;
    }

    /**
     * @return OptimumValue[]
     */
    public function getOptimum() {
        return $this->optimum;
    }

    /**
     * @param OptimumValue[] $optimum
     */
    public function setOptimum($optimum) {
        $this->optimum = $optimum;
    }

    /**
     * @return int
     */
    public function getIterations() {
        return $this->iterations;
    }

    /**
     * @param int $iterations
     */
    public function setIterations($iterations) {
        $this->iterations = $iterations;
    }

    /**
     * @param mixed $failMessage
     */
    public function setFailMessage($failMessage) {
        $this->failMessage = $failMessage;
        $this->failed = true;
    }

    public function getFailed() {
        return $this->failed;
    }

    /**
     * @return mixed
     */
    public function getFailMessage() {
        return $this->failMessage;
    }

    /**
     * @return boolean
     */
    public function isReduced() {
        return $this->reduced;
    }

    /**
     * @param boolean $reduced
     */
    public function setReduced($reduced) {
        $this->reduced = $reduced;
    }

    /**
     * @return int
     */
    public function getLastPivotValue() {
        return $this->lastPivotValue;
    }

    /**
     * @param int $lastPivotValue
     */
    public function setLastPivotValue($lastPivotValue) {
        $this->lastPivotValue = $lastPivotValue;
    }
}