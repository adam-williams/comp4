<?php


namespace AdamWilliams\DMLT\LinearProgramming\Entities;

/**
 * Class which contains a value and pointer to decision variable.
 * @package AdamWilliams\DMLT\LinearProgramming\Entities
 */
class VariableCoefficientPair {

    /**
     * @var string Represents the value in front of the variable.
     * This should be passed as a string to allow for arbitrary-precision decimal math.
     */
    private $value;
    /**
     * @var Variable The actual variable.
     */
    private $variable;

    /**
     * @param string $value
     * @param Variable $variable
     */
    public function __construct($value, Variable $variable) {
        $this->value = $value;
        $this->variable = $variable;
    }

    /**
     * @return int Represents the value in front of the variable.
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @return Variable The actual variable.
     */
    public function getVariable() {
        return $this->variable;
    }

} 