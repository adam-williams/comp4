<?php


namespace AdamWilliams\DMLT\LinearProgramming\Solver;


use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Exceptions\CannotSolveException;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\Solver;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotCellProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotColumnProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotRowEliminationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotRowIdentificationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotRowTransformationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\ProblemFinalizationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\ProblemFormulationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\ThetaCalculatorProcessor;

class MaximizingSolver implements Solver {

    public function solve(Problem $problem) {
        $i = 0;
        $firstStep = $this->setupCor();
        while (!$problem->getFinished()) {
            try {
                $firstStep->fire($problem);
            } catch (CannotSolveException $exception) {
                $problem->setFailMessage($exception->getMessage());
                break;
            }
            $i++;
            if ($i > 20) {
                $problem->setFailMessage("The maximum number of iterations (20) was exceeded so solving was stopped.");
                break;
            }
        }
        return $problem;
    }

    private function setupCor() {
        $pfp = (new ProblemFormulationProcessor());
        $pcp = new PivotColumnProcessor();
        $tcp = new ThetaCalculatorProcessor();
        $prp = new PivotRowIdentificationProcessor();
        $pcep = new PivotCellProcessor();
        $prtp = new PivotRowTransformationProcessor();
        $prep = new PivotRowEliminationProcessor();
        $tcp->setNextStep($prp);
        $pcp->setNextStep($tcp);
        $pfp->setNextStep($pcp);
        $prp->setNextStep($pcep);
        $pcep->setNextStep($prtp);
        $prtp->setNextStep($prep);
        $prep->setNextStep(new ProblemFinalizationProcessor());
        return $pfp;
    }
}