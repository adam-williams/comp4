<?php


namespace AdamWilliams\DMLT\LinearProgramming\Solver;


use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\Util\DecToFracUtil;

class OptimumValue implements \JsonSerializable{
    /**
     * @var Variable
     */
    private $variable;
    private $optimum;

    /**
     * @param Variable $variable
     * @param $optimum
     */
    public function __construct(Variable $variable, $optimum) {

        $this->variable = $variable;
        $this->optimum = $optimum;
    }

    /**
     * @return mixed
     */
    public function getOptimumValue() {
        return $this->optimum;
    }

    /**
     * @return Variable
     */
    public function getVariable() {
        return $this->variable;
    }

    public function jsonSerialize() {
        return ["identifier" => $this->variable->getIdentifier(), "value" => (new DecToFracUtil($this->getOptimumValue()))->convert()];
    }
}
