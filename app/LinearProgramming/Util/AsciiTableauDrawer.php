<?php


namespace AdamWilliams\DMLT\LinearProgramming\Util;


use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauDrawer;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Util\BcUtils;

/**
 * Class used for debugging purposes. This won't be used in production so I don't care how messy it is.
 * @package AdamWilliams\DMLT\LinearProgramming\Util
 */
class AsciiTableauDrawer implements TableauDrawer {

    const BOLD_ON = "\033[34m";

    const BOLD_OFF = "\033[0m";

    public function drawTableau(Tableau $tableau) {
        // debugging purposes only
        print "\n== TABLEAUX DRAWER FOR DEBUG USE ==\n\n";
        print $tableau->getDescription() . "\n\n";
        $headings = ["Basic variable"];

        foreach ($tableau->getColumnHeadings() as $heading) {
            /** @var $heading Cell */
            if ($heading->getReference() instanceof Variable) {
                $headings[] = $heading->getReference()->getIdentifier();
            } else {
                switch ($heading->getReference()) {
                    case SpecialColumn::VALUE:
                        $headings[] = "Value";
                        break;
                    case SpecialColumn::THETA_VALUES:
                        $headings[] = "θ values";
                        break;
                    case SpecialColumn::ROW_OPERATIONS:
                        $headings[] = "Row operations";
                        break;
                }
            }
        }
        print implode(",", $headings) . "\n";

        foreach ($tableau->getRows() as $row) {
            $activeRow = $tableau->getPivotRowRef() === $row->getReference();
            //echo $activeRow;
            if ($activeRow) {
                $rowText = self::BOLD_ON . $row->getReference()->getIdentifier() . ",";
            } else {
                $rowText = $row->getReference()->getIdentifier() . ",";
            }

            foreach ($row->getCells() as $cell) {
                $trimmedValue = BcUtils::bcRemoveTrailingZeroes(BcUtils::bcRound($cell->getValue(), 9));
                if ($tableau->getPivotColRef() === $cell->getReference()) {
                    if ($tableau->getPivotCell() === $cell) {
                        $rowText .= "\033[32m" . $trimmedValue . "\33[0m,";
                    } else {
                        $rowText .= "\033[31m" . $trimmedValue . "\33[0m,";
                    }
                    if ($activeRow) {
                        $rowText .= self::BOLD_ON;
                    }
                } else {
                    $rowText .= $trimmedValue . ",";
                }

            }
            if ($activeRow) {
                $rowText = substr($rowText, 0, strlen($rowText) -1) . self::BOLD_OFF;
            } else {
                $rowText = substr($rowText, 0, strlen($rowText) -1);
            }
            print $rowText . "\n";
        }
        print "\n== END TABLEAUX DRAWER FOR DEBUG USE ==\n\n";
    }
}