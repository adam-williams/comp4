<?php


namespace AdamWilliams\DMLT\LinearProgramming\Util;


use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauDrawer;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Tests\Unit\DecToFracUtilTest;
use AdamWilliams\DMLT\Util\BcUtils;
use AdamWilliams\DMLT\Util\DecToFracUtil;
use AdamWilliams\DMLT\Util\MathUtil;

/**
 * @package AdamWilliams\DMLT\LinearProgramming\Util
 */
class ArrayTableauDrawer implements TableauDrawer {

    public function processMultiple(array $tableaux, Problem $problem) {
        $obj = [];
        if ($problem->isReduced()) {
            $obj["all"] = $this->handleReduced($problem);
        } else {
            foreach ($tableaux as $i => $tableau) {
                $obj["all"][] = $this->drawTableau($tableau, ($i + 1));
            }
        }

        $obj['iterations'] = ($problem->getIterations() == 1) ? "iteration" : "iterations";
        if ($problem->getOptimum() !== null) {
            foreach ($problem->getOptimum() as $optimum) {
                $obj['optimum'][$optimum->getVariable()->getIdentifier()] = (new DecToFracUtil($optimum->getOptimumValue()))->convert(true);
            }
        }
        return $obj;
    }

    private function handleReduced(Problem $problem) {
        $tableaux = $problem->getTableaux();
        /** @var Tableau[] $tableaux */
        $tableauxArray = [];
        $iteration = -1;
        $thetaTableau = null;
        $workingTableau = null;
        $workingThetaValues = [];
        $x = 0;
        $step = 1;
        foreach ($tableaux as $tableau) {

            if ($tableau->getLangKey() == "eliminate_bv") {
                $workingTableau = $tableau;
            } elseif ($tableau->getLangKey() == "calculate_theta") {
                foreach ($tableau->getRows() as $row) {
                    foreach ($row->getCells() as $cell) {
                        if ((!$cell->getReference() instanceof Variable) && $cell->getReference() === SpecialColumn::THETA_VALUES) {
                            $workingThetaValues[] = $cell;
                        }
                    }
                }
            } elseif ($tableau->getLangKey() == "find_pivot_row") {
                $thetaTableau = $tableau;
                $thetaTableau->setLangKey("reduced_theta");
                $thetaTableau->setDescription("Calculate theta values.");
            }

            $x++;

            /** @var Tableau $workingTableau */
            if ($iteration !== $tableau->getIterationId() || $x === count($tableaux)) {
                if ($iteration < 0) {
                    // ignore
                } else {
                    $workingTableau->setLangKey("perform_iteration");
                    $workingTableau->setDescription("Finish the " . $iteration . MathUtil::getOrdinalSuffix($iteration) . " iteration");
                    foreach ($workingTableau->getRows() as $i => $row) {
                        $cells = $row->getCells();
                        $row->setCells(array_merge($cells, [$workingThetaValues[$i]]));
                    }
                    $workingTableau->generateColumnHeadings();
                    $tableauxArray[] = $this->drawTableau($thetaTableau, $step);
                    $step += 1;
                    $tableauxArray[] = $this->drawTableau($workingTableau, $step);
                }
                $iteration = $tableau->getIterationId();


            }

        }
        return $tableauxArray;
    }

    public function drawTableau(Tableau $tableau, $step=null) {
        $obj = [];
        $obj["description"] = $tableau->getDescription();
        $obj["prose"] = trans("explanations." . $tableau->getLangKey(), $tableau->getLocalizationData());
        $headings = [];
        foreach ($tableau->getColumnHeadings() as $heading) {
            /** @var $heading Cell */
            if ($heading->getReference() instanceof Variable) {
                $headings[] = ["value" => $heading->getReference()->getIdentifier(), "type" => "variable"];
            } else {
                switch ($heading->getReference()) {
                    case SpecialColumn::VALUE:
                        $headings[] = ["value" => "Value", "type" => "value"];
                        break;
                    case SpecialColumn::THETA_VALUES:
                        $headings[] = ["value" => "θ values", "type" => "theta"];
                        break;
                    case SpecialColumn::ROW_OPERATIONS:
                        $headings[] = ["value" => "Row operations", "type" => "rowops"];
                        break;
                }
            }
        }
        $obj['headings'] = $headings;
        $obj['step'] = $step;
        $obj['key'] = $tableau->getLangKey();
        $obj['iteration'] = $tableau->getIterationId();
        $rows = [];
        foreach ($tableau->getRows() as $row) {

            $activeRow = $tableau->getPivotRowRef() === $row->getReference();
            $rowItem = ["isPivot" => $activeRow];
            $rowItem['identifier'] = $row->getReference()->getIdentifier();
            $cells = [];
            foreach ($row->getCells() as $cell) {
                $cellItem = ['isPivotCol' => false, 'isPivot' => false, 'isTheta' => false, 'isRowOps' => false];
                $cellItem['value'] = $cell->getValue();

                if (!$cell->getIsText()) {
                    $trimmedValue = BcUtils::bcRemoveTrailingZeroes(BcUtils::bcRound($cell->getValue(), 9));
                    $frac = (new DecToFracUtil($cell->getValue()))->convert(true);
                    $cellItem['value'] = $frac;
                }

                if (!$cell->getReference() instanceof Variable) {
                    if ($cell->getReference() == SpecialColumn::THETA_VALUES) {
                        $cellItem['isTheta'] = true;
                    }

                    if ($cell->getReference() == SpecialColumn::ROW_OPERATIONS) {
                        $cellItem['isRowOps'] = true;
                    }
                }


                if ($tableau->getPivotColRef() === $cell->getReference()) {
                    $cellItem['isPivotCol'] = true;
                    if ($tableau->getPivotCell() === $cell) {
                        $cellItem['isPivot'] = true;
                    }
                }
                $classes = $cellItem['isPivotCol'] ? "tableau--pivot-col" : "";
                $classes = $classes . " " . ($rowItem['isPivot'] ? "tableau--pivot-row" : "");
                $classes = $classes . " " . ($cellItem['isTheta'] ? "tableau--theta-row" : "");
                $classes = $classes . " " . ($cellItem['isRowOps'] ? "tableau--ops-row" : "");
                $cellItem['classes'] = $classes;
                $cells[] = $cellItem;
            }
            $rowItem['cells'] = $cells;
            $rowItem['justChanged'] = $row->getHasRefJustChanged();
            $rows[] = $rowItem;
        }
        $obj['rows'] = $rows;
        return $obj;
    }
}
