<?php


namespace AdamWilliams\DMLT\LinearProgramming\Util;


use AdamWilliams\DMLT\LinearProgramming\Entities\Constraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackConstraint;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\ConstraintSlackConverter;

class ConstraintConverter implements ConstraintSlackConverter {

    /**
     * @param Constraint $constraint The constraint to convert to a slack constraint.
     * @param string $identifier identifier for the slack variable (e.g. r, s)
     * @return SlackConstraint
     */
    public function convertConstraintToSlack(Constraint $constraint, $identifier) {
        return new SlackConstraint($constraint->getVariables(), $identifier, $constraint->getValue());
    }
}