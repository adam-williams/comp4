<?php


namespace AdamWilliams\DMLT\LinearProgramming\Factories;

use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;

/**
 * Contains static functions to facilitate object creation.
 * @package AdamWilliams\DMLT\LinearProgramming\Factories
 */
class EntityFactory {

    /**
     * Static helper function to create a Variable class.
     * @param $identifier string The textual representation of the variable (e.g. x)
     * @return Variable The created object.
     */
    public static function createDecisionVariable($identifier) {
        return new Variable($identifier);
    }



} 