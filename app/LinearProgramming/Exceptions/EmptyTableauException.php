<?php


namespace AdamWilliams\DMLT\LinearProgramming\Exceptions;

/**
 * Exception thrown if tableu has no rows in it.
 * @package AdamWilliams\DMLT\LinearProgramming\Exceptions
 */
class EmptyTableauException extends \Exception {

}