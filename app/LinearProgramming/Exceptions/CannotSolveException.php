<?php


namespace AdamWilliams\DMLT\LinearProgramming\Exceptions;

/**
 * Exception thrown if none of the theta values are positive.
 * @package AdamWilliams\DMLT\LinearProgramming\Exceptions
 */
class CannotSolveException extends \Exception {

}