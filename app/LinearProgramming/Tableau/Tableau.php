<?php


namespace AdamWilliams\DMLT\LinearProgramming\Tableau;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Exceptions\EmptyTableauException;

/**
 * Class to represent a simplex tableau.
 * Tableaux contain rows which contain cells.
 * Each row references a basic variable (e.g. a SlackVariable or an ObjectiveVariable).
 * Each cell belongs to a row and references a Variable, SlackVariable or SpecialColumn.
 * @package AdamWilliams\DMLT\LinearProgramming\Tableau
 */
class Tableau {

    /**
     * @var Row[] Array of rows in the tableau.
     */
    private $rows = [];

    /**
     * @var array Array of column headings. These will either be Variable instances or SpecialColumn IDs.
     */
    private $columnHeadings = [];
    private $description;
    private $pivotColRef;
    private $pivotRowRef;
    private $pivotCell;
    private $langKey;
    private $localizationData = [];
    private $iterationId = -1;

    /**
     * @param Row[] $rows Rows to add to the tableau.
     * @param string $description Description of the step which this tableau represents.
     * @throws EmptyTableauException
     */
    public function __construct($rows, $description, $langKey) {
        $this->rows = $rows;
        $this->generateColumnHeadings();
        $this->description = $description;
        $this->langKey = $langKey;
    }

    public function generateColumnHeadings() {
        $this->columnHeadings = [];
        if (count($this->rows) < 1) {
            throw new EmptyTableauException();
        }
        /** @var Row $firstRow */
        $firstRow = $this->rows[0];

        foreach ($firstRow->getCells() as $cell) {
            $this->columnHeadings[] = $cell;
        }
    }

    /**
     * @return Row[] The rows in the tableau.
     */
    public function getRows() {
        return $this->rows;
    }

    /**
     * @return array
     */
    public function getColumnHeadings() {
        return $this->columnHeadings;
    }

    /**
     * @return string The description of the step which this tableau represents.
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param Variable $pivotColRef
     */
    public function setPivotColRef($pivotColRef) {
        $this->pivotColRef = $pivotColRef;
    }

    /**
     * @param Variable $pivotRowRef
     */
    public function setPivotRowRef($pivotRowRef) {
        $this->pivotRowRef = $pivotRowRef;
    }

    /**
     * @return Variable The reference to the pivot column variable in the tableau.
     */
    public function getPivotColRef() {
        return $this->pivotColRef;
    }

    /**
     * @return Variable The reference to the pivot row in the tableau.
     */
    public function getPivotRowRef() {
        return $this->pivotRowRef;
    }

    /**
     * @param string $description The new description for the tableau.
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @return Cell The pivot cell (i.e. cell in pivot col and row).
     */
    public function getPivotCell() {
        return $this->pivotCell;
    }

    /**
     * @param Cell $pivotCell The cell to set as the pivot.
     */
    public function setPivotCell($pivotCell) {
        $this->pivotCell = $pivotCell;
    }

    /**
     * @return mixed
     */
    public function getLangKey() {
        return $this->langKey;
    }

    /**
     * @param mixed $langKey
     */
    public function setLangKey($langKey) {
        $this->langKey = $langKey;
    }

    /**
     * @return array
     */
    public function getLocalizationData() {
        return $this->localizationData;
    }

    /**
     * @param array $localizationData
     */
    public function setLocalizationData($localizationData) {
        $this->localizationData = $localizationData;
    }

    /**
     * @return mixed
     */
    public function getIterationId() {
        return $this->iterationId;
    }

    /**
     * @param mixed $iterationId
     */
    public function setIterationId($iterationId) {
        $this->iterationId = $iterationId;
    }
}