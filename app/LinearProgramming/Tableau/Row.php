<?php


namespace AdamWilliams\DMLT\LinearProgramming\Tableau;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;

/**
 * Class to represent a row in a tableau.
 * @package AdamWilliams\DMLT\LinearProgramming\Tableau
 */
class Row {
    /**
     * @var Cell[] Represents the cells in the tableau.
     */
    private $cells = [];
    private $reference;
    private $hasRefJustChanged = false;

    /**
     * @param $cells Cell[] The array of cells that form the row.
     * @param $reference Variable The variable reference.
     */
    public function __construct($cells, $reference) {
        $this->cells = $cells;
        $this->reference = $reference;
    }

    /**
     * @return Cell[] The cells in the row.
     */
    public function getCells() {
        return $this->cells;
    }

    /**
     * @return Variable
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * @return mixed
     */
    public function getHasRefJustChanged() {
        return $this->hasRefJustChanged;
    }

    /**
     * @param mixed $hasRefJustChanged
     */
    public function setHasRefJustChanged($hasRefJustChanged) {
        $this->hasRefJustChanged = $hasRefJustChanged;
    }

    /**
     * @param Cell[] $cells
     */
    public function setCells(array $cells) {
        $this->cells = $cells;
    }
}