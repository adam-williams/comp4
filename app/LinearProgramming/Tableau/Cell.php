<?php


namespace AdamWilliams\DMLT\LinearProgramming\Tableau;


use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;

/**
 * Class to represent a cell in a tableau.
 * @see SpecialColumn
 * @package AdamWilliams\DMLT\LinearProgramming\Tableau
 */
class Cell {
    /**
     * @var Variable|int The variable the cell is attached to.
     */
    private $reference;
    private $value;
    private $isText;

    /**
     * @param Variable|int $reference The variable that the cell references.
     *                                This is basically the column header in the tableau.
     * @param string $value The value of the cell. String for BC math.
     * @param bool $isText
     * @throws \AdamWilliams\DMLT\Exceptions\InvalidEnumValueException
     */
    public function __construct($reference, $value, $isText=false) {
        if (!$reference instanceof Variable) {
            SpecialColumn::assertValid($reference);
        }
        $this->reference = $reference;
        $this->value = $value;
        $this->isText = $isText;
    }

    /**
     * @return Variable|int
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * @return string The cell's value.
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getIsText() {
        return $this->isText;
    }

    /**
     * @param mixed $isText
     */
    public function setIsText($isText) {
        $this->isText = $isText;
    }

} 