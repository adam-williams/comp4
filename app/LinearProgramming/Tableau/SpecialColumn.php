<?php


namespace AdamWilliams\DMLT\LinearProgramming\Tableau;


use AdamWilliams\DMLT\LinearProgramming\Enums\Enum;

/**
 * Class to represent a 'special' column type in the tableau.
 * @package AdamWilliams\DMLT\LinearProgramming\Tableau
 */
class SpecialColumn extends Enum {
    /**
     * Represents a column to store values.
     */
    const VALUE = 0;

    /**
     * Represents a column to store theta values.
     */
    const THETA_VALUES = 1;

    /**
     * Represents a column to store data about the changes to the row.
     */
    const ROW_OPERATIONS = 2;
}