<?php


namespace AdamWilliams\DMLT\LinearProgramming\Enums;


use AdamWilliams\DMLT\Exceptions\InvalidEnumValueException;
use ReflectionClass;

abstract class Enum {

    public static function isValid($valueToTest) {
        $rc = new ReflectionClass(get_called_class());
        $retVal = false;
        // could use in_array here but I think it's inconsistent
        foreach ($rc->getConstants() as $name => $value) {
            if ($value === $valueToTest) {
                $retVal = true;
                break;
            }
        }
        return $retVal;
    }

    public static function assertValid($valueToTest) {
        if (!self::isValid($valueToTest)) {
            throw new InvalidEnumValueException();
        }
    }

} 