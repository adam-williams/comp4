<?php


namespace AdamWilliams\DMLT\LinearProgramming\Enums;

/**
 * Enum to contain all possible constraint types
 * @package AdamWilliams\DMLT\LinearProgramming\Enums
 */
class ConstraintType extends Enum {

    /**
     * Represents a <= constraint type.
     */
    const LT_EQUAL_TO = 0;

    /**
     * Represents a >= constraint type.
     */
    const GT_EQUAL_TO = 0;
} 