<?php


namespace AdamWilliams\DMLT\LinearProgramming\Enums;

/**
 * Represents a type of objective function
 * @package AdamWilliams\DMLT\LinearProgramming\Enums
 */
class ObjectiveFunctionType extends Enum {
    const MAXIMIZING = 0;
    const MINIMIZING = 1;
}