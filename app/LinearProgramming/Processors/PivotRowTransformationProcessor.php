<?php
namespace AdamWilliams\DMLT\LinearProgramming\Processors;

use AdamWilliams\DMLT\Exceptions\InvalidProblemException;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveVariable;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackVariable;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Util\DecToFracUtil;
use \PHPUnit_Framework_Assert as Assert;

class PivotRowTransformationProcessor extends BaseProcessor implements TableauProcessor {

    /**
     * Takes a tableau with a valid pivot (row, cell & col) and divides the pivot row by the value of the pivot cell.
     * @param Problem $problem
     * @return Problem
     */
    public function fire(Problem $problem) {
        $tableau = $this->getLatestTableau($problem);
        $pivotValue = $tableau->getPivotCell()->getValue();
        $pivotReference = $tableau->getPivotCell()->getReference();
        $pivotCell = null;
        $pivotCol = null;
        $rows = [];
        foreach ($tableau->getRows() as $row) {
            if ($tableau->getPivotRowRef() === $row->getReference()) {
                $newRow = [];
                foreach ($row->getCells() as $cell) {
                    // loop over each cell in the pivot row and create a modified copy
                    if (!($cell->getReference() instanceof Variable) && $cell->getReference() === SpecialColumn::THETA_VALUES) {
                        continue;
                    }
                    $value = $cell->getValue();
                    $reference = $cell->getReference();
                    if (bccomp($pivotValue, 0, 11) === 0) {
                        $value = 0;
                    } else {
                        $value = bcdiv($value, $pivotValue, 11);
                    }
                    $newCell = new Cell($reference, $value);
                    $newRow[] = $newCell;
                    if ($tableau->getPivotCell() === $cell) {
                        // we need to update the reference to the pivot cell since we've changed all the references everywhere
                        $pivotCell = $newCell;
                        $pivotCol = $newCell->getReference();
                    }
                }
                $newRow[] = new Cell(SpecialColumn::ROW_OPERATIONS, "Divide by pivot (" . (new DecToFracUtil($pivotValue))->convert() . ")", true);
                $finalRow = new Row($newRow, $pivotReference);
                $finalRow->setHasRefJustChanged(true);
                $rows[] = $finalRow;

            } else {
                $newRowCells = $this->getCellsExcludingReference($row, SpecialColumn::THETA_VALUES, false);

                $newRowCells[] = new Cell(SpecialColumn::ROW_OPERATIONS, "Leave intact", true);
                
                $rows[] = new Row($newRowCells, $row->getReference());
            }
        }
        $problem->setLastPivotValue((new DecToFracUtil($pivotValue))->convert());
        $newTableau = new Tableau($rows, "Divide pivot row by pivot value and change row's basic variable reference", "divide_pivot_change_bv");
        // since we've changed the reference for the pivot row, we need to update the new tableau too
        $newTableau->setPivotRowRef($pivotReference);
        $newTableau->setPivotCell($pivotCell);
        // I think we've also broken the reference to the pivot column so let's update that one too
        $newTableau->setPivotColRef($pivotCol);
        $problem->addTableau($newTableau);
        return $this->fireNext($problem);
    }

}
