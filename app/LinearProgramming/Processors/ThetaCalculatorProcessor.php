<?php
namespace AdamWilliams\DMLT\LinearProgramming\Processors;

use AdamWilliams\DMLT\Exceptions\InvalidProblemException;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveVariable;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackVariable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Util\DecToFracUtil;
use \PHPUnit_Framework_Assert as Assert;

class ThetaCalculatorProcessor extends BaseProcessor implements TableauProcessor {

    /**
     * Takes a tableau and calculates the theta values using the pivot column.
     * @param Problem $problem
     * @return Problem
     */
    public function fire(Problem $problem) {
        $tableau = $this->getLatestTableau($problem);
        $newRows = [];
        $i = 0;
        $localizationData = [];
        foreach ($tableau->getRows() as $row) {
            if ($row->getReference() instanceof ObjectiveVariable) {
                $cells = $row->getCells();
                $cells[] = new Cell(SpecialColumn::THETA_VALUES, "N/A", true);
                $newRows[] = new Row($cells, $row->getReference());
                continue;
            }
            $pivotValue = null;
            $rowValue = null;
            foreach ($row->getCells() as $cell) {

                if ($cell->getReference() === $tableau->getPivotColRef()) {
                    $pivotValue = $cell->getValue();
                }

                if ($cell->getReference() === SpecialColumn::VALUE) {
                    $rowValue = $cell->getValue();
                }
            }
            $cmp = bccomp("0", $pivotValue, 11);
            if ($cmp === 0) {
                $value = "Infinity";
            } else {
                $value = bcdiv($rowValue, $pivotValue, 11);
            }

            if ($i == 0) {
                $localizationData = ["theta" => (new DecToFracUtil($value))->convert(true), "value" => (new DecToFracUtil($rowValue))->convert(true), "pivot" => (new DecToFracUtil($pivotValue))->convert(true)];
            }


            $cell = new Cell(SpecialColumn::THETA_VALUES, $value, ($value == "Infinity"));
            $newRows[] = new Row(array_merge($row->getCells(), [$cell]), $row->getReference());
            $i++;
        }
        $newTableau = new Tableau($newRows, "Calculate theta values", "calculate_theta");
        $newTableau->setLocalizationData($localizationData);
        $newTableau->setPivotColRef($tableau->getPivotColRef());
        $problem->addTableau($newTableau);
        return $this->fireNext($problem);
    }

}
