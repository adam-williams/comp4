<?php


namespace AdamWilliams\DMLT\LinearProgramming\Processors;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackConstraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Entities\VariableCoefficientPair;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\LinearProgramming\Util\AsciiTableauDrawer;

/**
 * CoR class. Handles formulation of a linear programming problem
 * into the initial tableau.
 * @package AdamWilliams\DMLT\LinearProgramming\Processors
 */
class ProblemFormulationProcessor extends BaseProcessor implements TableauProcessor {

    /**
     * @param Problem $problem The linear programming problem.
     * @return \AdamWilliams\DMLT\LinearProgramming\Entities\Problem The finished problem.
     */
    public function fire(Problem $problem) {

        //TODO: Refactor into small private methods
        if (count($problem->getTableaux()) > 0) {
            return $this->fireNext($problem);
        }
        $rows = [];
        /** @var Variable[] $variables */
        $variables = [];
        // Since we can't trust the constraints to reference all of our variables, we need to grab a list from the
        // objective function before we do anything else.

        $objective = $problem->getObjectiveFunction();

        // We don't care about the values in the objective function so let's only grab the variable instances.

        $variables = array_map($this->getVariablesFromPairArray(), $objective->getVariablePairs());

        $constraints = $problem->getConstraints();
        foreach ($constraints as $constraint) {
            // Each row of our tableau (apart from the last) references a slack constraint
            $currentCells = []; // reset temporary variable
            $constraintVariables = $this->getLookupFromPairArray($constraint->getVariables());
            foreach ($variables as $variable) {
                $cellValue = "0";
                $objectHash = spl_object_hash($variable);
                if (array_key_exists($objectHash, $constraintVariables)) {
                    $var = $constraintVariables[$objectHash];
                    /** @var $var VariableCoefficientPair */
                    $cellValue = $var->getValue();
                }
                $currentCells[] = new Cell($variable, $cellValue);
            }
            $currentCells = array_merge($currentCells, $this->getSlackCells($constraints, $constraint));
            $currentCells[] = $this->getValueCell($constraint);
            $row = new Row($currentCells, $constraint->getSlackVariable());
            $rows[] = $row;
        }

        // At this point we have the rows for the basic variables
        // we now need an extra row for our objective function
        $currentCells = [];
        foreach ($objective->getVariablePairs() as $pair) {
            $currentCells[] = new Cell($pair->getVariable(), bcmul("-1", $pair->getValue(), 11));
        }

        foreach ($constraints as $c) {
            $currentCells[] = new Cell($c->getSlackVariable(), 0);
        }
        $currentCells[] = new Cell(SpecialColumn::VALUE, 0);
        $rows[] = new Row($currentCells, $objective->getIdentifier());

        $resultantTableau = new Tableau($rows, "Formulate the constraints and objective function into a tableau", "formulate_constraints");

        $problem->addTableau($resultantTableau);
        return $this->fireNext($problem);
    }

    /**
     * @param VariableCoefficientPair[] $pairs
     * @return array
     */
    private function getLookupFromPairArray(array $pairs) {
        $arr = [];
        foreach ($pairs as $item) {
            $arr[spl_object_hash($item->getVariable())] = $item;
        };
        return $arr;
    }

    /**
     * @return callable
     */
    private function getVariablesFromPairArray() {
        return function (VariableCoefficientPair $item) {
            return $item->getVariable();
        };
    }

    /**
     * @param SlackConstraint[] $constraints
     * @param SlackConstraint $constraint
     * @return Cell[] The new cells.
     */
    private function getSlackCells($constraints, $constraint) {
        $cells = [];
        foreach ($constraints as $c) {
            $value = 0;
            if ($c->getSlackVariable() === $constraint->getSlackVariable()) {
                $value = 1;
            }
            $cells[] = new Cell($c->getSlackVariable(), $value);
        }
        return $cells;
    }

    private function getValueCell(SlackConstraint $constraint) {
        return new Cell(SpecialColumn::VALUE, $constraint->getValue());
    }
}