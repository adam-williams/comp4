<?php
namespace AdamWilliams\DMLT\LinearProgramming\Processors;

use AdamWilliams\DMLT\Exceptions\InvalidProblemException;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveVariable;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackVariable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Tests\Unit\DecToFracUtilTest;
use AdamWilliams\DMLT\Util\DecToFracUtil;

class PivotColumnProcessor extends BaseProcessor implements TableauProcessor {

    /**
     * Takes a tableau and identifies the pivot column.
     * @param Problem $problem
     * @return Problem
     */
    public function fire(Problem $problem) {
        $tableau = $this->getLatestTableau($problem);
        foreach ($tableau->getRows() as $row) {
            if ($row->getReference() instanceof ObjectiveVariable) {
                $currentSmallest = null;
                foreach ($row->getCells() as $cell) {
                    $cellReferencesSlack = $cell->getReference() instanceof SlackVariable;
                    if ($currentSmallest === null || (bccomp($currentSmallest->getValue(), $cell->getValue()) > 0 && !$cellReferencesSlack)) {
                        $currentSmallest = $cell;
                    }
                }
                $newTableau = new Tableau($tableau->getRows(), "Identify pivot column", "identify_pivot");
                $newTableau->setLocalizationData(["variable" => $currentSmallest->getReference()->getIdentifier(), "value" => (new DecToFracUtil($currentSmallest->getValue()))->convert(true)]);
                $newTableau->setPivotColRef($currentSmallest->getReference());
                $problem->addTableau($newTableau);
                $this->fireNext($problem);
            }
        }
    }
}
