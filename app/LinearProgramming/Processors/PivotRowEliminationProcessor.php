<?php
namespace AdamWilliams\DMLT\LinearProgramming\Processors;

use AdamWilliams\DMLT\Exceptions\InvalidProblemException;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveVariable;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackVariable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Util\DecToFracUtil;
use AdamWilliams\DMLT\Util\MathUtil;
use \PHPUnit_Framework_Assert as Assert;
use Symfony\Component\Console\Helper\Table;

class PivotRowEliminationProcessor extends BaseProcessor implements TableauProcessor {

    /**
     * Uses the pivot row to try and eliminate the basic variable from the other rows.
     * @param Problem $problem
     * @return Problem
     */
    public function fire(Problem $problem) {
        $tableau = $this->getLatestTableau($problem);
        $pivotReference = $tableau->getPivotCell()->getReference();
        $pivotValue = $tableau->getPivotCell()->getValue();
        $respectivePivotRowValues = [];
        $multiplicationValues = [];
        $i = 0;
        $pivotNumber = 1;
        foreach ($tableau->getRows() as $row) {
            $newRows[$i] = [$row->getCells(), $row->getReference()];
            $multiplicationValues[$i] = 1;
            if ($tableau->getPivotRowRef() === $row->getReference()) {
                $pivotNumber = $pivotNumber + $i;
                foreach ($row->getCells() as $cell) {
                    $respectivePivotRowValues[] = $cell->getValue();
                }
            }

            foreach ($row->getCells() as $cell) {
                // let's try and find the cell which references the same thing as our pivot cell
                if ($cell->getReference() === $pivotReference) {
                    // found it, now let's take a look at the value
                    $cellValue = $cell->getValue();
                    // We need to basically find a multiple of our pivotValue which eliminates our cellValue
                    // This multiple can be negative (in which case we're technically subtracting :P)
                    if (bccomp($pivotValue, "0", 11) === 0) {
                        $timesToMultiply = 0;
                    } else {
                        $timesToMultiply = bcdiv($cellValue, $pivotValue, 11);
                        $timesToMultiply = bcmul("-1", $timesToMultiply, 11);
                    }


                    $multiplicationValues[$i] = $timesToMultiply;
                }
            }

            $i++;
        }
        $newRows = [];
        $rN = 1;
        foreach ($tableau->getRows() as $i => $row) {
            $newCells = [];
            if ($tableau->getPivotRowRef() === $row->getReference()) {
                foreach ($row->getCells() as $cell) {
                    if ($cell->getReference() === SpecialColumn::THETA_VALUES) {
                        continue;
                    }
                    if ($cell->getReference() === SpecialColumn::ROW_OPERATIONS) {
                        if ($problem->isReduced()) {
                            $newCells[] = new Cell(SpecialColumn::ROW_OPERATIONS, "R<sub>$rN</sub> &divide; " . $problem->getLastPivotValue(), true);
                        } else {
                            $newCells[] = new Cell(SpecialColumn::ROW_OPERATIONS, "R<sub>$rN</sub>", true);
                        }

                    } else {
                        $newCells[] = $cell;
                    }
                }
                $newRows[] = new Row($newCells, $row->getReference());
            } else {
                $mf = $multiplicationValues[$i];
                $x = 0;
                foreach ($row->getCells() as $cell) {
                    if ($cell->getReference() === SpecialColumn::THETA_VALUES || $cell->getIsText()) {
                        continue;
                    } else {
                        $currentValue = $cell->getValue();
                        $multipliedValue = bcmul($respectivePivotRowValues[$x], $mf, 11);
                        $newValue = bcadd($currentValue, $multipliedValue, 11);
                        $newCell = new Cell($cell->getReference(), $newValue);
                        $newCells[] = $newCell;
                    }
                    $x++;

                }
                $rowMultiple = (float) $mf;
                if ($rowMultiple == "0") {
                    $newCells[] = new Cell(SpecialColumn::ROW_OPERATIONS, "R<sub>$rN</sub>", true);
                } else {
                    $newCells[] = new Cell(SpecialColumn::ROW_OPERATIONS, "R<sub>$rN</sub> " . MathUtil::getStringFromVariablePair($rowMultiple, "R<sub>$pivotNumber</sub>"), true);
                }

                $newRows[] = new Row($newCells, $row->getReference());
            }
            $rN++;
        }
        $newTableau = new Tableau($newRows, "Eliminate basic variable from non-pivot rows", "eliminate_bv");
        $newTableau->setPivotColRef($tableau->getPivotColRef());
        $newTableau->setPivotRowRef($tableau->getPivotRowRef());
        $newTableau->setPivotCell($tableau->getPivotCell());
        $problem->addTableau($newTableau);

        return $this->fireNext($problem);
    }

}
