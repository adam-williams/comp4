<?php


namespace AdamWilliams\DMLT\LinearProgramming\Processors;


use AdamWilliams\DMLT\Exceptions\InvalidProblemException;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\LinearProgramming\Util\AsciiTableauDrawer;

abstract class BaseProcessor implements TableauProcessor {

    /**
     * @var TableauProcessor The next processor in the CoR.
     */
    private $tableauProcessor;

    /**
     * @param Problem $problem The problem to act upon.
     * @return Problem
     */
    public function fire(Problem $problem) {
        $this->fireNext($problem);
    }

    /**
     * @param TableauProcessor $tableauProcessor The next processor to call in the CoR.
     * @return void
     */
    public function setNextStep(TableauProcessor $tableauProcessor) {
        $this->tableauProcessor = $tableauProcessor;
    }

    /**
     * @param Problem $problem The problem to pass on.
     * @return \AdamWilliams\DMLT\LinearProgramming\Entities\Problem The finished problem.
     */
    protected function fireNext(Problem $problem) {
        if ($this->tableauProcessor !== null) {
            return $this->tableauProcessor->fire($problem);
        } else {
            return $problem;
        }
    }

    /**
     * @param Problem $problem
     * @return Tableau The latest tableau in the problem class.
     * @throws InvalidProblemException
     */
    protected function getLatestTableau(Problem $problem) {
        $tableaux = $problem->getTableaux();
        $tableauxCount = count($tableaux);
        if ($tableauxCount === 0) {
            throw new InvalidProblemException("Problem has no tableaux!");
        }
        $lastItem = $tableaux[count($tableaux)-1];
        if ($lastItem === null) {
            throw new InvalidProblemException("Null final tableau!");
        } elseif (!$lastItem instanceof Tableau) {
            throw new InvalidProblemException("Final tableau in array is not a tableau!");
        }
        return $lastItem;
    }

    /**
     * @param Row $row The row to remove the cells from.
     * @param Variable|int $reference The reference to check against.
     * @return Row|Cell[] The new row
     */
    public function getCellsExcludingReference(Row $row, $reference,$newRow=true) {
        $cells = [];
        foreach ($row->getCells() as $cell) {
            if ($cell->getReference() === $reference) {
                continue;
            }
            $cells[] = $cell;
        }
        return (($newRow === true) ? new Row($cells, $row->getReference()) : $cells);
    }
}