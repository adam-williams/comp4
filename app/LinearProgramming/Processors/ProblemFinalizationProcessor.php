<?php
namespace AdamWilliams\DMLT\LinearProgramming\Processors;

use AdamWilliams\DMLT\Exceptions\InvalidProblemException;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveVariable;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackVariable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Solver\OptimumValue;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Util\BcUtils;
use \PHPUnit_Framework_Assert as Assert;

class ProblemFinalizationProcessor extends BaseProcessor implements TableauProcessor {

    /**
     * Takes a tableau and finds the pivot cell by looking at the pivot row and column.
     * @param Problem $problem
     * @return Problem
     */
    public function fire(Problem $problem) {
        $tableau = $this->getLatestTableau($problem);
        $finished = true;
        foreach ($tableau->getRows() as $row) {
            if ($row->getReference() instanceof ObjectiveVariable) {
                foreach ($row->getCells() as $cell) {
                    if (bccomp("0", $cell->getValue()) > 0) {
                        $finished = false;
                    }
                }
            }
        }
        $newRows = [];
        foreach ($tableau->getRows() as $row) {
            $newRows[] = $this->getCellsExcludingReference($row, SpecialColumn::ROW_OPERATIONS);
        }
        $tableau = new Tableau($newRows, "Check to see if problem is finished", "check_problem");

        $problem->addTableau($tableau);
        $problem->setIterations($problem->getIterations()+1);
        $problem->setFinished($finished);
        $optimumSolutions = [];
        if ($finished) {
            $done = [];
            foreach ($tableau->getRows() as $row) {
                foreach ($row->getCells() as $cell) {
                    if ($cell->getReference() === SpecialColumn::VALUE) {
                        $ov = new OptimumValue($row->getReference(), BcUtils::bcRound($cell->getValue(), 9));
                        $optimumSolutions[] = $ov;
                        $done[] = $row->getReference();
                    }
                }
            }

            foreach ($problem->getObjectiveFunction()->getVariablePairs() as $pair) {
                if (!in_array($pair->getVariable(), $done, true)) {
                    $optimumSolutions[] = new OptimumValue($pair->getVariable(), "0");
                    $done[] = $pair->getVariable();
                }
            }
            foreach ($problem->getConstraints() as $constraint) {
                    if (!in_array($constraint->getSlackVariable(), $done, true)) {
                        $optimumSolutions[] = new OptimumValue($constraint->getSlackVariable(), "0");
                        $done[] = $constraint->getSlackVariable();
                    }
            }
        }
        $problem->setOptimum($optimumSolutions);
        return $this->fireNext($problem);
    }

}
