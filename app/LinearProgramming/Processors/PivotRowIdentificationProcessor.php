<?php
namespace AdamWilliams\DMLT\LinearProgramming\Processors;

use AdamWilliams\DMLT\Exceptions\InvalidProblemException;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveVariable;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackVariable;
use AdamWilliams\DMLT\LinearProgramming\Exceptions\CannotSolveException;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Util\DecToFracUtil;
use \PHPUnit_Framework_Assert as Assert;

class PivotRowIdentificationProcessor extends BaseProcessor implements TableauProcessor {

    /**
     * Takes a tableau and finds the pivot row by looking at the theta values.
     * @param Problem $problem
     * @throws InvalidProblemException
     * @throws \Exception
     * @return Problem
     */
    public function fire(Problem $problem) {
        $tableau = $this->getLatestTableau($problem);
        /** @var Cell $smallestValue */
        $smallestValue = null;
        $rowRef = null;
        foreach ($tableau->getRows() as $row) {
            foreach ($row->getCells() as $cell) {
                if ($cell->getReference() === SpecialColumn::THETA_VALUES) {
                    if (!is_numeric($cell->getValue())) {
                        continue;
                    }
                    if (bccomp("0", $cell->getValue()) >= 1) {
                        continue;
                    }
                    if ($smallestValue === null || bccomp($smallestValue->getValue(), $cell->getValue()) > 0) {
                        $smallestValue = $cell;
                        $rowRef = $row->getReference();
                    }
                }
            }
        }

        if ($smallestValue === null) {
            throw new CannotSolveException("The pivot row is chosen from the smallest, positive theta value. In this instance, no positive theta values exist.");
        }

        $newTableau = clone $tableau;
        $newTableau->setDescription("Find pivot row from theta values");
        $newTableau->setLangKey("find_pivot_row");
        $newTableau->setLocalizationData(["theta" => (new DecToFracUtil($smallestValue->getValue()))->convert(true), "row" => $rowRef->getIdentifier()]);
        $newTableau->setPivotRowRef($rowRef);
        $problem->addTableau($newTableau);
        return $this->fireNext($problem);
    }

}
