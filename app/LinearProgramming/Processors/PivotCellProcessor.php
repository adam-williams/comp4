<?php
namespace AdamWilliams\DMLT\LinearProgramming\Processors;

use AdamWilliams\DMLT\Exceptions\InvalidProblemException;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveVariable;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackVariable;
use AdamWilliams\DMLT\LinearProgramming\Interfaces\TableauProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\SpecialColumn;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use \PHPUnit_Framework_Assert as Assert;

class PivotCellProcessor extends BaseProcessor implements TableauProcessor {

    /**
     * Takes a tableau and finds the pivot cell by looking at the pivot row and column.
     * @param Problem $problem
     * @return Problem
     */
    public function fire(Problem $problem) {
        $tableau = $this->getLatestTableau($problem);
        $newRows = [];
        $pivotCell = null;
        foreach ($tableau->getRows() as $row) {
            if ($tableau->getPivotRowRef() === $row->getReference()) {
                foreach ($row->getCells() as $cell) {
                    if ($tableau->getPivotColRef() === $cell->getReference()) {
                        $pivotCell = $cell;
                    }
                }
            }
            $newRows[] = $this->getCellsExcludingReference($row, SpecialColumn::THETA_VALUES);
        }
        $tableau->setPivotCell($pivotCell);

        return $this->fireNext($problem);
    }

}