<?php
namespace AdamWilliams\DMLT\ServiceProvider;


use AdamWilliams\DMLT\Routing\Registration;
use Illuminate\Routing\Router;

class RouteServiceProvider extends \Illuminate\Foundation\Support\Providers\RouteServiceProvider {

    /**
     * Define the routes for the application.
     *
     * @param Router $router
     * @return void
     */
    public function map(Router $router) {
        (new Registration($router))->registerRoutes();
    }

} 