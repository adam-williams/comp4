<?php
namespace AdamWilliams\DMLT\ServiceProvider;

use AdamWilliams\DMLT\Routing\Registration;
use AdamWilliams\DMLT\Validation\ProblemJsonValidator;
use AdamWilliams\DMLT\Validation\TwoFactorValidator;
use AdamWilliams\DMLT\Validation\ValidationResolver;
use Blade;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\ServiceProvider;
use Response;
use Validator;

/**
 * Service provider responsible for framework registration.
 * @package AdamWilliams\DMLT\ServiceProvider
 */
class DecisionMathsLearningTool extends ServiceProvider {

    /**
     * Called automatically by Laravel.
     * Used to register everything the application needs.
     */
    public function register() {
        $this->registerIocBindings();
        $this->registerRoutes();
        $this->registerCommands();
        $this->registerMaintenancePage();
    }

    /**
     * This function is called once most other stuff is ready
     * @param Log $log Logging contract implementation.
     */
    public function boot(Log $log) {
        Blade::setEchoFormat("%s");
        Validator::resolver(function ($translator, $data, $rules, $messages) {
            return new ValidationResolver($translator, $data, $rules, $messages);
        });
    }

    /**
     * Bind interfaces to concrete implementations.
     */
    protected function registerIocBindings() {
        // abstract => concrete
        $this->app->bind("AdamWilliams\\DMLT\\Contracts\\Auth\\DevAuthenticationInterface",
            "AdamWilliams\\DMLT\\Http\\Auth\\SessionDevAuthentication");
        $this->app->bind("AdamWilliams\\DMLT\\Contracts\\Repositories\\UserRepositoryInterface",
            "AdamWilliams\\DMLT\\Eloquent\\Repositories\\EloquentUserRepository");
        $this->app->bind("Illuminate\\Contracts\\Auth\\Registrar",
            "AdamWilliams\\DMLT\\Services\\Registrar");
    }

    /**
     * Register all the routes we need to listen for.
     */
    protected function registerRoutes() {
        (new Registration($this->app['router']))->registerRoutes();
    }

    /**
     * Register our handler for when the application is marked as down (e.g. during deployments).
     */
    private function registerMaintenancePage() {
        $this->app->down(function () {
            return Response::view("errors.maintenance", [], 503);
        });
    }

    private function registerCommands() {
        $this->commands(["AdamWilliams\\DMLT\\Console\\DeployCommand"]);
    }
}
