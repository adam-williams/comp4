<?php


namespace AdamWilliams\DMLT\Exceptions;

/**
 * Thrown when we encounter an invalid type
 * @package AdamWilliams\DMLT\Exceptions
 */
class InvalidTypeException extends \Exception {

    public function __construct($expected, $actual) {
        parent::__construct("Expected type of " . $expected . " but got " . $actual);
    }

}