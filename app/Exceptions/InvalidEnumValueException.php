<?php


namespace AdamWilliams\DMLT\Exceptions;

/**
 * Exception thrown when a value passed doesn't exist in an enum.
 * @see AdamWilliams\DMLT\LinearProgramming\Enums\Enum
 * @package AdamWilliams\DMLT\Exceptions
 */
class InvalidEnumValueException extends \Exception {

}