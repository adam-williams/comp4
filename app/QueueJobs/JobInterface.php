<?php
namespace AdamWilliams\DMLT\QueueJobs;

use Illuminate\Queue\Jobs\Job;

interface JobInterface {
    public function fire(Job $job, $data);
}