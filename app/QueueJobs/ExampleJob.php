<?php
namespace AdamWilliams\DMLT\QueueJobs;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Queue\Jobs\Job;

class ExampleJob extends BaseJob implements JobInterface {

    public function fire(Job $job, $data) {
        $fs = new Filesystem();
        $fs->put(base_path() . "/qj.txt", time());
    }
}