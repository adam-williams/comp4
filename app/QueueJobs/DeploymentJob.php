<?php
namespace AdamWilliams\DMLT\QueueJobs;

use Artisan;
use Cache;
use Illuminate\Cache\FileStore;
use Illuminate\Cache\Repository;
use Illuminate\Console\Application as Console;
use Illuminate\Foundation\Application;
use Illuminate\Log\Writer;
use Illuminate\Queue\Jobs\Job;
use Log;

/**
 * Class DeploymentJob
 * @package AdamWilliams\DMLT\QueueJobs
 */
class DeploymentJob extends BaseJob implements JobInterface {
    public $COMPOSER_LOCK_PATH;

    /**
     * @var Writer The log writer.
     */
    private $log;
    /**
     * @var Application Laravel's artisan Application instance.
     */
    private $artisan;
    /**
     * @var Application
     */
    private $app;

    public function __construct(Writer $log, Console $artisan, Application $app) {
        $this->log = $log->getMonolog();
        $this->artisan = $artisan;
        $this->app = $app;
        $this->COMPOSER_LOCK_PATH = base_path() . "/composer.lock";
    }

    public function fire(Job $job, $data) {
        // TODO: Reafctor this part to use DI and inject logging interface through constructor
        Log::info("START Deploy debug!");
        $this->setMaintenanceMode(true);
        Log::info("DEPLOY: We turned on maintenance mode");
        if (!array_key_exists("no-pull", $data)) {
            Log::info("DEPLOY: about to pull changes");
            $this->pullChanges();
            Log::info("DEPLOY: We pulled changes");
        }
        Log::info("DEPLOY: We're gonna perform a composer install");
        $this->composerInstall();
        Log::info("DEPLOY: We performed a composer install");
        Log::info("DEPLOY: We're gonna compile sass");
        $this->compileSass();
        Log::info("DEPLOY: SASS was compiled :D Now for TypeScript");
        $this->compileTypescript();
        Log::info("DEPLOY: We finished compiling typescript, now to bring back up the app");
        $this->setMaintenanceMode(false);
        Log::info("DEPLOY: We're gonna restart our queue workers");
        $this->restartQueueWorkers();
        Log::info("END Deploy debug!");
        Cache::forever("last-deploy", time());
    }

    private function pullChanges() {
        // We're using git as our VCS so we can just do a 'git pull' to grab the latest changes
        // since our code here is run by the queue worker we can be sure that we have permissions etc

        // shell_exec will be run in the context of the script's CWD
        // this should be a git repository.
        $this->log->info("Performed git pull" , ["result" => shell_exec("git pull 2>&1")]);
    }

    private function setMaintenanceMode($mode) {
        $this->log->info("Setting maintenance mode to " . $mode);
        $this->artisan->call($mode ? "down" : "up");
    }

    private function restartQueueWorkers() {
        $this->artisan->call("queue:restart");
    }

    private function composerInstall() {
        // only do an install if we need to
        $existingHash = Cache::get("deploy.composer-lock-hash");
        $currentHash = $this->getComposerLockHash();
        if ($existingHash === null || $existingHash !== $currentHash) {
            chdir(base_path());
            $this->log->info("Performed composer install" , ["result" => shell_exec("composer install --no-scripts 2>&1")]);
            Cache::forever("deploy.composer-lock-hash", $currentHash);
        }
    }

    private function getComposerLockHash() {
        return md5_file($this->COMPOSER_LOCK_PATH);
    }

    private function compileSass() {
        chdir(base_path() . '/resources/sass');
        $cl = "sass -I . -t compact --scss --sourcemap=none --update .:../../public/assets/css/ 2>&1";
        if ($this->app->isLocal()) {
            $cl = "sass -I . -g -l -t expanded --scss --sourcemap=auto --update .:../../public/assets/css/ 2>&1";
        }
        $this->log->info("Performed sass compile" , ["result" => shell_exec($cl)]);
    }

    /**
     * Compiles *.ts into *.js
     */
    private function compileTypescript() {
        chdir(base_path() . '/resources/typescript');
        $cmd = "tsc *.ts --removeComments --out ../../public/assets/js/app.js";
        if ($this->app->isLocal()) {
            $cmd = "tsc *.ts --sourcemap --out ../../public/assets/js/app.js";
        }
        $this->log->info("Performed typescript compile" , ["result" => shell_exec($cmd)]);
    }
}
