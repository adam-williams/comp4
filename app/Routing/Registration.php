<?php


namespace AdamWilliams\DMLT\Routing;


use Illuminate\Routing\Router;

/**
 * Responsible for registering our routes.
 * @package AdamWilliams\DMLT\Routing
 */
class Registration {

    /**
     * @var \Illuminate\Routing\Router
     */
    private $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router) {
        $this->router = $router;
    }

    public function registerRoutes() {
        // @formatter:off
        $this->router->group(array("namespace" => "AdamWilliams\\DMLT\\Http\\Controllers"), function () {
            $this->router->get('/2fa', [
                "as" => "2fa",
                "uses" => "DevAuthController@showChallenge"
            ]);
            $this->router->post('/2fa', [
                "uses" => "DevAuthController@checkCode"
            ]);
            $this->router->post('/2fa-ip-bypass', [
                "uses" => "DevAuthController@checkIp"
            ]);
            $this->router->get('/2fa-gen', [
                "as" => "2fa-gen",
                "uses" => "DevAuthController@generateSecret"
            ]);
            $this->router->post('/deploy', [
                "as" => "deploy",
                "uses" => "DeployController@performDeployment"
            ]);
        });

        /* DEV AUTH REQUIRED */
        $this->router->group(array("namespace" => "AdamWilliams\\DMLT\\Http\\Controllers", "middleware" => "DevAuthMiddleware"), function() {
            // Route for automated code deployment
            $this->router->get('/', [
                "as" => "home",
                "uses" => "HomeController@showHome"
            ]);
            $this->router->get('/solve', [
                "as" => "solve",
                "uses" => "ProblemCreationController@showForm"
            ]);
            $this->router->post('/solve', [
                "uses" => "ProblemCreationController@addProblem"
            ]);
            $this->router->get('/generator', [
                "as" => "generator",
                "uses" => "GeneratorController@showGenerator"
            ]);
            //$this->router->get('/about', [
             //   "as" => "about",
            //    "uses" => "AboutController@showAbout"
            //]);
            $this->router->get('/stats', [
                "as" => "stats",
                "uses" => "StatsController@showStats"
            ]);
            $this->router->get('/explain', [
                "as" => "explain",
                "uses" => "AlgorithmExplanationController@showAlgorithmExplanation"
            ]);
            $this->router->get('/204', function() {
                return \Response::make('', 204);
            });
            $this->router->controller("auth", "Auth\\AuthController", ["getRegister" => "register", "getLogin" => "login", "getLogout" => "logout"]);
        });
    }
}
