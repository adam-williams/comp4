<?php
namespace AdamWilliams\DMLT\UserInterface;

/**
 * Class to represent a navigation item.
 * @package AdamWilliams\DMLT\UserInterface
 */
use Route;

/**
 * Class NavItem
 * @package AdamWilliams\DMLT\UserInterface
 */
class NavItem {

    /**
     * @var string The title of the link.
     */
    private $title;
    /**
     * @var string The route name (from Laravel).
     */
    private $routeName;
    /**
     * @var bool Whether the item is selected.
     */
    private $active = false;
    /**
     * @var bool Whether the item is secondary.
     */
    private $isSecondary = false;

    /**
     * @param string $title The title displayed to the user for this item.
     * @param string $routeName Laravel's route name for this link.
     * @param $isSecondary
     */
    public function __construct($title, $routeName, $isSecondary=false) {
        $this->title = $title;
        $this->routeName = $routeName;
        $this->isSecondary = $isSecondary;
    }

    /**
     * @return string The title displayed to the user for this item.
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return string The name of the route.
     */
    public function getRouteName() {
        return $this->routeName;
    }

    /**
     * @return string The URL that can be used in the 'href' attribute of the link.
     */
    public function getUrl() {
        // We're now tied to the framework :/
        if (!Route::has($this->routeName)) {
            return "missing-route";
        }
        return \URL::route($this->routeName);
    }

    /**
     * @param bool $bool New value for if the item is currently selected.
     */
    public function setActive($bool = true) {
        $this->active = $bool;
    }

    /**
     * @param bool $isSecondary The new value for the isSecondary attribute.
     */
    public function setIsSecondary($isSecondary) {
        $this->isSecondary = $isSecondary;
    }

    /**
     * @return bool Whether or not the item is currently selected.
     */
    public function isActive() {
        return $this->active;
    }

    /**
     * @return boolean Whether or not the navigation item is of secondary type.
     */
    public function isSecondary() {
        return $this->isSecondary;
    }
}