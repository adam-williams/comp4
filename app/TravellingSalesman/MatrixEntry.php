<?php


namespace AdamWilliams\DMLT\TravellingSalesman;


class MatrixEntry implements \JsonSerializable {

    /**
     * @param GraphNode $colref
     * @param GraphNode $rowRef
     * @param int $value
     */
    public function __construct($colref, $rowRef, $value) {
        $this->colref = $colref;
        $this->rowRef = $rowRef;
        $this->value = $value;
    }

    public function __toString() {
        return $this->colref->getIdentifier() . $this->rowRef->getIdentifier() . "(" . $this->value . ")";
    }

    protected $rowRef;
    protected $colref;
    protected $value;


    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize() {
        return $this->__toString();
    }
}