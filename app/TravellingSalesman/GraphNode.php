<?php


namespace AdamWilliams\DMLT\TravellingSalesman;


use AdamWilliams\DMLT\Exceptions\InvalidArcException;
use AdamWilliams\DMLT\Util\TypeUtils;

class GraphNode {
    protected $identifier;
    protected $connectedArcs = [];
    protected $visited = false;

    /**
     * @param $identifier
     * @param $connectedArcs GraphArc[]
     */
    public function __construct($identifier, $connectedArcs=null) {
        if ($connectedArcs != null) {
            TypeUtils::checkTypeOfArray($connectedArcs, "AdamWilliams\\DMLT\\TravellingSalesman\\GraphArc");
            $this->connectedArcs[] = $connectedArcs;
        }
        $this->identifier = $identifier;
    }

    public function addConnection(GraphArc $arc) {
        if ($arc->getDestination() === $this) {
            throw new InvalidArcException("Arc cannot join the same node to itself.");
        }
        $this->connectedArcs[] = $arc;
    }

    /**
     * @return boolean
     */
    public function isVisited() {
        return $this->visited;
    }

    /**
     * @param boolean $visited
     */
    public function setVisited($visited) {
        $this->visited = $visited;
    }

    /**
     * @return mixed
     */
    public function getIdentifier() {
        return $this->identifier;
    }
}