<?php

namespace AdamWilliams\DMLT\TravellingSalesman;

use AdamWilliams\DMLT\Exceptions\InvalidArcException;

class GraphArc implements \JsonSerializable {
    protected $value;
    protected $dest;
    protected $source;

    /**
     * Represents a connection between two nodes.
     * @param GraphNode $sourceNode The source node node.
     * @param GraphNode $targetNode The destination node node.
     * @param int $value The weight of the arc.
     * @throws InvalidArcException
     */
    public function __construct($sourceNode, $targetNode, $value) {
        if ($sourceNode === $targetNode) {
            throw new InvalidArcException("Arcs can't connect the same node to itself.");
        }
        $this->source = $sourceNode;
        $this->dest = $targetNode;
        $this->value = $value;
    }

    /**
     * @return GraphNode The source node.
     */
    public function getSource() {
        return $this->source;
    }

    /**
     * @return GraphNode The destination node.
     */
    public function getDestination() {
        return $this->dest;
    }

    /**
     * @return int The weight.
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize() {
        return $this->source->getIdentifier() . $this->dest->getIdentifier() . "(" . $this->value . ")";
    }

    function __toString() {
        return $this->source->getIdentifier() . $this->dest->getIdentifier() . "(" . $this->value . ")";
    }
}