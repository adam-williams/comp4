<?php


namespace AdamWilliams\DMLT\TravellingSalesman;


use AdamWilliams\DMLT\Util\TypeUtils;

class LeastDistanceMatrix {

    protected $size;
    protected $itemsArray = [];
    protected $nodes = [];
    protected $arcs = [];
    protected $arcLookup = null;
    protected $arcsToInclude = [];
    protected $firstNode = null;
    protected $solutionArcs = [];

    /**
     * @param GraphNode $startNode First node.
     * @param GraphNode[] $nodes Node entries.
     * @param GraphArc[] $arcs Arc entries.
     * @throws \AdamWilliams\DMLT\Exceptions\InvalidTypeException
     */
    public function __construct($startNode, $nodes, $arcs) {
        TypeUtils::checkTypeOfArray($nodes, "AdamWilliams\\DMLT\\TravellingSalesman\\GraphNode");
        TypeUtils::checkTypeOfArray($arcs, "AdamWilliams\\DMLT\\TravellingSalesman\\GraphArc");
        $size = count($nodes);
        $this->size = $size;
        $this->firstNode = $startNode;
        for ($i = 0; $i < $size; $i++) {
            $this->itemsArray[$i] = [];
        }
        $this->arcLookup = new \SplObjectStorage();
        $this->arcs = $arcs;
        $this->nodes = $nodes;
        $this->createMatrix();
    }

    /**
     * @return array
     */
    public function getSolutionArcs() {
        return $this->solutionArcs;
    }


    private function createMatrix() {
        foreach ($this->arcs as $arc) {
            if (isset($this->arcLookup[$arc->getSource()])) {
                $arr = $this->arcLookup[$arc->getSource()];
                $arr[] = $arc;
                $this->arcLookup[$arc->getSource()] = $arr;
            } else {
                $this->arcLookup[$arc->getSource()] = [$arc];
            }

            if (isset($this->arcLookup[$arc->getDestination()])) {
                $arr = $this->arcLookup[$arc->getDestination()];
                $arr[] = $arc;
                $this->arcLookup[$arc->getDestination()] = $arr;
            } else {
                $this->arcLookup[$arc->getDestination()] = [$arc];
            }
        }

        $i = 0;
        $this->handleNode($this->firstNode);

    }

    private function handleNode(GraphNode $node) {
        $cheapestArc = null;
        /** @var $cheapestArc GraphArc */
        $x = 0;
        $nextNode = null;
        foreach ($this->arcLookup[$node] as $arc) {
            /** @var $arc GraphArc */
            $cost = $arc->getValue();
            /** @var $arc GraphArc */
            if ($arc->getSource() === $node) {
                if (!$arc->getDestination()->isVisited()) {
                    if ($cheapestArc == null || $cost < $cheapestArc->getValue()) {
                        $cheapestArc = $arc;
                        $nextNode = $arc->getDestination();
                    }
                }
            } else {
                if (!$arc->getSource()->isVisited()) {
                    if ($cheapestArc == null || $cost < $cheapestArc->getValue()) {
                        $cheapestArc = $arc;
                        $nextNode = $arc->getSource();
                    }
                }
            }


            $x++;
        }
        if ($nextNode === null) {
            $cheapestArc = array_values(array_intersect($this->arcLookup[$node], $this->arcLookup[$this->firstNode]))[0];
        }
        $node->setVisited(true);
        $this->solutionArcs[] = $cheapestArc;
        if ($nextNode !== null) {
            $this->handleNode($nextNode);
        }

    }

} 