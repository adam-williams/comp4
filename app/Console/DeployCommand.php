<?php


namespace AdamWilliams\DMLT\Console;


use Illuminate\Console\Command;
use \Queue;
use Symfony\Component\Console\Input\InputOption;

class DeployCommand extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dmlt:deploy';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manually initiate a deploy';

    /**
     * Create a new command instance.
     *
     * @return DeployCommand
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        $data = [];
        if ($this->option("exjob")) {
            $this->info("Pushing example job.");
            Queue::push("AdamWilliams\\DMLT\\QueueJobs\\ExampleJob");
            die();
        }
        if ($this->option("no-pull")) {
            $data['no-pull'] = 1;
        }
        /** @see AdamWilliams\DMLT\QueueJobs\DeploymentJob */
        Queue::push("AdamWilliams\\DMLT\\QueueJobs\\DeploymentJob", $data);
        $this->info("Deployment has been queued.");
    }

    public function getOptions() {
        return array(
            array("no-pull", null, InputOption::VALUE_NONE , "Disables the git pull"),
            array("exjob", null, InputOption::VALUE_NONE , "Tests the queue system instead of deploying")
        );
    }
}