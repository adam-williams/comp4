<?php


namespace AdamWilliams\DMLT\Tests\Integration;


use AdamWilliams\DMLT\Tests\TestCase;
use AdamWilliams\DMLT\TravellingSalesman\GraphNode;
use AdamWilliams\DMLT\TravellingSalesman\LeastDistanceMatrix;
use AdamWilliams\DMLT\TravellingSalesman\GraphArc;

class NearestNeighbourTest extends TestCase {

   /**
    * Test the nearest neighbour algorithm used as part of the
    * travelling salesman process (taught in D2).
    *
    * @return void  
    */
    public function testNearestNeighbourAlgorithmWithSimpleProblem() {
        $nodeA = new GraphNode("A");
        $nodeB = new GraphNode("B");
        $nodeC = new GraphNode("C");
        // A
        $entryAB = new GraphArc($nodeA, $nodeB, 7);
        $entryAC = new GraphArc($nodeA, $nodeC, 3);
        // B
        $entryBC = new GraphArc($nodeB, $nodeC, 2);

        $ldm = new LeastDistanceMatrix($nodeA, [$nodeA, $nodeB, $nodeC], [$entryAB, $entryAC, $entryBC]);
        $solution = $ldm->getSolutionArcs();
        $this->assertEquals([$entryAC, $entryBC, $entryAB], $solution);
    }

    public function testNearestNeighbourAlgorithmWithAdvancedProblem() {
        $nodeA = new GraphNode("A");
        $nodeB = new GraphNode("B");
        $nodeC = new GraphNode("C");
        $nodeD = new GraphNode("D");
        $nodeE = new GraphNode("E");
        // A
        $entryAB = new GraphArc($nodeA, $nodeB, 7);
        $entryAC = new GraphArc($nodeA, $nodeC, 4);
        $entryAD = new GraphArc($nodeA, $nodeD, 3);
        $entryAE = new GraphArc($nodeA, $nodeE, 1);
        // B
        $entryBC = new GraphArc($nodeB, $nodeC, 21);
        $entryBD = new GraphArc($nodeB, $nodeD, 17);
        $entryBE = new GraphArc($nodeB, $nodeE, 9);
        // C
        $entryCD = new GraphArc($nodeC, $nodeD, 11);
        $entryCE = new GraphArc($nodeC, $nodeE, 18);
        // D
        $entryDE = new GraphArc($nodeD, $nodeE, 70);

        $allConnections = [$entryAB, $entryAC, $entryAD, $entryAE, $entryBC, $entryBD, $entryBE, $entryCD, $entryCE, $entryDE];

        $ldm = new LeastDistanceMatrix($nodeC, [$nodeA, $nodeB, $nodeC, $nodeD, $nodeE], $allConnections);
        $solution = $ldm->getSolutionArcs();
        $this->assertEquals([$entryAC, $entryAE, $entryBE, $entryBD, $entryCD], $solution);
    }

} 
