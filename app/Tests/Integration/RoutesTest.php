<?php

namespace AdamWilliams\DMLT\Tests\Unit;

use AdamWilliams\DMLT\Tests\TestCase;
use Queue;

/**
 * Collection of integration tests to ensure pages are being displayed properly etc.
 * @package AdamWilliams\DMLT\Tests\Unit
 */
class RoutesTest extends TestCase {

    /**
     * Ensure index route gives us what we expect.
     *
     * @return void
     */
    public function testHomepage() {
        //$crawler = $this->client->request("GET", "/");
        //$this->assertResponseOk();
    }

    /**
     * Ensure the deploy route schedules the deployment task as we expect it to.
     *
     * @return void
     */
    public function testDeploy() {
        // Mock Queue facade so we can ensure it's being called
       // Queue::shouldReceive("connected")->once(); // Apparently this is called when the facade is used
        //Queue::shouldReceive("push")->once()->withArgs(["AdamWilliams\\DMLT\\QueueJobs\\DeploymentJob", []]);
        //$crawler = $this->client->request("POST", "/deploy");
        //$this->assertResponseOk();
    }

}