<?php

namespace AdamWilliams\DMLT\Tests\Integration;

use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveFunction;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackConstraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\VariableCoefficientPair;
use AdamWilliams\DMLT\LinearProgramming\Enums\ObjectiveFunctionType;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotCellProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotColumnProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotRowEliminationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotRowIdentificationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotRowTransformationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\ProblemFinalizationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\ProblemFormulationProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\ThetaCalculatorProcessor;
use AdamWilliams\DMLT\LinearProgramming\Solver\MaximizingSolver;
use AdamWilliams\DMLT\LinearProgramming\Solver\OptimumValue;
use AdamWilliams\DMLT\Tests\TestCase;
use AdamWilliams\DMLT\LinearProgramming\Factories\EntityFactory as EF;

/**
 * Integration tests to test actual linear programming
 * problems.
 * @package AdamWilliams\DMLT\Tests\Unit
 */
class ProblemTest extends TestCase {

    /**
     * Test the solver with our dedicated example problem.
     *
     * @return void
     */
    public function testExampleProblemBase() {
        $vars = [EF::createDecisionVariable("x"), EF::createDecisionVariable("y"), EF::createDecisionVariable("z")];
        $objFuncVars = [new VariableCoefficientPair("10", $vars[0]), new VariableCoefficientPair("12", $vars[1]),
                        new VariableCoefficientPair("8", $vars[2])];
        $objectiveFunction = new ObjectiveFunction($objFuncVars, "P", ObjectiveFunctionType::MAXIMIZING);
        $this->assertTrue($objectiveFunction->getVariablePairs()[0]->getVariable() === $vars[0]);

        // Great, we have an objective function. Now we need some constraints
        $constraints = [];
        $constraints[] = new SlackConstraint([new VariableCoefficientPair("2", $vars[0]),
                                              new VariableCoefficientPair("2", $vars[1])], "r", "5");
        $constraints[] = new SlackConstraint([new VariableCoefficientPair("5", $vars[0]),
                                              new VariableCoefficientPair("3", $vars[1]),
                                              new VariableCoefficientPair("4", $vars[2])], "s", "15");
        // Now we can make our Problem instance
        $problem = new Problem($constraints, $objectiveFunction);
        (new MaximizingSolver())->solve($problem);
        $this->checkSolutions($problem->getOptimum(), ["r" => "0", "s" => "0", "x" => "0", "z" => "1.875", "P" => "45", "y" => "2.5"]);
    }

    /**
     * Test the solver with another alternative example problem from the textbook.
     * Exercise 4B, Question 1.
     * @return void
     */
    public function testExampleProblemOne() {
        $vars = [EF::createDecisionVariable("x"), EF::createDecisionVariable("y"), EF::createDecisionVariable("z")];
        $objFuncVars = [new VariableCoefficientPair("5", $vars[0]), new VariableCoefficientPair("6", $vars[1]),
                        new VariableCoefficientPair("4", $vars[2])];
        $objectiveFunction = new ObjectiveFunction($objFuncVars, "P", ObjectiveFunctionType::MAXIMIZING);
        $this->assertTrue($objectiveFunction->getVariablePairs()[0]->getVariable() === $vars[0]);

        // Great, we have an objective function. Now we need some constraints
        $constraints = [];
        $constraints[] = new SlackConstraint([new VariableCoefficientPair("1", $vars[0]),
                                              new VariableCoefficientPair("2", $vars[1])], "r", "6");
        $constraints[] = new SlackConstraint([new VariableCoefficientPair("5", $vars[0]),
                                              new VariableCoefficientPair("3", $vars[1]),
                                              new VariableCoefficientPair("3", $vars[2])], "s", "24");
        // Now we can make our Problem instance
        $problem = new Problem($constraints, $objectiveFunction);
        (new MaximizingSolver())->solve($problem);
        $this->checkSolutions($problem->getOptimum(), ["x" => "0", "z" => "5", "P" => "38", "y" => "3", "r" => "0", "s" => "0"]);
    }

    /**
     * Test the solver with another alternative example problem from the textbook.
     * Exercise 4B, Question 2.
     * @return void
     */
    public function testExampleProblemTwo() {
        $vars = [EF::createDecisionVariable("x"), EF::createDecisionVariable("y"), EF::createDecisionVariable("z")];
        $objFuncVars = [new VariableCoefficientPair("3", $vars[0]), new VariableCoefficientPair("4", $vars[1]),
                        new VariableCoefficientPair("10", $vars[2])];
        $objectiveFunction = new ObjectiveFunction($objFuncVars, "P", ObjectiveFunctionType::MAXIMIZING);
        $this->assertTrue($objectiveFunction->getVariablePairs()[0]->getVariable() === $vars[0]);

        // Great, we have an objective function. Now we need some constraints
        $constraints = [];
        $constraints[] = new SlackConstraint([new VariableCoefficientPair("1", $vars[0]),
                                              new VariableCoefficientPair("2", $vars[1]),
                                              new VariableCoefficientPair("2", $vars[2])], "r", "100");
        $constraints[] = new SlackConstraint([new VariableCoefficientPair("1", $vars[0]),
                                              new VariableCoefficientPair("4", $vars[2])], "s", "40");
        // Now we can make our Problem instance
        $problem = new Problem($constraints, $objectiveFunction);
        (new MaximizingSolver())->solve($problem);
        $this->checkSolutions($problem->getOptimum(), ["r" => "0", "s" => "0", "x" => "0", "z" => "10", "P" => "260", "y" => "40"]);
    }

    /**
     * Test the solver with another alternative example problem from the textbook.
     * Exercise 4B, Question 3.
     * @return void
     */
    public function testExampleProblemThree() {
        $vars = [EF::createDecisionVariable("x"), EF::createDecisionVariable("y"), EF::createDecisionVariable("z")];
        $objFuncVars = [new VariableCoefficientPair("3", $vars[0]), new VariableCoefficientPair("5", $vars[1]),
                        new VariableCoefficientPair("2", $vars[2])];
        $objectiveFunction = new ObjectiveFunction($objFuncVars, "P", ObjectiveFunctionType::MAXIMIZING);
        $this->assertTrue($objectiveFunction->getVariablePairs()[0]->getVariable() === $vars[0]);

        // Great, we have an objective function. Now we need some constraints
        $constraints = [];
        $constraints[] = new SlackConstraint([new VariableCoefficientPair("3", $vars[0]),
                                              new VariableCoefficientPair("4", $vars[1]),
                                              new VariableCoefficientPair("5", $vars[2])], "r", "10");

        $constraints[] = new SlackConstraint([new VariableCoefficientPair("1", $vars[0]),
                                              new VariableCoefficientPair("3", $vars[1]),
                                              new VariableCoefficientPair("10", $vars[2])], "s", "5");

        $constraints[] = new SlackConstraint([new VariableCoefficientPair("1", $vars[0]),
                                              new VariableCoefficientPair("-2", $vars[1])], "t", "1");
        // Now we can make our Problem instance
        $problem = new Problem($constraints, $objectiveFunction);
        (new MaximizingSolver())->solve($problem);
        $this->checkSolutions($problem->getOptimum(), ["r" => "0", "s" => "0", "t" => "0", "x" => "2", "z" => "0", "P" => "11", "y" => "1", "t" => "1"]);
    }

    /**
     * @param OptimumValue[] $solutions
     * @param array $actual
     */
    private function checkSolutions(array $solutions, array $actual) {
        //$this->assertNotEmpty($solutions);
        foreach ($solutions as $solution) {
            $actualSolution = $actual[$solution->getVariable()->getIdentifier()];
            $this->assertTrue(bccomp($actualSolution, $solution->getOptimumValue(), 10) === 0);
        }
    }

}
