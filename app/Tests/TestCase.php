<?php
namespace AdamWilliams\DMLT\Tests;

/**
 * Bootstrap class shipped with framework.
 * I've made some small modifications due to change in directory structure.
 * @package AdamWilliams\DMLT\Tests
 */
class TestCase extends \Illuminate\Foundation\Testing\TestCase {

    /**
         * Creates the application.
         *
         * @return \Symfony\Component\HttpKernel\HttpKernelInterface
    */
    public function createApplication() {
            $unitTesting = true;
            $testEnvironment = 'testing';
            return require __DIR__ . '/../../bootstrap/app.php'; // ensures everything is autoloaded for PHPUnit
    }
}
