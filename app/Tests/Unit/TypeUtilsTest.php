<?php


namespace AdamWilliams\DMLT\Tests\Unit;


use AdamWilliams\DMLT\Tests\TestCase;
use AdamWilliams\DMLT\Util\TypeUtils;

/**
 * tests for the TypeUtils class.
 * @package AdamWilliams\DMLT\Tests\Unit
 */
class TypeUtilsTest extends TestCase {

    public function testCheckTypeWithValidArray() {
        $array = [new TestClass1(), new TestClass1()];
        TypeUtils::checkTypeOfArray($array, "AdamWilliams\\DMLT\\Tests\\Unit\\TestClass1");
    }

    /**
     * Tests the checkTypeOfArray function with an array containing multiple types.
     * @expectedException \AdamWilliams\DMLT\Exceptions\InvalidTypeException
     */
    public function testCheckTypeWithInvalidArray() {
        $array = [new TestClass1(), new TestClass1(), new TestClass2()];
        TypeUtils::checkTypeOfArray($array, "AdamWilliams\\DMLT\\Tests\\Unit\\TestClass1");
    }

    public function ensureIUnderstandHowPhpWorks() {
        $tc1 = new TestClass1();
        $tc2 = new TestClass1();
        $this->assertFalse($tc1 === $tc2);
        $this->assertFalse(spl_object_hash($tc1) === spl_object_hash($tc2));
    }

    public function testCheckTypeForSingleValidVariable() {
        $tc1 = new TestClass1();
        TypeUtils::checkTypeOfVariable($tc1, "AdamWilliams\\DMLT\\Tests\\Unit\\TestClass1");
    }

    /**
     * Tests the checkTypeOfVariable function with an invalid variable.
     * @expectedException \AdamWilliams\DMLT\Exceptions\InvalidTypeException
     */
    public function testCheckTypeForSingleInvalidVariable() {
        $tc1 = new TestClass1();
        TypeUtils::checkTypeOfVariable($tc1, "AdamWilliams\\DMLT\\Tests\\Unit\\TestClass2");
    }
}

class TestClass1 {

}

class TestClass2 {

}