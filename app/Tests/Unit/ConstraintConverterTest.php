<?php


namespace AdamWilliams\DMLT\Tests\Unit;


use AdamWilliams\DMLT\LinearProgramming\Entities\Constraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackConstraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Entities\VariableCoefficientPair;
use AdamWilliams\DMLT\LinearProgramming\Enums\ConstraintType;
use AdamWilliams\DMLT\LinearProgramming\Util\ConstraintConverter;
use AdamWilliams\DMLT\Tests\TestCase;

class ConstraintConverterTest extends TestCase {

    public function testConstraintConverter() {
        // ARRANGE
        $vars = [new Variable("x"), new Variable("y"), new Variable("z")];
        $vars = [new VariableCoefficientPair("5", $vars[0]), new VariableCoefficientPair("10", $vars[0]),
                 new VariableCoefficientPair("15", $vars[0])];
        $originalConstraint = new Constraint($vars, ConstraintType::LT_EQUAL_TO, "5");
        // ACT
        $slackConstraint = (new ConstraintConverter())->convertConstraintToSlack($originalConstraint, "r");
        // ASSERT
        $this->assertTrue($slackConstraint instanceof SlackConstraint);
        $this->assertTrue($slackConstraint->getSlackVariable()->getIdentifier() === "r");
        $this->assertTrue($slackConstraint->getVariables() === $vars);
    }

} 