<?php

namespace AdamWilliams\DMLT\Tests\Integration;

use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveFunction;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Enums\ObjectiveFunctionType;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotCellProcessor;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotColumnProcessor;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Cell;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Row;
use AdamWilliams\DMLT\LinearProgramming\Tableau\Tableau;
use AdamWilliams\DMLT\Tests\TestCase;

class PivotCellProcessorTest extends TestCase {

    /**
     * Ensure the PivotCellProcessor correctly identifies the pivot cell.
     */
    public function testPivotCellIsCorrectlyPicked() {
        // ARRANGE
        $problem = new Problem([], new ObjectiveFunction([], "P", ObjectiveFunctionType::MAXIMIZING));
        $rows = [];
        $pivotColCellRef = new Variable("abc123");
        $pivotCell = new Cell($pivotColCellRef, 5);
        $pivotRowRef = new Variable("test");
        $rows[] = new Row([new Cell(new Variable("000abc"), 5), $pivotCell,
                           new Cell(new Variable("def456"), 5)], $pivotRowRef);

        $rows[] = new Row([new Cell(new Variable("08576gk"), 5), new Cell($pivotColCellRef, 5),
                           new Cell(new Variable("89045jk"), 5)], new Variable("test2"));

        $rows[] = new Row([new Cell(new Variable("060glfkj"), 5), new Cell($pivotColCellRef, 5),
                           new Cell(new Variable("af94jhcj"), 5)], new Variable("test3"));
        $tableau = new Tableau($rows, "Testing tableau.", "");
        $tableau->setPivotColRef($pivotColCellRef);
        $tableau->setPivotRowRef($pivotRowRef);
        $problem->addTableau($tableau);
        // ACT
        $newProblem = (new PivotCellProcessor())->fire($problem);

        // ASSERT
        $tableaux = $newProblem->getTableaux();
        $this->assertSame($pivotCell, $tableaux[count($tableaux)-1]->getPivotCell());
    }
}
