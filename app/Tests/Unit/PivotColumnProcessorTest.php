<?php

namespace AdamWilliams\DMLT\Tests\Integration;

use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveFunction;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Enums\ObjectiveFunctionType;
use AdamWilliams\DMLT\LinearProgramming\Processors\PivotColumnProcessor;
use AdamWilliams\DMLT\Tests\TestCase;

class PivotColumnProcessorTest extends TestCase {

    /**
     * Ensure an empty Problem class causes an exception to be thrown.
     * @expectedException \AdamWilliams\DMLT\Exceptions\InvalidProblemException
     * @return void
     */
    public function testInvalidProblemWithNoTableaux() {
        $problem = new Problem([], new ObjectiveFunction([], "P", ObjectiveFunctionType::MAXIMIZING));
        $processor = new PivotColumnProcessor();
        $processor->fire($problem);
    }
    /**
     * Ensure an invalid Problem class causes an exception to be thrown.
     * @expectedException \AdamWilliams\DMLT\Exceptions\InvalidProblemException
     * @return void
     */
    public function testInvalidProblemWithNullTableaux() {
        $problem = new Problem([], new ObjectiveFunction([], "P", ObjectiveFunctionType::MAXIMIZING));

        $processor = new PivotColumnProcessor();
        $processor->fire($problem);
    }

}