<?php


namespace AdamWilliams\DMLT\Tests\Unit;


use AdamWilliams\DMLT\Tests\TestCase;
use AdamWilliams\DMLT\TravellingSalesman\GraphNode;
use AdamWilliams\DMLT\TravellingSalesman\GraphArc;

class MatrixEntryTest extends TestCase {

    /**
     * @expectedException \AdamWilliams\DMLT\Exceptions\InvalidArcException
     */
    public function testArcsConnectingTheSameNodeThrowsException() {
        $nodeA = new GraphNode("A");
        $entryAA = new GraphArc($nodeA, $nodeA, 7);

    }

} 