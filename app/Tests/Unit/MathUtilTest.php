<?php


namespace AdamWilliams\DMLT\Tests\Unit;


use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveFunction;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackConstraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Entities\VariableCoefficientPair;
use AdamWilliams\DMLT\LinearProgramming\Enums\ObjectiveFunctionType;
use AdamWilliams\DMLT\Tests\TestCase;
use AdamWilliams\DMLT\Util\MathUtil;

/**
 * Tests for the MathUtil class.
 * @package AdamWilliams\DMLT\Tests\Unit
 */
class MathUtilTest extends TestCase {

    public function testStringsMadeCorrectly() {
        $this->assertEquals("+ 2x", MathUtil::getStringFromVariablePair(2, "x"));
        $this->assertEquals("+ x", MathUtil::getStringFromVariablePair(1, "x"));
        $this->assertEquals("- x", MathUtil::getStringFromVariablePair(-1, "x"));
        $this->assertEquals("- 4y", MathUtil::getStringFromVariablePair(-4, "y"));
        $this->assertEquals("-4y", MathUtil::getStringFromVariablePair(-4, "y", false));
        $this->assertEquals("-y", MathUtil::getStringFromVariablePair(-1, "y", false));
    }


    public function testOrdinalSuffixFunction() {
        $this->assertEquals("st", MathUtil::getOrdinalSuffix(1));
    }

    public function testGetStringForMultipleVariables() {
        $this->assertEquals("2x + y - 7z", MathUtil::getStringForMultipleVariables(["x" => 2, "y" => 1, "z" => -7]));
    }

    public function testConvertSlackToString() {
        // arrange
        $xPair = new VariableCoefficientPair("-2", new Variable("x"));
        $yPair = new VariableCoefficientPair("1", new Variable("y"));
        $zPair = new VariableCoefficientPair("-7", new Variable("z"));
        $constraint = new SlackConstraint([$xPair, $yPair, $zPair], "r", "21");
        // act
        $str = MathUtil::convertSlackToString($constraint);
        // assert
        $this->assertEquals("-2x + y - 7z + r = 21", $str);
    }

    public function testConvertSlackToStringWithOld() {
        // arrange
        $xPair = new VariableCoefficientPair("-2", new Variable("x"));
        $yPair = new VariableCoefficientPair("1", new Variable("y"));
        $zPair = new VariableCoefficientPair("-7", new Variable("z"));
        $constraint = new SlackConstraint([$xPair, $yPair, $zPair], "r", "21");
        // act
        $str = MathUtil::convertSlackToString($constraint, true);
        // assert
        $this->assertEquals("-2x + y - 7z ≤ 21 → -2x + y - 7z + r = 21", $str);
    }

    public function testConvertObjectiveFunctionToString() {
        // arrange
        $xPair = new VariableCoefficientPair("2", new Variable("x"));
        $yPair = new VariableCoefficientPair("1", new Variable("y"));
        $zPair = new VariableCoefficientPair("-7", new Variable("z"));
        $objFunc = new ObjectiveFunction([$xPair, $yPair, $zPair], "P", ObjectiveFunctionType::MAXIMIZING);
        // act
        $str = MathUtil::convertObjectiveFunctionToString($objFunc);
        // assert
        $this->assertEquals("P - 2x - y + 7z = 0", $str);
    }

    public function testConvertObjectiveFunctionToStringWithOld() {
        // arrange
        $xPair = new VariableCoefficientPair("2", new Variable("x"));
        $yPair = new VariableCoefficientPair("1", new Variable("y"));
        $zPair = new VariableCoefficientPair("-7", new Variable("z"));
        $objFunc = new ObjectiveFunction([$xPair, $yPair, $zPair], "P", ObjectiveFunctionType::MAXIMIZING);
        // act
        $str = MathUtil::convertObjectiveFunctionToString($objFunc, true);
        // assert
        $this->assertEquals("P = 2x + y - 7z → P - 2x - y + 7z = 0", $str);
    }
}
