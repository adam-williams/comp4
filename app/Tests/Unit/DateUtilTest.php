<?php


namespace AdamWilliams\DMLT\Tests\Unit;


use AdamWilliams\DMLT\Tests\TestCase;
use AdamWilliams\DMLT\Util\DateUtil;

class DateUtilTest extends TestCase {

    public function testGetTimeElapsedMinutes() {
        $time = 120;
        $this->assertEquals("2 minutes", DateUtil::getTimeElapsed($time));
    }

    public function testGetTimeElapsedMinutesSeconds() {
        $time = 125;
        $this->assertEquals("2 minutes, 5 seconds", DateUtil::getTimeElapsed($time));
    }

    public function testGetTimeElapsedHoursMinutes() {
        $time = (6 * 60 * 60) + 120;
        $this->assertEquals("6 hours, 2 minutes", DateUtil::getTimeElapsed($time));
    }

} 