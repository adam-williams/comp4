<?php


namespace AdamWilliams\DMLT\Tests\Unit;


use AdamWilliams\DMLT\Tests\TestCase;
use AdamWilliams\DMLT\Util\BcUtils;
use AdamWilliams\DMLT\Util\DecToFracUtil;
use AdamWilliams\DMLT\Util\TypeUtils;

/**
 * Tests for the BcUtils class.
 * @package AdamWilliams\DMLT\Tests\Unit
 */
class BcUtilsTest extends TestCase {

    public function testBcRound() {
        $this->assertSame("1", BcUtils::bcRound("0.99", 0));
        $this->assertSame("1", BcUtils::bcRound("0.5", 0));
        $this->assertSame("1.5", BcUtils::bcRound("1.45", 1));
    }

    public function testBcFloor() {
        $this->assertSame("7", BcUtils::bcFloor("7.23239845698"));
        $this->assertSame("9", BcUtils::bcFloor("9"));
    }

    public function testBcCeil() {
        $this->assertSame("8", BcUtils::bcCeil("8"));
        $this->assertSame("10", BcUtils::bcCeil("9.19"));
    }
}