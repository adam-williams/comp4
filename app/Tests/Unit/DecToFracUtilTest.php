<?php
namespace AdamWilliams\DMLT\Tests\Unit;

use AdamWilliams\DMLT\Tests\TestCase;
use AdamWilliams\DMLT\Util\DecToFracUtil;

/**
 * Tests for the DecToFracUtil class.
 * @package AdamWilliams\DMLT\Tests\Unit
 */
class DecToFracUtilTest extends TestCase {

    public function testConvertTwoThirds() {
        $dc = new DecToFracUtil("0.666666666");
        $this->assertSame("2/3", $dc->convert());
    }

    public function testConvertFiveNineteenths() {
        $dc = new DecToFracUtil("0.263157894");
        $this->assertSame("5/19", $dc->convert());
    }

    public function testConvertOneHundredth() {
        $dc = new DecToFracUtil("0.01");
        $this->assertSame("1/100", $dc->convert());
    }

    public function testConvertFiftyThreeNines() {
        $dc = new DecToFracUtil("5.88888888");
        $this->assertSame("53/9", $dc->convert());
    }

    public function testConvertSevenThirtyTwoths() {
        $dc = new DecToFracUtil("0.21875");
        $this->assertSame("7/32", $dc->convert());
    }

    public function testConvertThreeTenths() {
        $dc = new DecToFracUtil("0.3000000000");
        $this->assertSame("3/10", $dc->convert());
    }

    public function testConvertWithHtml() {
        $dc = new DecToFracUtil("0.3000000000");
        $this->assertSame("<sup>3</sup>/<sub>10</sub>", $dc->convert(true));
    }

}
