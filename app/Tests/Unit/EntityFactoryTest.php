<?php

namespace AdamWilliams\DMLT\Tests\Unit;

use AdamWilliams\DMLT\LinearProgramming\Entities\Variable;
use AdamWilliams\DMLT\LinearProgramming\Factories\EntityFactory as EF; // alias for convenience
use AdamWilliams\DMLT\Tests\TestCase;

/**
 * Testing class to test EntityFactory object creation.
 * @see EntityFactory
 * @package AdamWilliams\DMLT\Tests\Unit
 */
class EntityFactoryTest extends TestCase {

    /**
     * Ensure decision variable object creation works.
     *
     * @return void
     */
    public function testCreateDecisionVariable() {
        $variable = EF::createDecisionVariable("x");
        $this->assertTrue($variable instanceof Variable, "Returned object is instance of DecisionVariable class.");
        $this->assertTrue($variable->getIdentifier() === "x", "Returned object has correctly set identifier.");
    }

}
