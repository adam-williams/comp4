<?php


namespace AdamWilliams\DMLT\Tests\Unit;


use AdamWilliams\DMLT\LinearProgramming\Enums\Enum;
use AdamWilliams\DMLT\Tests\TestCase;

/**
 * Tests the Enum abstract class.
 * @package AdamWilliams\DMLT\Tests\Unit
 */
class EnumTest extends TestCase {

    public function testIsValidWithValidValue() {
        $this->assertTrue(TestEnum::isValid(TestEnum::APPLE));
    }

    public function testIsValidWithInvalidValue() {
        $this->assertFalse(TestEnum::isValid(-1));
    }

    public function testAssertValidWithValidValue() {
        TestEnum::assertValid(TestEnum::APPLE);
    }

    /**
     * @expectedException \AdamWilliams\DMLT\Exceptions\InvalidEnumValueException
     */
    public function testAssertValidWithInvalidValue() {
        TestEnum::assertValid(-1);
    }
}

class TestEnum extends Enum {
    const APPLE = 0;
    const DOG = 1;
}