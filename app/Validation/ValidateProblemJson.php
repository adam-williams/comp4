<?php

namespace AdamWilliams\DMLT\Validation;

use Google\Authenticator\GoogleAuthenticator;
use Illuminate\Validation\Validator;
use Config;

class ValidateProblemJson extends Validator {

    public function handle($attribute, $value, $parameters) {
        json_decode($value);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
