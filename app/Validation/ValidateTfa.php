<?php

namespace AdamWilliams\DMLT\Validation;

use Google\Authenticator\GoogleAuthenticator;
use Illuminate\Validation\Validator;
use Config;

class ValidateTfa extends Validator {

    public function handle($attribute, $value, $parameters) {
        // This does ABSOLUTELY NOTHING to prevent replay attacks
        // Since we're serving this over HTTP anyway, I see no point in adding extra code to mitigate the above issue
        // This application isn't important enough and we aren't storing personal info or anything sensitive enough
        // to warrant in-depth security measures.

        if (strlen($value) !== 6) {
            return false;
        }
        $ga = new GoogleAuthenticator();
        return $ga->checkCode(Config::get("2fa.secret"), $value);
    }
}
