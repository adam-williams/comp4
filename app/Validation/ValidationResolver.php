<?php

namespace AdamWilliams\DMLT\Validation;

use Google\Authenticator\GoogleAuthenticator;
use Illuminate\Validation\Validator;
use Config;

class ValidationResolver extends Validator {



    /**
     * Handles all validation calls.
     *
     * @param string $name The method name.
     * @param array $args The arguments.
     * @return mixed|void
     * @throws \ReflectionException
     */
    public function __call($name, $args) {
        $rule = "\\Validate" . str_replace("validate", "", $name);
        $validator = $this->container->make(__NAMESPACE__ . $rule, [$this->translator, $this->customAttributes, $this->data, $this->rules, $this->customAttributes]);
        $ro = new \ReflectionObject($validator);
        if (!$ro->hasMethod("handle")) {
            throw new \ReflectionException("No such method handle on validator!");
        }
        $method = $ro->getMethod("handle");
        return $method->invokeArgs($validator, $args);
    }
}
