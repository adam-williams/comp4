<?php

namespace AdamWilliams\DMLT\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request that handles 2FA input for the dev server.
 * @package AdamWilliams\DMLT\Http\Requests
 */
class DevAuthRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "code" => "required|numeric|tfa"
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     * Since everyone needs to be able to login, this is set to true.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
