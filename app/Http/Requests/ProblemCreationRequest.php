<?php

namespace AdamWilliams\DMLT\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request that handles new problem creation.
 * @package AdamWilliams\DMLT\Http\Requests
 */
class ProblemCreationRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "json_data" => "required|problem_json"
        ];
    }

    public function getData() {
        return json_decode($this->input("json_data"));
    }

    /**
     * Determine if the user is authorized to make this request.
     * Since everyone needs to be able to login, this is set to true.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
