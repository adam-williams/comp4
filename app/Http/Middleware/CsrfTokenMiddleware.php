<?php
namespace AdamWilliams\DMLT\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Input;
use Session;

/**
 * Middleware to prevent cross-site request forgery.
 * @package AdamWilliams\DMLT\Middleware
 */
class CsrfTokenMiddleware {

    public function handle(Request $request, Closure $next) {
        //die("Token: " . Session::token() . " Sent: " . Input::get('_token'));
        if ($request->method() == "POST" && Session::token() !== Input::get('_token')) {
            //throw new TokenMismatchException;
        }
        return $next($request);
    }
}