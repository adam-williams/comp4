<?php
namespace AdamWilliams\DMLT\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Redirect;

/**
 * Middleware to prevent logged in users from accessing pages meant for guests.
 * @package AdamWilliams\DMLT\Middleware
 */
class GuestMiddleware {
    /**
     * @var Guard
     */
    private $auth;

    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle(Request $request, Closure $next) {
        if ($this->auth->check()) {
            return Redirect::route("home");
        }
        return $next($request);
    }
}