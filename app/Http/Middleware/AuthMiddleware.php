<?php
namespace AdamWilliams\DMLT\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Redirect;
use Session;

/**
 * Middleware to prevent guests accessing content made for logged in users.
 * @package AdamWilliams\DMLT\Middleware
 */
class AuthMiddleware {
    /**
     * @var Guard
     */
    private $auth;

    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle(Request $request, Closure $next) {
        if (!$this->auth->check()) {
            Session::flash("last-page", $request->url());
            return Redirect::route("login");
        }
        return $next($request);
    }
}