<?php
namespace AdamWilliams\DMLT\Http\Middleware;

use AdamWilliams\DMLT\Contracts\Auth\DevAuthenticationInterface;
use Closure;
use Illuminate\Http\Request;
use Redirect;


/**
 * Middleware to prevent unauthorized access to the dev server.
 * @package AdamWilliams\DMLT\Middleware
 */
class DevAuthMiddleware {

    /**
     * @var DevAuthenticationInterface
     */
    private $devAuth;

    /**
     * @param DevAuthenticationInterface $devAuth
     */
    public function __construct(DevAuthenticationInterface $devAuth) {
        $this->devAuth = $devAuth;
    }

    public function handle(Request $request, Closure $next) {
        if (false && !$this->devAuth->isAuthenticated()) {
            return Redirect::route("2fa");
        }
        return $next($request);
    }
}
