<?php
namespace AdamWilliams\DMLT\Http\Middleware;

use App;
use Closure;
use Illuminate\Http\Request;
use Response;

/**
 * Middleware to prevent pages from being displayed while we're in maintenance mode.
 * @package AdamWilliams\DMLT\Middleware
 */
class MaintenanceMiddleware {

    public function handle(Request $request, Closure $next) {
        if (App::isDownForMaintenance()) {
            return Response::view("errors.maintenance", ["deployLastTime" => "Maintenance"], 503);
        }
        return $next($request);
    }
}