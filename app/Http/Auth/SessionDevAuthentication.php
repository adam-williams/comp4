<?php

namespace AdamWilliams\DMLT\Http\Auth;

use AdamWilliams\DMLT\Contracts\Auth\DevAuthenticationInterface;
use Illuminate\Session\Store;

class SessionDevAuthentication implements DevAuthenticationInterface {
    /**
     * @var Store The laravel session manager.
     */
    private $sessionStore;

    /**
     * @param Store $sessionStore The laravel session manager.
     */
    public function __construct(Store $sessionStore) {
        $this->sessionStore = $sessionStore;
    }

    public function isAuthenticated() {
        return $this->sessionStore->has("2fa-auth");
    }

    public function setAuthenticated($authed = true) {
        if ($authed) {
            $this->sessionStore->set("2fa-auth", true);
        } else {
            $this->sessionStore->forget("2fa-auth");
        }
    }
}