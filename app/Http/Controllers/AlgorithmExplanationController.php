<?php

namespace AdamWilliams\DMLT\Http\Controllers;

use View;

class AlgorithmExplanationController extends BaseController {

    public function showAlgorithmExplanation() {
        $this->setPageTitle("The Simplex algorithm");
        $this->setActiveItem("explain");
        return View::make("pages.algorithm");
    }
}
