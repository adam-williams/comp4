<?php

namespace AdamWilliams\DMLT\Http\Controllers;
use AdamWilliams\DMLT\UserInterface\NavItem;
use App;
use Blade;
use Cache;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use View;

/**
 * BaseController class we can use for shared logic.
 * Note that since we're using Blade we don't need a setupLayout function.
 */
class BaseController extends Controller {


    use ValidatesRequests;


    /**
     * @var NavItem[] The navigation items.
     */
    private $navItems = [];
    private $pageTitle = "Unknown page";
    protected $auth;

    public function __construct(Guard $au, $csrf=true) {
        $this->auth = $au;
        $this->navItems = $this->getNavigation();
        $this->shareNavigation();
        if ($csrf) {
            $this->middleware('csrf', ['on' => ['post']]);
        }
    }

    /**
     * @return NavItem[] The array of navigation items.
     */
    protected function getNavigation() {

        $nav = array(
            new NavItem("Home", "home"),
            new NavItem("Solver", "solve"),
            new NavItem("Problem generator", "generator"),
            new NavItem("About Simplex", "explain")
        );
        if ($this->auth->check()) {
            $nav[] = new NavItem("My account", "account/settings", true);
            $nav[] = new NavItem("Logout", "logout", true);
        } else {
            $nav[] = new NavItem("Login", "login", true);
            $nav[] = new NavItem("Register", "register", true);
        }
        return $nav;

    }

    /**
     * @param string $routeName The route name to set as active.
     */
    protected function setActiveItem($routeName) {
        foreach ($this->navItems as $navItem) {
            if ($navItem->getRouteName() === $routeName) {
                $navItem->setActive(true);
            }
        }
        $this->shareNavigation();
    }

    protected function setNavigation(array $nav) {
        $this->navItems = $nav;
        $this->shareNavigation();
    }

    protected function setPageTitle($title) {
        $this->pageTitle = $title;
        $this->sharePageTitle();
    }

    /**
     * Shares the current page title with the views.
     */
    private function sharePageTitle() {
        View::share("title", $this->pageTitle);
        View::share("deployLastTime", Cache::get("last-deploy", "Never"));
    }

    /**
     * Shares the current nav array with the views.
     */
    protected function shareNavigation() {
        View::share("nav", $this->filterNavigation($this->navItems, false));
        View::share("navSecondary", $this->filterNavigation($this->navItems, true));
    }

    /**
     * Middleware the navigation item array.
     * @param Navitem[] $items The items to perform the filter operation on.
     * @param bool $secondary Whether or not to filter to secondary only items as opposed to primary items.
     * @return Navitem[] The filtered array of items.
     */
    private function filterNavigation(array $items, $secondary=true) {
        return array_filter($items, function($item) use ($secondary) {
            /** @var $item NavItem */
            return (($secondary && $item->isSecondary()) || (!$secondary && !$item->isSecondary()));
        });
    }

}
