<?php

namespace AdamWilliams\DMLT\Http\Controllers;

use App;
use Illuminate\Auth\Guard;
use Queue;
use Response;

class DeployController extends BaseController {

    public function __construct(Guard $auth) {
        parent::__construct($auth, false); // explicitly turn off CSRF protection
    }

    public function performDeployment() {
        // We don't want any of the deployment logic to hold up the HTTP request
        // since we can't guarantee that the client will remain connected
        // To combat this, we're going to use the queue system that ships with Laravel
        // which will ensure that our logic is run separately by a beanstalkd queue worker
        $data = [];
        if (App::isLocal()) {
            $data['no-pull'] = 1;
        }
        /** @see AdamWilliams\DMLT\QueueJobs\DeploymentJob */
        Queue::push("AdamWilliams\\DMLT\\QueueJobs\\DeploymentJob", $data);
        return Response::json("Deployment queued.");
    }
}
