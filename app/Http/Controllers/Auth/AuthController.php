<?php namespace AdamWilliams\DMLT\Http\Controllers\Auth;

use AdamWilliams\DMLT\Http\Controllers\BaseController;
use AdamWilliams\DMLT\Http\Requests\Auth\LoginRequest;
use AdamWilliams\DMLT\Http\Requests\Auth\RegisterRequest;
use Illuminate\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthController extends BaseController {


    // For a variety of reasons we can't use Laravel's trait here.

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * The registrar implementation.
     *
     * @var Registrar
     */
    protected $registrar;

    /**
     * Create a new authentication controller instance.
     *
     * @param  Guard $auth
     * @param  Registrar $registrar
     * @return AuthController
     */
    public function __construct(Guard $auth, Registrar $registrar) {
        parent::__construct($auth, true);
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Show the application registration form.
     *
     * @return Response
     */
    public function getRegister() {
        $this->setPageTitle("Register");
        $this->setActiveItem("register");
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param Request $request
     * @return Response
     */
    public function postRegister(Request $request) {
        $validator = $this->registrar->validator($request->all()); // doesn't return void :/

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->auth->login($this->registrar->create($request->all()));

        return redirect($this->redirectPath());
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin() {
        $this->setPageTitle("Login");
        $this->setActiveItem("login");
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param LoginRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(LoginRequest $request) {

        $credentials = $request->only('email', 'password');

        if ($this->auth->attempt($credentials, $request->has('remember'))) {
            return redirect()->intended($this->redirectPath());
        }

        return redirect('/auth/login')
            ->withInput($request->only('email'))
            ->withErrors([
                'email' => 'These credentials do not match our records.',
            ]);
    }

    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    public function getLogout() {
        $this->auth->logout();

        return redirect('/');
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath() {
        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }
}
