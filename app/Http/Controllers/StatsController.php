<?php

namespace AdamWilliams\DMLT\Http\Controllers;

use AdamWilliams\DMLT\Eloquent\Entities\Statistic;
use AdamWilliams\DMLT\Util\DateUtil;
use View;

class StatsController extends BaseController {

    public function showStats() {
        $this->setPageTitle("Stats");

        $stats = Statistic::all()->lists("value", "key");
        $time = DateUtil::getTimeElapsed($stats[Statistic::$TABLEAUX_CREATED] * 105);
        return View::make("pages.stats", ["stats" => $stats, "time" => $time]);
    }
}
