<?php

namespace AdamWilliams\DMLT\Http\Controllers;

use View;

class HomeController extends BaseController {

    public function showHome() {
        $this->setPageTitle("Home");
        $this->setActiveItem("home");
        return View::make("pages.home");
    }
}
