<?php
namespace AdamWilliams\DMLT\Http\Controllers;

use AdamWilliams\DMLT\Contracts\Repositories\UserRepositoryInterface;
use Illuminate\Contracts\Auth\Authenticator;

class AccountController extends BaseController {

    public function __construct(Authenticator $auth) {
        parent::__construct($auth);
        $this->middleware("AuthFilter");
    }

    public function getSettings(UserRepositoryInterface $users) {
        $this->setActiveItem("account/settings");
        $this->setPageTitle("My account");
        $user = $users->getUserById($this->auth->user()->getAuthIdentifier());
        return view("auth.account", ["user" => $user]);
    }

} 