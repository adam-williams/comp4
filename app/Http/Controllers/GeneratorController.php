<?php

namespace AdamWilliams\DMLT\Http\Controllers;

use AdamWilliams\DMLT\Eloquent\Entities\Statistic;
use AdamWilliams\DMLT\Util\DateUtil;
use View;

class GeneratorController extends BaseController {

    public function showGenerator() {
        $this->setPageTitle("Problem generator");
        $this->setActiveItem("generator");
        return view("pages.problem-generator", []);
    }
}
