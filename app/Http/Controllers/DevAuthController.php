<?php

namespace AdamWilliams\DMLT\Http\Controllers;

use AdamWilliams\DMLT\Contracts\Auth\DevAuthenticationInterface;
//use AdamWilliams\DMLT\Http\Requests\DevAuthRequest;
use AdamWilliams\DMLT\Http\Requests\DevAuthRequest;
use AdamWilliams\DMLT\UserInterface\NavItem;
use App;
use Google\Authenticator\GoogleAuthenticator;
use Redirect;
use Request;
use Response;
use View;

/**
 * This controller is not critical to application functionality.
 *
 * When developing the app, we expose the development server to the internet.
 * To ensure only people we expect to have access to the application have access, we use TOTP.
 * @package AdamWilliams\DMLT\Http\Controllers
 */
class DevAuthController extends BaseController {

    public function showChallenge() {
        $this->setNavigation([new NavItem("2FA", "2fa")]);
        $this->setPageTitle("2FA Challenge");
        $this->setActiveItem("2fa");
        return Response::view("errors.2fa");
    }

    public function checkCode(DevAuthRequest $req, DevAuthenticationInterface $devAuth) {
        $devAuth->setAuthenticated(true);
        return Redirect::route("home");
    }

    public function checkIp(DevAuthenticationInterface $devAuth) {
        $whiteListedIps = ["127.0.0.1", "84.92.172.209", "86.53.134.242", "192.168.0.79"];
        if (App::isLocal()) {
            $ip = Request::ip();
        } else {
            $ip = Request::header("HTTP_X_REAL_IP", Request::ip());
        }
        /*if (!in_array($ip, $whiteListedIps)) {
            return Redirect::route("2fa");
        }*/
        $devAuth->setAuthenticated(true);
        return Redirect::route("home");
    }

    public function generateSecret() {
        $this->setNavigation([new NavItem("2FA", "2fa"), new NavItem("2FA Gen", "2fa-gen")]);
        $this->setActiveItem("2fa-gen");
        $this->setPageTitle("2FA Gen");
        $ga = new GoogleAuthenticator();
        $secret = $ga->generateSecret();
        return View::make("dev.2fa-gen", ["secret" => $secret, "url" => $ga->getUrl("adam", "dmlt", $secret)]);
    }
}
