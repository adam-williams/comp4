<?php

namespace AdamWilliams\DMLT\Http\Controllers;

use AdamWilliams\DMLT\Eloquent\Entities\Statistic;
use AdamWilliams\DMLT\Http\Requests\ProblemCreationRequest;
use AdamWilliams\DMLT\LinearProgramming\Entities\Constraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\ObjectiveFunction;
use AdamWilliams\DMLT\LinearProgramming\Entities\Problem;
use AdamWilliams\DMLT\LinearProgramming\Entities\SlackConstraint;
use AdamWilliams\DMLT\LinearProgramming\Entities\VariableCoefficientPair;
use AdamWilliams\DMLT\LinearProgramming\Enums\ConstraintType;
use AdamWilliams\DMLT\LinearProgramming\Enums\ObjectiveFunctionType;
use AdamWilliams\DMLT\LinearProgramming\Solver\MaximizingSolver;
use AdamWilliams\DMLT\LinearProgramming\Util\ArrayTableauDrawer;
use AdamWilliams\DMLT\LinearProgramming\Util\ConstraintConverter;
use AdamWilliams\DMLT\Tests\Unit\ConstraintConverterTest;
use AdamWilliams\DMLT\Util\MathUtil;
use Illuminate\Http\JsonResponse;
use View;
use AdamWilliams\DMLT\LinearProgramming\Factories\EntityFactory as EF;


class ProblemCreationController extends BaseController {

    public function showForm() {
        $this->setPageTitle("Solve a problem");
        $this->setActiveItem("solve");
        return View::make("pages.new-problem");
    }

    private function getConstraintIdentifier($number) {
        $standard = ["r", "s", "t", "u", "v", "w"];
        if (array_key_exists($number, $standard)) {
            return $standard[$number];
        } else {
            return "s" . (($number + 1) - count($standard));
        }
    }

    public function addProblem(ProblemCreationRequest $req) {
        $data = $req->getData();
        return $this->showProblemSolutions($req);

    }

    public function showProblemSolutions(ProblemCreationRequest $req) {
        $data = $req->getData();
        $vars = [];
        foreach ($data->vars as $var) {
            $vars[$var] = EF::createDecisionVariable($var);
        }
        $constraints = [];
        $cc = new ConstraintConverter();
        $objArray = [];
        foreach ($vars as $letter => $var) {
            if ($data->objective->{$letter} !== null && $data->objective->{$letter} != 0) {
                $objArray[] = new VariableCoefficientPair($data->objective->{$letter}, $var);
            }
        }
        $objFunc = new ObjectiveFunction($objArray, "P", ObjectiveFunctionType::MAXIMIZING);
        $n = 0;
        foreach ($data->constraints as $item) {
            $cArray = [];
            foreach ($vars as $letter => $var) {
                if (property_exists($item, $letter) && $item->{$letter} !== null && $item->{$letter} != 0) {
                    $cArray[] = new VariableCoefficientPair($item->{$letter}, $var);
                }
            }
            $constraint = new Constraint($cArray, ConstraintType::LT_EQUAL_TO, $item->rhs);
            $slack = $cc->convertConstraintToSlack($constraint, $this->getConstraintIdentifier($n));
            $constraints[] = $slack;

            $n++;
        }
        $problem = new Problem($constraints, $objFunc);
        $problem->setReduced($req->has("reduced"));
        $s = new MaximizingSolver();
        $s->solve($problem);
        $this->addStats($problem);
        $this->setPageTitle("View workings");
        $this->setActiveItem("solve");
        if ($problem->isReduced()) {
            $last = $problem->getIterations() + 3;
        } else {
            $last = count($problem->getTableaux()) + 2;
        }

        $objectiveString = MathUtil::convertObjectiveFunctionToString($problem->getObjectiveFunction(), true);
        $problemDescription = ["objective" => MathUtil::convertObjectiveFunctionToString($problem->getObjectiveFunction(), true, true),
            "slacks" => $this->getConstraintsAsStrings($problem->getConstraints(), true)];
        if ($req->has("json")) {
            $time = count($problem->getTableaux()) * 105;
            $time = round($time / 60);
           return new JsonResponse(["solutions" => $problem->getOptimum(), "time" => $time], ($problem->getFailed()) ? 500 : 200);
        }
        return View::make("pages.workings", ["objective" => $objectiveString,
                                              "constraints" => $this->getConstraintsAsStrings($problem->getConstraints()),
                                              "problem" => $problem, "lastStep" => $last,
                                              "tableaux" => (new ArrayTableauDrawer())->processMultiple($problem->getTableaux(), $problem),
                                              "description" => $problemDescription
        ]);

    }

    private function addStats(Problem $problem) {
        Statistic::incrementStatistic(Statistic::$CONSTRAINTS_CONVERTED, count($problem->getConstraints()));
        Statistic::incrementStatistic(Statistic::$PROBLEMS_PROCESSED);
        if ($problem->getFailed()) {
            Statistic::incrementStatistic(Statistic::$PROBLEMS_FAILED);
        }
        Statistic::incrementStatistic(Statistic::$TABLEAUX_CREATED, count($problem->getTableaux()));
        Statistic::incrementStatistic(Statistic::$ITERATIONS_PERFORMED, $problem->getIterations());
    }

    /**
     * @param SlackConstraint[] $slacks
     * @param bool $onlyOld Whether to include the old constraint
     * @return \string[]
     */
    private function getConstraintsAsStrings(array $slacks, $onlyOld = false) {
        $arr = [];
        foreach ($slacks as $slack) {
            $arr[] = MathUtil::convertSlackToString($slack, true, $onlyOld);
        }
        return $arr;
    }

}
