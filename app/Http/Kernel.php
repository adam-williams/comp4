<?php


namespace AdamWilliams\DMLT\Http;


use Exception;
use Symfony\Component\HttpFoundation\Request;


class Kernel extends \Illuminate\Foundation\Http\Kernel {

    /**
     * The application's HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        'AdamWilliams\DMLT\Http\Middleware\MaintenanceMiddleware',
        'Illuminate\Cookie\Middleware\EncryptCookies',
        'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
        'Illuminate\Session\Middleware\StartSession',
        'Illuminate\View\Middleware\ShareErrorsFromSession',
    ];
    protected $routeMiddleware = [
        'DevAuthMiddleware' => 'AdamWilliams\DMLT\Http\Middleware\DevAuthMiddleware',
        'csrf' => 'AdamWilliams\DMLT\Http\Middleware\CsrfTokenMiddleware',
        'guest' => 'AdamWilliams\DMLT\Http\Middleware\GuestMiddleware'
    ];

    /**
     * Handle an incoming HTTP request.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function handle($request) {
        try {
            return parent::handle($request);
        } catch (Exception $e) {
            $this->reportException($e);
            return $this->renderException($request, $e);
        }
    }

}
