\documentclass[12pt,a4paper]{article}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{float}
\usepackage{fullpage}
\usepackage{wrapfig}
\usepackage[none]{hyphenat}
\usepackage{pdfpages}

\usepackage{fancyhdr}

\usepackage[headsep=25pt]{geometry}
\pagestyle{fancy}
\fancyhf{}
\rhead{Adam Williams}
\lhead{Candidate 8393}
\rfoot{Page \thepage}

\begin{document}
\setcounter{secnumdepth}{3}
\title{COMP4 Project Manual\\Decision Maths Learning Tool}

\author{Adam Williams}
\date{}
\maketitle
\clearpage
\tableofcontents
\clearpage
\section{Introduction}
This manual has been produced for use with the ``Decision Maths Learning Tool" project for the AQA A2 COMP4 unit. Hereby referred to as ``DMLT", the project aims to provide intuitive, easy-to-understand workings to linear programming problems as are seen on the D2 unit of the EdExcel GCE Mathematics qualification.

The system is web-based and can be accessed from any modern browser. Desktop browsers known to work\footnote{Other browsers (including older versions of those above) have not been tested but may work.} with the system include:

\begin{itemize}
\item Firefox v23 and v33
\item Google Chrome/Chromium v38
\item Opera v26
\item Internet Explorer v10 and v11\footnote{Solving is functional but some styling doesn't work as intended leading to layout issues.}
\end{itemize}

Mobile browsers known to work with the system include:

\begin{itemize}
\item Android's stock browser v5
\item Firefox for Android v33
\item Chrome for Android v39
\end{itemize}

Versions of Internet Explorer prior to version 10 are unlikely to work with the system. Similarly, issues have been encountered with very old versions of Firefox (around v15) which prevent input of linear programming problems and updated versions of these browsers should be used to access the system instead.

On computers which form part of a network where control over software upgrades is managed by another entity and where the above requirements cannot be met with the available software, it might be fruitful to investigate whether browsers such as Chromium can be installed. This browser is of particular note because it doesn't require administrative privileges to be installed.

The rest of the manual will assume you are using a supported browser.

\section{Introduction to terms used in linear programming}
Throughout this manual, some terms will be used which are relevant to linear programming. These will be familiar to students and teachers who are confident with D1 or D2 (since both units cover the concept of linear programming to an extent) but for the sake of clarity they are defined below.

It's important to understand what a linear programming problem is before trying to comprehend any of the terminology involved. A linear programming or optimisation problem is one in which the goal is to maximise or minimise a function subject to a set of conditions. The function to be minimised or maximised is defined in terms of a set of variables which are added together and the conditions restrict the values of said variables.

\begin{itemize}
\item Decision variable - A variable that the solving the linear programming problem will find a value for. These variables are featured in the objective function and constraints.
\item Objective function - The function to be optimised by the Simplex algorithm.
\item Constraint - A condition that affects the possible values of the decision variables.
\end{itemize}

Consider the linear programming problem below:

{\addtolength{\leftskip}{5mm}
Maximise $P = 2x + 4y + 6z$

Subject to:
\begin{itemize}
	\item $2x + y \leq 5$
	\item $z \leq 10$
\end{itemize}
}

In the above problem, an objective function $P$ is defined which is to be maximised given that the sum of $2x + y$ is less than or equal to 5 and that the value of the decision variable $z$ is less than or equal to 10. It is implied that all decision and slack variables are greater than or equal to 0. This requires that the feasible region (which covers the best possible values for the objective function) is in the positive quadrant.

The solution to the problem above is:

\begin{itemize}
	\item $x = 0$
	\item $y = 5$
	\item $z = 10$
\end{itemize}

This results in a total value of 80 for the objective function.

\section{Accessing the system}
Since the code for the project is freely available, anyone can run their own instance of the system. Furthermore, anyone can modify the code to add, change or remove functionality to their needs.

The 'main' instance of DMLT is available at \texttt{https://dmlt.adamwilliams.eu} and hosted by Adam Williams. In situations where the system is being hosted locally, the URL to use will depend on a number of factors and the person responsible for hosting the system should be consulted for information on how to access the system.

Once authenticated or if the system is not in debug mode, the homepage should be displayed:

\begin{figure}[H]
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{hpfrag}
\caption{The top part of the homepage, which includes information on the system's core features.}
\end{figure}

On the homepage, the key functionality of the system is highlighted. At the top of the page is the navigation bar. This allows you to move from page to page within the application. The key items visible to all users are:

\begin{itemize}
\item Home - Returns you to the homepage where an overview of the system is presented.
\item Solver - Takes you to the form where a new linear programming problem can be submitted to be solved.
\item Problem generator - Allows users to generate problems with varying difficulty to solve.
\item About - Gives some background information on what the system is using to solve the linear programming problems along with information on problems it cannot solve.
\item Login - Takes you to the login form where you can sign-in to an existing account.
\item Register - Takes you to the registration form where you can sign-up. This allows problems to be saved and retrieved later.
\end{itemize}

All of the pages are served over HTTPS using TLS for security purposes. This simply means that all requests to the server and responses sent to the client are encrypted for security purposes.

\section{Installation}

For most users, no installation (other than a web browser) is required since the applications acts as a SaaS.

For more technical users who may wish to run their own instance, instructions are provided below:

To get this running, you'll want to:
\begin{itemize}
    \item Install composer (http://getcomposer.org)
    \item Run composer install to download the dependencies
    \item Use php artisan serve to start the development server
\end{itemize}

You'll need PHP 5.4+ (the application was developed using PHP 5.6) and the mcrypt and XSL extensions for this to work. The application also requires beanstalkd for queueing work.

If you're on a Debian/Ubuntu system, try sudo apt-get install php5-mcrypt php5-xsl.

To setup bourbon, run bourbon install in app/sass. You'll want to do the same for neat (neat install), too. These two commands require that the bourbon and neat gems are installed (respectively).

\section{Using mobile devices}

\begin{wrapfigure}{l}{200px}
\includegraphics[width=200px,keepaspectratio]{android}
\caption{Firefox Beta on a Nexus 5 Android device, showing the homepage of the system.}
\vspace{-30pt}
\end{wrapfigure}

The system is designed to work on all systems which can run a modern browser. This means the system will work on smartphones, tablets, IWBs etc. The page will automatically adapt to the device's width, collapsing the navigation bar and stacking items vertically where necessary. Tableau will use abbreviated headings to try and maximise use of screen width, however it may be beneficial to investigate whether the device can use landscape orientation when displaying workings.

You can use the 'input mode' button on the solver form to switch from sliders to numeric input boxes. The display of these inputs varies based on the browser in use, but a lot of devices will now adapt the keyboard in recognition of the fact that these are numeric values and as such only numbers should be entered into the textbox.

It may also be useful to use the alternative input method when the linear programming problem that you are trying to submit does not fit into the bounds of the sliders. For instance, the coefficient of a variable in the objective function is \textgreater 10 or \textless -10. These bounds were chosen based on common values for objective functions presented in problems in the textbook but may prove unsuitable. By using the text input mode, you can bypass these minimums and maximums.

The text input mode allows for decimals to be used for values. For instance, consider the objective function below:

\[ P = 2x - \frac{y}{2} + 7z \]

By using the text input mode, the $y$ coefficient can be entered as -0.5.



\begin{figure}[H]
\centerline{\includegraphics[width=200px,keepaspectratio]{solverandroid}}
\caption{The solver interface, using the alternative input system.}
\end{figure}
\iffalse
\section{Submitting a problem}
Use the ``Solve problem" link in the navigation bar to access the solver interface where you can specify the problem to be solved. Keep in mind that because the system uses Simplex to solve the linear programming problems, certain non-standard problems cannot be solved. See the ``About the algorithm" page for more information.

From the solver interface, information needs to be supplied to allow the application to effectively solve a problem. Firstly, you must tell the system how many decision variables are involved in the problem. For D1 problems, this is usually 2. For D2 problems this can be up to 4 (but in the interests of simplifying the user interface, the application is limited to a maximum of problems involving 3 variables). While you are supplying this and other information, the current problem preview is displayed at the top of the page:

\begin{figure}[H]
\centerline{\includegraphics[width=460px,keepaspectratio]{preview}}
\caption{The problem preview appears at the top of the page and sticks to the top of the page when scrolling down in supported browsers.}
\end{figure}

Two toggle buttons are available to change between 2 and 3 variable problem input mode. As explained in the previous section, the input mode can also be switched between slider and text mode.

Once the number of decision variables has been set, you can move on to the next step. If you wish to save some screen space you can use the 'eye' icon to collapse the sections you no longer need open.

To supply the objective function, consider the coefficients for each variable component that makes up the function. For example, if the objective function was $P = 2x + 3y + 4z$, the coefficient for $x$ would be 2 and the coefficients for $y$ and $z$ would be 3 and 4 respectively. Once you have this information, use the sliders to choose the coefficient for each variable. Slider values to the left of the middle are negative and the slider step is 1. The bounds for these sliders are from -10 to +10.

\begin{figure}[H]
\centerline{\includegraphics[width=460px,keepaspectratio]{objfunc_slider}}
\caption{The sliders are used to supply the objective function. When dragging the sliders the preview will be automatically updated.}
\end{figure}

The next step is to supply the constraints. These are added one at a time. Once you're finished with each constraint, the 'append' button is used to add the constraint to the problem. The bounds for the coefficient sliders match the bounds used when inputting the objective function. The slider used to specify the right-hand side has a minimum of 0 and a maximum of 200 (since all constraints must be non-negative).

\begin{figure}[H]
\centerline{\includegraphics[width=460px,keepaspectratio]{constraint}}
\caption{Constraints are added one at a time, using the same coefficient input system. As before, the preview automatically updates.}
\end{figure}

Finally, the problem is ready to be submitted. If you'd like to reduce the number of tableau used to solve the problem, tick the ``use reduced tableaux" checkbox before clicking the ``submit problem" button.
\fi
\section{Step by step guide}
The step by step guide to using the system is included below. This uses screenshots and annotations to explain common tasks that a user might wish to carry out when using the system.
\setlength{\footskip}{60pt}
\includepdf[pages={1,2,3,4,5,7,8,9},scale=.8,pagecommand={\pagestyle{fancy}}]{sbs.pdf}

\section{Common issues}

In some cases, invalid input and limitations of the Simplex algorithm can mean that errors are encountered. These are listed below along with explanations of their cause and how they can be mitigated.

Issues before the solving process has begun will be presented as a modal dialog and will prevent form submission. Issues during the solving process will cause all of the steps prior to the fault to be displayed along with an explanation of what the issue was and why it might have happened.

\begin{figure}[H]
\centerline{\includegraphics[width=229px,keepaspectratio]{error_example}}
\caption{An example of an error dialog which stopped a problem being submitted.}
\end{figure}

\begin{figure}[H]
\centerline{\includegraphics[width=346px,keepaspectratio]{generror}}
\caption{An example of an error encountered during solving.}
\end{figure}

\begin{figure}[H]
\centerline{\includegraphics[width=346px,keepaspectratio]{too_many}}
\caption{The system provides more details of the error and what caused it in the step where the error actually occurred. This particular error was caused by submitting a known 'cycling' linear programming problem that Simplex cannot solve.}
\end{figure}

\subsection{The problem must have at least one constraint.}
This error is displayed when you attempt to solve a problem without adding any constraints first. Simplex cannot solve such unbounded problems and if you consider the objective function $P = x + y + z$ without any constraints, the best values for all three decision variables will be $\infty$ in order to produce the highest possible value for the function $P$. To address this issue, add some constraints to the problem.

\subsection{The objective function must have at least one component.}
This error is displayed when a problem with no objective function is submitted. To resolve this, add at least one component to the objective function by selecting a coefficient which is $\neq 0$.

\subsection{In order for a pivot column to be able to be chosen, at least one component of the objective function should be positive.}
This error is encountered when a problem with an objective function of which all components have negative coefficients is submitted. For example, $P = -x - 2y - z$. Since all the constraints must be positive, it should be immediately clear that maximising such an objective function is identical to minimising $P = x + 2y + z$ and of course the best value for our decision variables in these circumstances is $-\infty$. To resolve this error, submit a problem with an objective function where at least one component is $> 0$.

\subsection{The maximum number of iterations (20) was exceeded so solving was stopped.}
To ensure system CPU time isn't wasted on huge problems and that webpages loading speed remains fast, a cap of 20 iterations is enforced. The majority of problems will be solved in far less than 20 iterations, so ensure the problem has been specified properly and is feasible if you're encountering this error. If you're sure the problem has been submitted properly, you can look at the last tableau the system got to when solving the problem and write down the equations for the objective function and constraints before resubmitting for an extra 20 iterations. Alternatively you can try solving the problem by hand.

\subsection{The pivot row is chosen from the smallest, positive theta value. In this instance, no positive theta values exist.}
This error is encountered when all of the calculated theta values are negative and as such there is no pivot row to choose to improve the value of the objective function. This is a limitation of the Simplex method taught in D2

\subsection{Debug mode authentication}
When debug mode is enabled, you may need to authenticate. If you're accessing the system from a whitelisted location, you can just click the ``Use IP bypass" button. Otherwise, you may need to use a 2 factor authentication code. 

\begin{figure}[H]
\includegraphics[width=1\textwidth]{2fa}
\caption{Authentication is required to access the system because potentially sensitive environment information is available when debug mode is enabled.}
\end{figure}

\end{document}
