# A2 COMP4 Project

This repository includes the source code for my A level computing project. I'm using laravel as the framework and as
such most of my code is located in app/DMLT/.

To get this running, you'll want to:

* Install composer (http://getcomposer.org)
* Run `composer install` to download the dependencies
* Use `php artisan serve` to start the development server

You'll need PHP5 and the mcrypt & XSL extensions for this to work. The application also requires beanstalkd for queueing work.

If you're on a Debian/Ubuntu system, try `sudo apt-get install php5-mcrypt php5-xsl`.

To setup bourbon, run `bourbon install` in app/sass. You'll want to do the same for neat, too.

## Env Vars

You need to set the following environmental variables. Create a file named '.env.{laravel-env-name}.php' in the root of
the project. It should return an array with the following keys and their respective values:

* PUSHOVER_APP_TOKEN - App token for Pushover
* PUSHOVER_USER_KEY - User key for Pushover

# Development server

Anything committed here on the master branch will be automatically deployed to the development server. This is available at:

https://dmlt.adamwilliams.eu

# Running the unit tests

Laravel ships with a PHPUnit configuration file. To run the tests, execute `phpunit` in the root of the project.

# Example problem

Throughout this project, the example linear programming problem that will be used is:

![image](https://imgrush.com/tUwVAs84LnRX.png)

# Code guidelines

* Treat PHP as a typed language
    * Use === comparisons
    * Typehint function parameters
    * Use doc-blocks to give fields & parameters types
* Use namespaces
* Use Americanized spellings. It's just easier, especially with CSS naming being the same.
* Test as much as possible PHP-wise