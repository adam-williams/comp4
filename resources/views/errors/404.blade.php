@extends("layouts.master")

@section("modules")
    @include("partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("content")
    <div class="generic--outer generic--outer-topmargin">
        <h1>404 Not found</h1>
        <p>The page you tried to visit does not exist. Try starting over from the <a href="/">homepage</a>.</p>
    </div>
@stop