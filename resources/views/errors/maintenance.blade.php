<!doctype html>
<html>
    <head>
        <title>Maintenance - DMLT</title>
        <link rel="stylesheet" href="/assets/css/styles.css">
    </head>
    <body class="maintenance typography--sans">
        <div class="hero">
            <div class="hero--inner">
                <h1>Sorry, this site is down for maintenance :(</h1>
                <h2>Your browser will <noscript>not</noscript> automatically refresh once the site is back up<noscript> because JavaScript is disabled</noscript>.</h2>
            </div>
        </div>
    @include("...partials.footer")
    <script id="modules" type="application/json">["Maintenance"]</script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.3/fastclick.min.js"></script>
    <script src="/assets/js/app.js"></script>
    </body>
</html>