@extends("layouts.master")

@section("modules")
    @include("partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("content")
    <div class="generic--outer generic--outer-topmargin">
        <h1>Authentication required</h1>
        <p>Since this application is in debug mode, authentication is required before the application can be accessed.</p>
        <h4>Use 2FA</h4>
        <p>
            If you have a two factor authentication code to hand, you can enter it <here class="x"> </here>
        </p>
        @include("partials.errors")
        {{ Form::open() }}
            {{ Form::text("code", "", ["class" => "forms--text", "placeholder" => "Code"]) }}
            {{ Form::submit("Submit", ["class" => "button button--primary"]) }}
        {{ Form::close() }}

        <h4>Use IP bypass</h4>
        <p>
            Some IP addresses have been whitelisted to use this application. If this is the case for
            you, click the button below.
        </p>
        {{ Form::open(["url" => "/2fa-ip-bypass"]) }}
            {{ Form::submit("Use IP Bypass", ["class" => "button button--primary"]) }}
        {{ Form::close() }}


    </div>
@stop