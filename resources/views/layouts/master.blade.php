<!doctype html>
<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>{{{ $title }}} - DMLT</title>
        <link rel="stylesheet" href="/assets/css/styles.css?v=1">
        <link rel="stylesheet" href="/assets/css/print.css" media="print">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/0.3.2/sweet-alert.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" >
        <meta name="referrer" content="origin-when-crossorigin">
    </head>
    <body class="typography--sans">

        <a href="#content" class="accessibility--skip">Skip to content</a>
        {{-- @include("partials.nav.admin") --}}
        @include("partials.nav.main")
        <main>
            <a id="content"></a>
            @yield("content")
        </main>
        @include("...partials.footer", ["footerLinks" => true])
        @yield("modules")
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.3/fastclick.min.js"></script>
        <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/2.0.0/handlebars.runtime.min.js"></script> -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/0.3.2/sweet-alert.min.js"></script>
        <script src="/assets/js/app.js"></script>
        @yield("additionalScripts")
    </body>
</html>
