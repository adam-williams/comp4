@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("content")
    <div class="generic--outer generic--outer-topmargin">
        <h1>2FA Code Generation</h1>
        <p>Please use the Google Authenticator application to scan the code below:</p>
        <img src="{{{ $url }}}">
        <p>The secret is: {{{ $secret }}}</p>
    </div>
@stop