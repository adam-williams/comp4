<footer class="footer">
    <div class="footer--inner">
        <p class="footer--copyright">Adam Williams</p>
        @if (isset($footerLinks))
            <ul class="footer--links">
                <li><a href="https://bitbucket.org/adam-williams/comp4">Get the code</a></li>
            </ul>
        @endif
    </div>
</footer>
