@if ($errors->any())
    <div class="alert alert--error">
        <i class="fa fa-warning"></i> <strong>We had trouble processing this form</strong>
        <ul>
            @foreach ($errors->all() as $msg)
                <li>{{{ $msg }}}</li>
            @endforeach
        </ul>
    </div>
@endif
