<nav class="navigation navigation--admin">
    <div class="navigation--inner">
        <div class="navigation--brand">
            <a href="/" class="navigation--brandlink"><i class="fa fa-cog"></i> Admin tasks</a>
        </div>
        <ul class="navigation--links navigation--links-secondary">
                <li><a href="/admin/queue/stats">Queue stats</a></li>
                <li><a href="/admin/queue/clear">Clear queue</a></li>
                <li><a href="/admin/deploy/stats">Deployment stats</a></li>
                <li><a href="/admin/deploy/trigger">Trigger manual deployment</a></li>
                <li><a href="/admin/users/manage">Manage users</a></li>
        </ul>
    </div>
</nav>