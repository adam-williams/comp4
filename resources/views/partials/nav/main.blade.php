<nav class="navigation" role="navigation">
    <div class="navigation--inner">
        <div class="navigation--brand-mobile">
            <a href="/" class="navigation--brandlink navigation--brandlink-mobile">DMLT</a>
        </div>
        <div class="navigation--brand">
            <a href="/" class="navigation--brandlink" role="menuitem">DMLT</a>
            <i class="navigation--menutoggle fa fa-navicon"></i>
        </div>
        <ul class="navigation--links">
            @foreach($nav as $navItem)
                <li @if ($navItem->isActive())class="active"@endif>
                    <a href="{{{ $navItem->getUrl() }}}" role="menuitem">{{{ $navItem->getTitle() }}}</a>
                </li>
            @endforeach
        </ul>
        <ul class="navigation--links navigation--links-secondary">
            @foreach($navSecondary as $navItem)
                <li @if ($navItem->isActive())class="active"@endif>
                    <a href="{{{ $navItem->getUrl() }}}" role="menuitem">{{{ $navItem->getTitle() }}}</a>
                </li>
            @endforeach
        </ul>
    </div>
</nav>