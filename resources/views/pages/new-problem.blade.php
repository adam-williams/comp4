@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation", "Solver"]])
@stop
@section("additionalScripts")
    <script src="/assets/js/rangeslider.min.js"></script>
@stop
@section("content")
    <div class="solver generic--outer generic--outer-topmargin">
        {{ Form::open() }}
        <h1>Solve new linear programming problem <span class="btns"><a class="button button--primary problem--swap-input"><i class="fa fa-sliders"></i> Input method</a></span></h1>
        <p>
            You can use the form below to submit a problem to be solved by the system. The objective function and constraints involved
            in the problem will appear in the preview below. By using the sliders, problems can be specified for the system to solve.
        </p>

        <h2>Problem preview @include("partials.vistog")</h2>
        <div class="solver--block solver--probprev">
            <div class="placeholder">
                <div class="math-wrapper"><span class="math">Maximize: </span><div class="objFuncPrevWrapper" data-text="false"></div></div>
                <div class="math-wrapper"><span class="math">Subject to: </span><div class="allConstraintsPrev" data-text="false"></div></div>
            </div>
        </div>

        <input type="hidden" name="json_data" id="jsonData">

        <h2>Decision variables @include("partials.vistog")</h2>
        <div class="solver--block">
            <p>
                Please choose an option below based on how many decision variables are in the problem. The Simplex algorithm
                can theoretically handle an unlimited number of decision variables but this application is limited to a
                maximum of three to make input more accessible. Problems with two decision variables are found in D1 and usually
                solved graphically, but you can use this tool to check answers.
            </p>
            <p>
                <a class="button button--light solver--default" data-value="2" data-element="numberDecisionVariables">2 variables</a>
                <a class="button button--primary button--pressed solver--default" data-value="3" data-element="numberDecisionVariables">3 variables</a>
                {{ Form::input("hidden", "numberDecisionVariables", 3, ["id" => "numberDecisionVariables"]) }}

            </p>
        </div>

        <h2>Objective function @include("partials.vistog")</h2>
        <div class="solver--block solver--objfunc-block">
            <p>
                The objective function is the function that the application will use in order to find optimal values for
                each decision variable. The coefficients of each component of the objective function can be specified below:
            </p>
            <div class="objFuncPrevWrapper math-wrapper"><span class="math">Preview: </span></div>
            <div class="solver--objective-slider-block" id="xObjSlider"><label for="variableCoefficient0">Coefficient for <span class="math math--math">x</span>:</label>
                {{ Form::input("range", "variableObjectiveCoefficient0", 0, ["id" => "variableCoefficient0", "max" => 10, "min" => -10]) }}
            </div>
            <div class="solver--objective-slider-block" id="yObjSlider"><label for="variableCoefficient1">Coefficient for <span class="math math--math">y</span>:</label>
                {{ Form::input("range", "variableObjectiveCoefficient1", 0, ["id" => "variableCoefficient1", "max" => 10, "min" => -10]) }}
            </div>
            <div class="solver--objective-slider-block solver--zobj" id="zObjSlider"><label for="variableCoefficient2">Coefficient for <span class="math math--math">z</span>:</label>
                {{ Form::input("range", "variableObjectiveCoefficient2", 0, ["id" => "variableCoefficient2", "max" => 10, "min" => -10]) }}
            </div>
        </div>

        <h2>Constraints @include("partials.vistog")</h2>
        <div class="solver--block solver--cons-block">
            <p>
                Constraints can be specified in the section below. As this application only solves standard linear programming problems, constraints must be in the form
                ax + by + (cz) &lt;= r, where r is a real, non-negative constant. This can be configured with the sliders below.
            </p>
            <div class="constraintPrevWrapper math-wrapper">
                <span class="math">Preview: </span>
            </div>

            <div class="solver--constraint-slider-block" id="xConSlider1"><label for="variableConstraintCoefficient0">Coefficient for <span class="math math--math">x</span>:</label>
                {{ Form::input("range", "variableConstraintCoefficient0", 0, ["id" => "variableConstraintCoefficient0", "max" => 10, "min" => -10]) }}
            </div>
            <div class="solver--constraint-slider-block" id="yConSlider1"><label for="variableConstraintCoefficient1">Coefficient for <span class="math math--math">y</span>:</label>
                {{ Form::input("range", "variableConstraintCoefficient1", 0, ["id" => "variableConstraintCoefficient1", "max" => 10, "min" => -10]) }}
            </div>
            <div class="solver--constraint-slider-block solver--zobj" id="zConSlider1"><label for="variableConstraintCoefficient2">Coefficient for <span class="math math--math">z</span>:</label>
                {{ Form::input("range", "variableConstraintCoefficient2", 0, ["id" => "variableConstraintCoefficient2", "max" => 10, "min" => -10]) }}
            </div>
            <div class="solver--constraint-slider-block" id="rhsConSlider1"><label for="variableConstraintCoefficient3">RHS<span class="math math--math"></span>:</label>
                {{ Form::input("range", "variableConstraintCoefficient3", 0, ["id" => "variableConstraintCoefficient3", "max" => 200, "min" => 0]) }}
            </div>
            <p><a class="button button--primary solver--add-constraint">Add constraint</a></p>
        </div>
        <hr>
        <p>Click the button below to send the problem to the server to be saved. The problem will be placed in a queue and processed as soon as possible.
        After the problem is submitted, the page will automatically update to show the solving progress.</p>

        <p>If you'd like to save the solution for later use, consider <a href="/auth/register">registering</a> or <a href="/auth/login">logging in</a>.</p>
        <p><a class="button button--primary solver--submit">Solve problem</a></p>
        <p><input type="checkbox" name="reduced" id="reduced"><label for="reduced">Use reduced tableaux</label> </p>
        <p><input type="checkbox" name="save" id="save"><label for="save">Save problem</label></p>
        {{ Form::close() }}
    </div>
@stop
