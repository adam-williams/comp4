@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("additionalScripts")
    <script src="/assets/js/rangeslider.min.js"></script>
@stop
@section("content")

    <div class="stats generic--outer generic--outer-topmargin">
        <h1>Stats</h1>
        <p>This page includes some fun stats relating to the system and the problems it has processed.</p>
        <ul>
            <li>20,000 lines of code</li>
            <li>{{{ $stats[\AdamWilliams\DMLT\Eloquent\Entities\Statistic::$PROBLEMS_PROCESSED] }}} problems processed</li>
            <li>{{{ $stats[\AdamWilliams\DMLT\Eloquent\Entities\Statistic::$PROBLEMS_PROCESSED] - $stats[\AdamWilliams\DMLT\Eloquent\Entities\Statistic::$PROBLEMS_FAILED] }}} problems solved</li>
            <li>{{{ $stats[\AdamWilliams\DMLT\Eloquent\Entities\Statistic::$PROBLEMS_FAILED] }}} problems which couldn't be solved</li>
            <li>{{{ $stats[\AdamWilliams\DMLT\Eloquent\Entities\Statistic::$TABLEAUX_CREATED] }}} tableaux created (would have taken around {{{ $time }}} to do manually*)</li>
            <li>{{{ $stats[\AdamWilliams\DMLT\Eloquent\Entities\Statistic::$ITERATIONS_PERFORMED] }}} iterations performed</li>
        </ul>

        <small>* Assuming each tableau takes about 105 seconds to draw.</small>
    </div>
@stop
