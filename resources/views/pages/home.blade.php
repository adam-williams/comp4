@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("content")
    <div class="hero" role="banner">
        <div class="hero--inner">
            <h1 class=" animated ">Automated step-by-step solutions for linear programming problems using Simplex</h1>

            <div class="features">
                <ul class="feature-bullets">
                    <li class="bullet three-col-bullet">
                        <div class="bullet-content">
                            <h2><i class="fa fa-star"></i> Fast</h2>

                            <p>All solving logic is offloaded onto the server to avoid slowing down your browser.
                                Typical problems are solved in under a second.</p>
                        </div>
                    </li>
                    <li class="bullet three-col-bullet">
                        <div class="bullet-content">
                            <h2><i class="fa fa-graduation-cap"></i> Educational</h2>

                            <p>Workings closely match the familiar format in the D2 textbook and include
                                easy-to-understand step-by-step explanations.</p>
                        </div>
                    </li>
                    <li class="bullet three-col-bullet">
                        <div class="bullet-content">
                            <h2><i class="fa fa-laptop"></i> Accessible</h2>

                            <p>This web application is fully responsive and should work with any modern browser on any
                                modern device.</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="call-to-action">

                <a class="button--primary button" href="/solve">Solve a new problem</a>

                <div class="secondary-links">
                    <a href="/explain">Learn about the algorithm</a>
                </div>
            </div>
        </div>
    </div>
    <div class="homepage--block homepage--block-no-bottom">
        <h1>What is linear programming?</h1>
        <div class="homepage--linintro">
            <p>A linear programming problem is the problem of maximizing or minimizing a linear function subject to a
                set of constraints to try and find the "best" values for a set of variables. A linear programming
                problem can theoretically consist of an infinite number of variables with an infinite number of
                constraints.</p>

            <p>Linear programming has many applications in business. For example, maximizing profit or minimizing cost.
                Linear programming has also been used for nutritional planning and a number of other mathematical
                problems can be formulated as linear programming problems. These include transportation and allocation
                problems along with zero-sum game theory.</p>

            <p>Simple linear programming problems with 2 decision variables can be solved using a 2-dimensional plot
                in conjunction with vertex testing or use of the objective line method ("ruler method") in order to
                see which vertex produces the best result for the objective function.
                More complex problems can potentially be solved by using the Simplex algorithm. This web application
                offers an interface for inputting linear programming problems and uses an implementation of the Simplex
                algorithm to try and solve them and present worked solutions.</p>
        </div>

        <figure id="example-graph">
            <img src="/assets/img/graph.svg">

            <p>Solving a 2-variable linear programming problem with the objective line method.</p>
        </figure>

    </div>

    <div class="homepage--block">
        <div class="about">
            <h1>About the project</h1>

            <p>
                Edexcel's GCE Mathematics includes the "Decision Mathematics 2" (D2) module. Within this module is the
                topic of further linear programming. In the D1 unit, limited linear programming (solved graphically) is
                included
                with up to four constraints and variables can be set in the exam.
            </p>

            <p>
                The creatively named 'Decision Maths Learning Tool' (DMLT) was my GCE Computing project to provide
                worked solutions
                and explanations on the topic of linear programming for use primarily by D2 students and teachers.

                I implemented the Simplex algorithm as taught in the course over the Summer of 2014 and worked on the
                frontend and documentation up until the Spring of 2015 whilst I was in Year 13.
            </p>

            <p>
                I've open-sourced this tool on <a href="https://bitbucket.org/adam-williams/comp4/src">Bitbucket</a> now
                that it has been marked and moderated by AQA. For those interested,
                it's built with TypeScript, PHP (using Laravel 5) and SASS. The project runs on a DigitalOcean instance,
                using nginx as the web server.
            </p>
        </div>
    </div>
@stop
