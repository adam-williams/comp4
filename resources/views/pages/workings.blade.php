@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation", "Workings"]])
@stop
@section("content")
    <div class="js-only">
        <div class="workings--iwb-nav">
          <a href="#" class="button button--light workings--iwb-nav-left"><i class="fa fa-arrow-left"></i> Previous</a>
          <a href="#" class="button button--light workings--iwb-nav-right">Next <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>

    <div class="workings generic--outer generic--outer-topmargin">

        <h1><span class="title">Workings</span>
            @if ($problem->getFinished())
                <span class="btns"><a class="button button--primary workings--iwb-toggle" data-iterations="{{{ $problem->getIterations() }}}"><i class="fa fa-expand"></i> IWB</a>
                <a class="button button--primary workings--print"><i class="fa fa-print"></i> Print</a> <a class="button button--primary workings--worksheet"><i class="fa fa-pencil"></i> Worksheet</a></span>
            @endif
        </h1>
        @if ($problem->getFinished())
            <p class="workings--result">Solving this problem was successful in {{{ $problem->getIterations() }}} {{{ $tableaux['iterations'] }}}.</p>
        @else
            <div class="alert alert--error workings--result">
                    <i class="fa fa-warning"></i> <strong>This problem could not be solved.</strong>
                    <p>
                       This could happen for a number of reasons. Either the Simplex algorithm is unable to solve the
                       problem, too many iterations were performed or a bug in the solving process exists.
                    </p>
                    <p>For full details, scroll to the bottom of the page.</p>
                </div>
        @endif

        <div class="worksheet-problem-description">
            <h3>Problem description</h3>
            <p>Please solve this linear programming problem by maximizing the following objective function:</p>
            <div class="math-wrapper">
                <span class="to-convert">{{{ $description['objective'] }}}</span>
            </div>
            <p>Subject to the constraints below:</p>
            <div class="math-wrapper"><ul>
                @foreach ($description['slacks'] as $constraint)
                    <li><span class="to-convert">{{{ $constraint }}}</span></li>
                @endforeach
            </ul></div>
        </div>

        <div class="step-block workings--initial-step" data-step-num="1" data-iteration="1">
            <h3><span class="step-id">Step 1 - </span>Convert constraints and objective function</h3>
            <p class="help-text">{{ trans("explanations.convert_constraints") }}</p>
            <div class="math-wrapper"><ul>
                @foreach ($constraints as $constraint)
                    <li><span class="to-convert">{{{ $constraint }}}</span></li>
                @endforeach
            </ul></div>
            <p>{{{ trans("explanations.convert_objective") }}}</p>
            <div class="math-wrapper obj-func"><span class="to-convert">{{{ $objective }}}</span></div>
        </div>

        @foreach ($tableaux['all'] as $tableau)
            <div class="step-block step-tableau" data-iteration="{{{ $tableau['iteration'] }}}" data-step-num="{{{ $tableau['step'] + 1 }}}">
                <h3 class="worksheet--heading">Tableau {{{ $tableau['step'] }}}</h3>
                <h3><span class="step-id">Step {{{ $tableau['step'] + 1 }}} &ndash; </span>{{{ $tableau['description'] }}}</h3>
                <p class="help-text">
                    {{ $tableau['prose'] }}
                </p>
                <table class="tableau" data-stage="{{{ $tableau['key'] }}}">
                  <tr>
                    <th class="tableau--bvlbl"><span class="tableau--condensed">BV</span><span>Basic variable</span></th>
                    @foreach ($tableau['headings'] as $heading)

                        @if ($heading['type'] == "theta")
                            <th><span class="tableau-symbol">&theta;</span><span class="tableau--thetalbl"> values</span></th>
                        @else
                            @if ($heading['type'] == "value")
                                <th class="tableau--vallbl"><span class="tableau--condensed">=</span><span>Value</span></th>
                            @else
                                <th class="@if ($heading['type'] == "variable") tableau-symbol @endif">{{{ $heading['value'] }}}</th>
                            @endif
                        @endif
                    @endforeach
                  </tr>
                  @foreach ($tableau['rows'] as $row)
                    <tr>
                        <td class="tableau-symbol @if ($row['justChanged']) tableau--justchanged @endif"><span>{{{ $row['identifier'] }}}</span></td>
                        @foreach ($row['cells'] as $cell)
                            <td class="{{{ $cell['classes'] }}}">{{ $cell['value'] }}</td>
                        @endforeach
                    </tr>
                  @endforeach
                </table>
                <br />
            </div>
        @endforeach
        @if ($problem->getFailed())
            <div class="step-block" data-step-num="{{{ $lastStep }}}" data-iteration="{{{ $problem->getIterations() }}}">
                <h3><i class="fa fa-warning"></i> Solving failed in the next step</h3>
                <h4>{{{ $problem->getFailMessage() }}}</h4>
                <p>Please understand that the Simplex algorithm taught in D2 cannot solve all linear programming problems.</p>
            </div>
        @else
            <div class="step-block solutions" data-step-num="{{{ $lastStep }}}" data-iteration="{{{ $problem->getIterations() }}}">
                <h3><span class="step-id">Step  {{{ $lastStep }}} - </span>State solutions</h3>
                <h3 class="worksheet--heading">Solutions</h3>
                <p>In an exam, all of the below values should be stated, including the slack variables and the value of the objective function.</p>
                @foreach($tableaux['optimum'] as $var => $value)
                    <div class="math-wrapper"><span class="math math--math">{{{ $var }}} = {{ $value }}</span></div>
                @endforeach
            </div>
        @endif
    </div>
@stop
