@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("content")
    <div class="algorithm generic--outer generic--outer-topmargin">

        <h1>Simplex</h1>

        <p>
            This web application is an implementation of George Dantzig's Simplex algorithm <i>as taught in D2</i> which
            solves Linear Programming problems. The entire concept of linear programming used throughout World War 2 in
            order to
            reduce costs to the armed forces. The Simplex algorithm was kept secret until 1947.
        </p>

        <p>
            The Simplex algorithm works by performing pivot operations to repeatedly improve the solution until
            (hopefully) the
            optimum is found.
        </p>

        <h2>General steps</h2>

        <p>
            For a linear programming problem in standard form (using &le; constraints), the Simplex algorithm can be
            applied as described below to
            find optimum values for the decision variables involved. You may want to use the
            <a href="/generator">problem generator</a> so you have a set of example workings to refer to along with
            the notes below.
        </p>

        <h3>Step 1 &ndash; Convert constraints and objective function</h3>

        <p class="help-text">{{ trans("explanations.convert_constraints") }}</p>

        <p>{{{ trans("explanations.convert_objective") }}}</p>

        <h3>Step 2 &ndash; Formulate the constraints and objective function into a tableau</h3>

        <p>Once the slack variables have been introduced, it's time to create our initial tableau.
            This is done by first considering each of the constraints in turn and using the coefficients in the equation
            to fill in the boxes.
            The value column should contain the RHS of the equation. Once the constraints have all been added, the
            transformed objective function needs to be added to the tableau.
            As before, we consider the values of the co-efficients involved in the equation except this time they should
            all be negative, and the value should be zero.</p>

        <h3>Step 3 &ndash; Identify pivot column</h3>

        <p>The next step is to scan across the values in the <strong>bottom row</strong> and look for the most negative
            value. This will determine which column is our pivot column.
            That is, increasing the variable represented by this column will result in the fastest improvements. If you
            recall the
            objective function before the transformation process this should make sense. We want the most negative
            because before the transformation this was the most positive and remember, we're looking to maximize the
            value of the objective function.</p>

        <h3>Step 4 &ndash; Calculate theta values</h3>

        <p>In order to be able to work out which pivot row to select, we need to work out what are known as <strong>theta
                values</strong>.
            These come from the numbers in the <strong>value column</strong> and the corresponding number in the same
            row in the <strong>pivot column</strong> we picked earlier.
            To work out the theta values, simply divide the number in the <strong>value column</strong> by the
            corresponding <strong>pivot column</strong> number. We don't need to work out a theta value for the
            objective function row.
        </p>

        <h3>Step 5 &ndash; Find pivot row from theta values</h3>

        <p>In addition to finding the pivot column, we also need to find the pivot row. We do this by looking for the
            <strong>smallest, positive theta value</strong> that we calculated in the previous step. The row that this
            is found in now becomes the pivot row.</p>

        <h3>Step 6 &ndash; Divide pivot row by pivot value and change row's basic variable reference</h3>

        <p>Now that we have a pivot row and column, we can see that there is a single pivot cell where the row and
            column overlap. Our aim is to transform the pivot row such that the pivot cell changes to have a value of
            1. We achieve this by dividing every number in the pivot row by the value of the pivot. Since a number
            divided by itself is 1, this will result in changing the pivot cell to contain the value 1.</p>

        <p><strong>Important:</strong> Once we've modified the pivot row, we need to change the basic variable cell for
            the pivot row to match the heading for the pivot column. On the worked solutions page, this is shown with a
            green box. This step is missed out by far too many students!</p>

        <p>We use the row operations column to communicate the division operation which has taken place. It's important
            this is used in an exam.</p>

        <h3>Step 7 &ndash; Eliminate basic variable from non-pivot rows</h3>

        <p>We're almost getting to the final step of the current iteration now. The last thing we have to do is
            eliminate the basic variable from the other rows in the tableau. We do this by adding or subtracting
            <strong>multiples of the pivot row</strong> to the other rows. Each cell in the row we're trying to
            eliminate the basic variable from has a multiple of the corresponding pivot row cell added or subtracted
            from it. The aim is to have all zeroes in the pivot
            column, except for in the pivot row (where we will have a 1, per step 6).</p>

        <p>Again, it's important to communicate the multiple of the pivot row you use in the row operations column to
            avoid losing marks.</p>

        <h3>Step 8 &ndash; Check to see if problem is finished</h3>

        <p>Some linear programming problems can be solved with just one iteration of the Simplex algorithm. To check to
            see if the problem is finished, look at the objective function row. If there are <strong>no negative
                values</strong> in this row, that's it! No more iterations are required.</p>

        <p>It's more common that multiple iterations will be required, especially in an exam situation (though more than
            2-3 iterations is unlikely due to the time constraints of an exam question). If negative values exist in the
            bottom row, jump to step 3 and perform the steps again.</p>

        <h3>Step 9 &ndash; Read the solutions from the tableau</h3>

        <p>Once you're done, you can figure out the optimum values for the decision variables, slack variables and the
            resultant objective function value by looking at the value column on the right-hand side. Other variables
            will have a value of zero if they're not in the tableau.</p>

        <h2>Related exam questions</h2>

        <p>Edexcel often ask for iterations of the Simplex algorithm to be performed and then ask further questions
            afterwards relating to the work that has been done.
            This section describes a few common questions and how they should be answered.</p>

        <h3>Write down the profit equation given by the tableau</h3>

        <p>Let's look at an example tableau from partway through the solving process:</p>

        <table class="tableau" data-stage="formulate_constraints">
            <tbody>
            <tr>
                <th class="tableau--bvlbl"><span class="tableau--condensed">BV</span><span>Basic variable</span></th>

                <th class="tableau-symbol">x</th>

                <th class="tableau-symbol">y</th>

                <th class="tableau-symbol">z</th>

                <th class="tableau-symbol">r</th>
                <th class="tableau--vallbl"><span class="tableau--condensed">=</span><span>Value</span></th>
            </tr>
            <tr>
                <td class="tableau-symbol"><span>r</span></td>
                <td>4</td>
                <td>6</td>
                <td>5</td>
                <td>1</td>
                <td>225</td>
            </tr>
            <tr>
                <td class="tableau-symbol"><span>P</span></td>
                <td>2</td>
                <td>-8</td>
                <td>-9</td>
                <td>3</td>
                <td>20</td>
            </tr>
            </tbody>
        </table>
        <p></p>

        <p>The profit equation for this tableau is obtained by writing down the numbers in the bottom row with the
            decision variables and the profit variable on the left-hand side:</p>

        <p><span class="math"><span class="math math--math">P</span> + 2<span class="math math--math">x</span> - 8<span
                        class="math math--math">y</span> - 9<span class="math math--math">z</span> + 3<span
                        class="math math--math">r</span> = 20</span></p>

        <p>The June 2014 (R) D2 paper asks this type of question as part of question 4, part (c).</p>

        <h3>Using the profit equation determine whether the tableau is optimal or not, justifying your answer</h3>

        <div class="alert alert--error workings--result">
            <i class="fa fa-warning"></i> <strong>Careful! This question is often answered incorrectly</strong>

            <p>
                It's not enough to simply point out one of the values in the objective function row being negative when
                answering this sort of question.
            </p>
        </div>

        <p>To properly answer this question, you need to state the profit equation (see above), e.g.:</p>

        <p><span class="math"><span class="math math--math">P</span> + 2<span
                        class="math math--math">x</span> - 8<span
                        class="math math--math">y</span> - 9<span class="math math--math">z</span> + 3<span
                        class="math math--math">r</span> = 20</span></p>

        <p>You then need to explain (for our equation above) that because the x coefficient is positive, we can increase the the value of the
            objective function by increasing the variable x. As a result, the tableau cannot be optimal.</p>

        <p>Of course, the tableau could be optimal - in which case the argument would be reversed.</p>

    </div>
@stop
