@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation", "Generator"]])
@stop
@section("content")
    <div class="generator generic--outer generic--outer-topmargin">
        <h1>Problem generator</h1>
        <p>
            if you'd like to practice solving linear programming problems, you can use this page to generate a problem to
            solve. Simply click a button below and the problem will be generated for you locally. It will then be sent
            to be solved so that you can view the answers once you've worked them out for yourself.
        </p>

        <div class="alert alert--warning">
            The D2 specification will only ever require that you solve
            problems with up to <strong>four variables</strong> and <strong>four constraints</strong>. Solving problems
            with greater than four variables may however be useful as a revision exercise to ensure you understand the algorithm.
        </div>

        <div class="generator-buttons">
            <a class="button button--primary" data-num="2">Easy (2 variables)</a> <a class="button button--primary" data-num="3">Medium (3 variables)</a>
            <a class="button button--primary" data-num="4">Hard (4 variables)</a> <a class="button button--primary" data-num="10">Hardest (10 variables)</a>
        </div>


        <div class="result">
            <h2>Generating problem...</h2>
            <div class="fail">
                <div class="alert alert--error">
                    <strong><i class="fa fa-warning"></i> Well, this is embarrassing</strong><br />
                    <p>It seems we may have accidentally generated an unsolvable problem. Sorry about that. You might like to try and generate another.</p>
                </div>
            </div>
            <div class="description">
                <p>Please solve this linear programming problem by maximizing the following objective function. <span class="first-line"></span></p>
                <div class="math-wrapper">
                    <span class="objective-function"></span>
                </div>
                <p>Subject to the constraints below:</p>
                <div class="math-wrapper"><ul></ul></div>
                <p>Done? <a href="#" class="reveal-answers">Show</a> the answers.</p>
            </div>
        </div>
        <div class="answers">
            <h2>Solutions</h2>
            <ul>

            </ul>
            {{ Form::open(["url" => "/solve"]) }}
                <input type="hidden" name="json_data" id="data">
                <input type="submit" class="button button--primary" value="I'm still stuck, show me the workings">
            {{ Form::close() }}
        </div>

    </div>
@stop
