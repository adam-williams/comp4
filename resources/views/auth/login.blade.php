@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("content")
    <div class="generic--outer generic--outer-topmargin">


        <div class="login--wrapper">
            <h1>Login</h1>
            @include("partials.errors")
            @if (isset($filtered))
            <p>
                <strong>You cannot access that page unless you login.</strong>
            </p>
            @endif
            {{ Form::open() }}
                <div class="login--block">
                    {{ Form::email("email", "", ["class" => "forms--text", "placeholder" => "Email"]) }}
                </div>
                <div class="login--block">
                    {{ Form::password("password", ["class" => "forms--text", "placeholder" => "Password"]) }}
                </div>
                <div class="login--block">
                    {{ Form::submit("Login", ["class" => "button button--primary"]) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
@stop