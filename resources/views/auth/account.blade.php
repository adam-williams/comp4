@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("content")
    <div class="account generic--outer-topmargin">
        <div class="account--settings">
            <h1>Account settings</h1>
            <p>This page allows you to change any settings associated with your account.</p>

            {{ Form::open() }}
                <div class="account--block">
                    <label for="email">Email:</label>
                    {{ Form::email("email", $user->email, ["class" => "forms--text", "id" => "email", "required" => true]) }}
                </div>

                <div class="account--block">
                    <label for="password">Password:</label>
                    {{ Form::email("password", "", ["class" => "forms--text", "id" => "password", "placeholder" => "Leave blank for current password"]) }}
                </div>

                <div class="account--block">
                    <label for="password_confirmation">Password (confirm):</label>
                    {{ Form::email("password_confirmation", "", ["class" => "forms--text", "id" => "password_confirmation", "placeholder" => "Leave blank for current password"]) }}
                </div>

                <div class="account--block">
                    <label for="password_confirmation" class="account--noblock"><abbr title="IWB mode enables sliders and other board-accessible input fields">IWB Mode</abbr>:</label>
                    {{ Form::checkbox("password_confirmation", 1, 1, ["class" => "account--noblock"]) }}
                </div>
            {{ Form::submit("Save settings", ["class" => "button button--primary"]) }}
            {{ Form::close() }}
        </div>
        <div class="account--history">
            <h1>Past problems</h1>
            <p>Previous problems which you have solved with this application will appear below.</p>
        </div>
    </div>
@stop