@extends("...layouts.master")

@section("modules")
    @include("...partials.ts-mod", ["modules" => ["Navigation"]])
@stop
@section("content")
    <div class="generic--outer generic--outer-topmargin">


        <div class="login--wrapper">
            <h1>Register</h1>
            @include("partials.errors")
            {{ Form::open() }}
                <div class="login--block">
                    {{ Form::text("name", "", ["class" => "forms--text", "placeholder" => "Name"]) }}
                </div>
                <div class="login--block">
                    {{ Form::email("email", "", ["class" => "forms--text", "placeholder" => "Email"]) }}
                </div>
                <div class="login--block">
                    {{ Form::password("password", ["class" => "forms--text", "placeholder" => "Password"]) }}
                </div>
                <div class="login--block">
                    {{ Form::password("password_confirmation", ["class" => "forms--text", "placeholder" => "Password (confirm)"]) }}
                </div>
                <div class="login--block">
                    {{ Form::submit("Register", ["class" => "button button--primary"]) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
@stop