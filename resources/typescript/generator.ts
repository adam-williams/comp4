module Generator {

    class Details {
        constraints: {};
        optimums: {};
        numVars: number;
        vars: string[];
        objective: {};
    }

    class Constraint {
        rhs: number;
    }

    var lettersThree = ["x", "y", "z"];

    function generateProblem(numVars) {
        var obj = getObjectiveFunction(numVars);
        console.log(obj);
        return getConstraintsAndOptimums(obj, true);
    }

    function getObjectiveFunction(numVars) {
        var objectiveFunction = {};
        if (numVars <= 3) {
            for (var i = 0; i < numVars; i++) {
                objectiveFunction[lettersThree[i]] = getRandomCoefficient();
            }
        } else {
            for (var x = 0; x < numVars; x++) {
                objectiveFunction[String.fromCharCode(97 + x)] = getRandomCoefficient();
            }
        }
        return objectiveFunction;
    }

    function getConstraintsAndOptimums(objectiveFunction, hard) {
        var details = new Details();
        var agreedValues = {};
        var runningTotal = 0;
        var constraints = [];
        details.numVars = Object.keys(objectiveFunction).length;
        details.vars = Object.keys(objectiveFunction);


        for (var key in objectiveFunction) {
            var optimum = getRandomVariableValue();
            agreedValues[key] = optimum;
            console.log("Picked optimum of " + optimum);
            runningTotal += (optimum * objectiveFunction[key]);
        }
        console.log("Running total " + runningTotal);

        if (Math.random() < 0.5) {
            console.log("One big constraint");
            // We'll just sum all the optimums, basically
            var total = 0;
            var constraint = new Constraint();
            for (var item in objectiveFunction) {
                var multiple = getRandomCoefficient();
                constraint[item] = multiple;
                total += multiple * agreedValues[key];
            }
            constraint.rhs = total;
            constraints.push(constraint);
        } else if (Math.random() < 0.5) {
            // This is the easiest way to make an absolute ton of constraints
            for (var x = 0; x < details.numVars; x++) {
                var total = 0;
                var constraint = new Constraint();
                var multiple = getRandomCoefficient();
                constraint[details.vars[x]] = multiple;
                total += multiple * agreedValues[details.vars[x]];
                constraint.rhs = total;
                constraints.push(constraint);
            }
        } else if (Math.random() < 0.5 || hard) {
            console.log("n constraints: one big, one localised");
            // first two variables get summed for one constraint
            // last variable has its own special constraint :D
            total = 0;
            constraint = new Constraint();
            var max = Object.keys(objectiveFunction).length - 1;
            for (var item in objectiveFunction) {
                max -= 1;
                if (max === 0) {
                    var randomCoefficient = getRandomCoefficient();
                    var finalConstraint = {rhs: randomCoefficient * agreedValues[key]};
                    finalConstraint[item] = randomCoefficient;
                } else {
                    multiple = getRandomCoefficient();
                    constraint[item] = multiple;
                    total += multiple * agreedValues[key];
                    constraint.rhs = total;
                }
            }
            constraints.push(constraint);
            constraints.push(finalConstraint);
        } else {
            console.log("All separate constraints");
            // each variable has its own separate constraint
            for (item in objectiveFunction) {
                multiple = getRandomCoefficient();
                constraint[key] = multiple;
                total = multiple * agreedValues[key];
                constraint = {item: multiple, rhs: total};
                constraints.push(constraint);
            }
        }
        console.log("Done!");

        details.constraints = constraints;
        details.optimums = agreedValues;
        details.objective = objectiveFunction;
        return details;

    }

    function getRandomCoefficient() {
        return Math.floor((Math.random() * 10) + 1);
    }

    function getRandomVariableValue() {
        var value = Math.floor((Math.random() * 30) + 1);
        // Make things a bit more exciting by using decimal optimums!
        if (Math.random() < 0.5) {
            value += 0.5;
        }
        if (Math.random() > 0.75 || Math.random() < 0.1) {
            value += 0.25;
        }
        return value;

    }

    function handleObjectiveFunctionHtml(data) {
        var objectiveHtml = "P = ";
        var first = true;
        for (var key in data.objective) {
            objectiveHtml += Util.MathRenderer.getLetterWithCoefficient(key, data.objective[key], first);
            first = false;
        }
        var $objective = $(".objective-function");
        $objective.html("");
        $objective.append(Util.MathRenderer.getMathFromString(objectiveHtml));
    }

    function handleConstraintHtml(data) {
        var $description = $(".description ul");
        $description.html("");
        console.log(data);
        for (var constraint in data.constraints) {
            var item = data.constraints[constraint];
            console.log("Got a constraint");
            console.log(item);
            var li = $("<li>");
            var first = true;
            var constraintHtml = "";
            for (var key in item) {
                console.log("key is " + key);
                if (key == "rhs") {
                    continue;
                }
                constraintHtml += Util.MathRenderer.getLetterWithCoefficient(key, item[key], first);
                first = false;
            }

            console.log(constraintHtml);
            li.append(Util.MathRenderer.getMathFromString(constraintHtml + " ≤ " + item.rhs));
            $description.append(li);
        }
    }

    export class ModuleLoader {
        static fire():void {
            $(function () {
                $(".result, .description, .fail, .answers").hide();
                $(".generator-buttons > .button").click(function() {
                    var num = $(this).data("num");
                    $(".fail").hide();
                    $(".answers").hide();
                    $(".result").show();
                    var data = generateProblem(num);
                    console.log(data);
                    handleObjectiveFunctionHtml(data);
                    handleConstraintHtml(data);
                    $(".result h2").text("Generated problem");
                    $(".description").show();
                    $("#data").val(JSON.stringify(data));
                    $.ajax({
                        url: "/solve",
                        type: "POST",
                        data: {"json_data": JSON.stringify(data), "json": true},
                        success: function(result) {
                            var $answerList = $(".answers ul");
                            $answerList.text("");
                            for (var item in result.solutions) {
                                var solution = result.solutions[item];
                                $answerList.append($("<li>").append(Util.MathRenderer.getMathFromString(solution.identifier + " = " + solution.value)));
                            }
                            var firstLine = $(".first-line");
                            firstLine.text("The problem should take about " + result.time + " minutes to solve.");
                        },
                        error: function(result) {
                            $(".fail").show();
                        }
                    })
                });
                $(".reveal-answers").click(function(event) {
                    event.preventDefault();
                    $(".answers").slideDown();
                })
            });
        }
    }
}
