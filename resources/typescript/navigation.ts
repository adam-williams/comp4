module Navigation {
    export class ResponsiveNav {
        _element: HTMLElement;
        _on = false;

        constructor (element: HTMLElement) {
            this._element = element;
            this.onClick = <any>this.onClick.bind(this);
            this._element.addEventListener('click', this.onClick);
        }

        onClick() {
            if (!this._on) {
                jQuery(this._element).parent().parent().parent().addClass("on-mobile");
            } else {
                jQuery(this._element).parent().parent().parent().removeClass("on-mobile");
            }

            this._on = !this._on;
        }
    }

    export class ModuleLoader {
        static fire():void {
            new Navigation.ResponsiveNav(<HTMLElement>document.querySelector(".navigation--menutoggle"));
        }
    }
}