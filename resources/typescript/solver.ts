/// <reference path="sa.d.ts" />

module Solver {
    var opts = {
        polyfill: true,
        // Callback function
        onSlide: function (position, value) {
            $('input[type="range"]').trigger("change");
        }
    };

    export class ItemComponent {
        _variable:string;
        _coefficient:number;

        constructor(variable:string, coefficient:number) {
            this._variable = variable;
            this._coefficient = coefficient;
        }

        getVariable() {
            return this._variable;
        }

        getCoefficient() {
            return this._coefficient;
        }
    }

    export class CurrentProblem {
        static getNumberVariables() {
            return ($("#numberDecisionVariables").val());
        }
    }

    // Class used to represent the current objective function
    export class FunctionStorage extends Util.Singleton {
        _components:ItemComponent[];
        _list:JQuery;

        constructor(list:JQuery) {
            super();
            this._list = list;
            this._components = [];
        }
    }

    // Class used to represent a constraint function
    export class ConstraintStorage extends Util.Singleton {
        constraints;

        constructor() {
            super();
            this.constraints = [];
        }
    }

    export class VariableDropdownFiller {
        static updateDropdowns(numVars:number, letters:boolean) {
            var lettersArray = ["x", "y", "z"];
            var finalArray = [];
            if (letters) {
                // The user has less than or equal to 3 variables
                // this means we can use the letters x,y,z to represent them
                finalArray = lettersArray.slice(0, numVars); // zero-indexed
            } else {
                // In this case we use "x{n}" style names
                for (var i = 1; i <= numVars; i++) {
                    finalArray.push("x" + (i));
                }
            }
            // so great, we now have an array
            Util.Dropdown.setOpts($(".variableBinding"), finalArray);
        }

        static getLetterForVariable(varNum:number, letters:boolean) {
            var lettersArray = ["x", "y", "z"];
            if (letters) {
                // don't give a value > 3
                return lettersArray[varNum - 1];
            } else {
                return "x";
            }
        }
    }

    export class InputWatcher {
        _slider:JQuery;
        _zSection:JQuery;

        constructor(slider:JQuery, zSectionSelector:JQuery) {
            this._slider = slider;
            this._zSection = zSectionSelector;
            this.registerEvents();
            this.changeHandler(undefined);
        }

        registerEvents() {
            //this._slider.bind("change", $.proxy(this.changeHandler, this));
            this._slider.bind("input", $.proxy(this.changeHandler, this));
        }

        static recalculateObjFunction() {
            var xSliderVal = $("#variableCoefficient0").val();
            var ySliderVal = $("#variableCoefficient1").val();
            var zSliderVal = $("#variableCoefficient2").val();
            var xFirst = true; var yFirst = false; var zFirst = false;
            if (xSliderVal == 0) {
                if (ySliderVal != 0) {
                    xFirst = false; yFirst = true; zFirst = false;
                } else {
                    xFirst = false; yFirst = false; zFirst = true;
                }
            }
            var xVal = Util.MathRenderer.getLetterWithCoefficient("x", xSliderVal, xFirst);
            var yVal = Util.MathRenderer.getLetterWithCoefficient("y", ySliderVal, yFirst);
            var zVal = Util.MathRenderer.getLetterWithCoefficient("z", zSliderVal, zFirst);
            var $objFuncPrevWrapper = $(".objFuncPrevWrapper");
            $objFuncPrevWrapper.html('<span class="math preview-text">Preview: </span>');
            //console.log("Preview: " + xVal + yVal + zVal);
            var completeString = xVal + yVal + zVal;
            if (completeString.length != 0) {
                $objFuncPrevWrapper.append(Util.MathRenderer.getMathFromString(completeString));
                //$objFuncPrevWrapper.slideDown();
            } else {
                //$objFuncPrevWrapper.slideUp();
            }
        }

        static reRenderConstraints() {
            var instance = ConstraintStorage.getInstance();
            var cs = "";
            // for (x in y) sounds good, right? But of course this is JavaScript and things are always painful.
            for (var i = 0; i < instance.constraints.length; ++i) {
                var xVal = instance.constraints[i]['x'];
                var yVal = instance.constraints[i]['y'];
                var zVal = instance.constraints[i]['z'];
                var rhs = instance.constraints[i]['rhs'];
                var xFirst = (xVal != 0);
                var yFirst = (!xFirst && yVal != 0);
                var zFirst = (!yFirst && !xFirst && zVal != 0);
                xVal = Util.MathRenderer.getLetterWithCoefficient("x", xVal, xFirst);
                yVal = Util.MathRenderer.getLetterWithCoefficient("y", yVal, yFirst);
                zVal = Util.MathRenderer.getLetterWithCoefficient("z", zVal, zFirst);
                cs += ", " + xVal + yVal + zVal + " ≤ " + rhs;
            }
            cs = cs.substr(2); //woo
            $(".allConstraintsPrev").html("").append(Util.MathRenderer.getMathFromString(cs));
        }

        static recalculateConstraints() {
            // let's repeat code because we can
            var xVal = $("#variableConstraintCoefficient0").val();
            var yVal = $("#variableConstraintCoefficient1").val();
            var zVal = $("#variableConstraintCoefficient2").val();
            var rhs = $("#variableConstraintCoefficient3").val();
            var xFirst = (xVal != 0);
            var yFirst = (!xFirst && yVal != 0);
            var zFirst = (!yFirst && !xFirst && zVal != 0);
            //console.log("x = " + xVal + ", y = " + yVal + ", z = " + zVal);
            //console.log("xFirst = " + xFirst + ", yFirst = " + yFirst + ", zFirst = " + zFirst);
            xVal = Util.MathRenderer.getLetterWithCoefficient("x", xVal, xFirst);
            yVal = Util.MathRenderer.getLetterWithCoefficient("y", yVal, yFirst);
            zVal = Util.MathRenderer.getLetterWithCoefficient("z", zVal, zFirst);

            var $previewWrapper = $(".solver--cons-block .constraintPrevWrapper");
            $previewWrapper.html('<span class="math preview-text">Preview: </span>');
            if (xFirst || yFirst || zFirst) {
                $previewWrapper.append(Util.MathRenderer.getMathFromString(xVal + yVal + zVal + " ≤ " + rhs));
                $previewWrapper.append($("<span>").text(" (unsaved, click append to save)").addClass("math"));
            }
        }

        changeHandler(ev:JQueryEventObject) {
            var count = this._slider.val();
            console.log("Got a count of " + count + " from " + this._slider.length);
            if (count > 3) {
                console.log("The backend does actually support an unlimited number of decision variables but the" +
                "UI unfortunately does not :(");
                count = 3;
            }
            var $variableCoefficient2 = $("#variableCoefficient2");
            var $variableConstraintCoefficient2 = $("#variableConstraintCoefficient2");
            if (count == 2) {
                $variableCoefficient2.data("old-val", $variableCoefficient2.val());
                this._zSection.slideUp(100);
                $variableCoefficient2.val("0");
                $variableConstraintCoefficient2.data("old-val", $variableConstraintCoefficient2.val());
                $variableConstraintCoefficient2.val("0");
                InputWatcher.recalculateConstraints();
                InputWatcher.recalculateObjFunction();
                ConstraintStorage.getInstance().constraints = [];
                InputWatcher.reRenderConstraints();
            } else {
                this._zSection.slideDown(100);
                $variableCoefficient2.val($variableCoefficient2.data("old-val"));
                $variableConstraintCoefficient2.val($variableConstraintCoefficient2.data("old-val"));
                InputWatcher.recalculateConstraints();
                InputWatcher.recalculateObjFunction();
            }
        }
    }

    export class ModuleLoader {
        static fire():void {
            $(function () {
                // register objective function component watcher

                var $numberDecisionVariables = $("#numberDecisionVariables");
                new InputWatcher($numberDecisionVariables, $(".solver--zobj"));
                ConstraintStorage.setInstance(new ConstraintStorage());

                $('input[type="range"]').rangeslider(opts);
                $(".solver--visibility-toggle").click(function (event) {

                    var targetEl = $(this).parent().next();
                    if (targetEl.hasClass("zoomOut")) {
                        targetEl.removeClass("zoomOut");
                        targetEl.removeClass("animated");
                        targetEl.toggleClass("animated").toggleClass("zoomIn").slideToggle();

                    } else {
                        targetEl.removeClass("zoomIn");
                        targetEl.removeClass("animated");
                        var nextStep = $(this).parent().next(".solver--block");
                        console.log("Next step length! " + nextStep.length);
                        if (nextStep.length !== 0) {
                            console.log(nextStep);
                            var offset = nextStep.offset().top;
                            targetEl.toggleClass("animated").toggleClass("zoomOut").slideToggle(400, function() {

                                console.log("Offset is" + offset);
                                $('html, body').animate({
                                    scrollTop: offset
                                }, 500);
                            });
                        } else {
                            targetEl.toggleClass("animated").toggleClass("zoomOut").slideToggle();
                        }
                    }
                    event.preventDefault();

                });
		        console.log("Registering event change listener on inputs");



		        $(".solver--constraint-slider-block input").on("input", function() {
                    InputWatcher.recalculateConstraints();
                });
                $(".solver--objective-slider-block input").on("input", function () {
                    InputWatcher.recalculateObjFunction();
                });

                $("input[type=range]").on("change", function (ev: JQueryEventObject) {
                    //MSIE
                    $(this).trigger("input");
                });

                $(".solver--default").click(function () {
                    var $el = $("#" + $(this).data("element"));
                    $el.val($(this).data("value"));
                    var defaultBtns = $(".solver--default");
                    defaultBtns.removeClass("button--pressed");
                    $(this).addClass("button--pressed");
                    defaultBtns.removeClass("button--primary");
                    defaultBtns.addClass("button--light");
                    $(this).removeClass("button--light");
                    $(this).addClass("button--primary");
                    $el.change();
                    $el.trigger("change");
                    $el.trigger("input");
                });

                $(".solver--submit").click(function() {
                    var $hiddenInput = $("#jsonData");
                    var data = {};
                    data["constraints"] = ConstraintStorage.getInstance().constraints;
                    var xSliderVal = $("#variableCoefficient0").val();
                    var ySliderVal = $("#variableCoefficient1").val();
                    var zSliderVal = $("#variableCoefficient2").val();
                    data["objective"] = {"x": xSliderVal, "y": ySliderVal, "z": zSliderVal};
                    data['numVars'] = $("#numberDecisionVariables").val();
                    data['vars'] = ["x", "y"];
                    if (data['numVars'] == 3) {
                        data['vars'] = ["x", "y", "z"];
                    }

                    if (data["constraints"].length == 0) {
                        sweetAlert("Oops...", "The problem must have at least one constraint.", "error");
                    } else {
                        if (xSliderVal == "0" && ySliderVal == "0" && zSliderVal == "0") {
                            sweetAlert("Oops...", "The objective function must have at least one component.", "error");
                        } else {
                            if (xSliderVal < 1 && ySliderVal <1 && zSliderVal < 1) {
                                sweetAlert("I can't solve that :/", "In order for a pivot column to be able to be chosen, at least one component of the objective function should be positive.", "warning");
                                return;
                            }
                            var dataStr = JSON.stringify(data);
                            if ($("#save").prop('checked')) {
                                sweetAlert({title: "Problem data", text: dataStr}, function() {
                                    $hiddenInput.val(JSON.stringify(data));
                                    $("form").submit();
                                });
                            } else {
                                $hiddenInput.val(JSON.stringify(data));
                                $("form").submit();
                            }
                        }
                    }
                });


                $(".solver--add-constraint").click(function() {
                    var instance = ConstraintStorage.getInstance();

                    var xVal = $("#variableConstraintCoefficient0").val();
                    var yVal = $("#variableConstraintCoefficient1").val();
                    var zVal = $("#variableConstraintCoefficient2").val();
                    var rhs = $("#variableConstraintCoefficient3").val();
                    $(".solver--cons-block input").val("0");
                    if (xVal == 0 && yVal == 0 && zVal == 0) {
                        sweetAlert("Oops...", "Constraints must have at least one component.", "error");
                        return;
                    }
                    instance.constraints.push({"x": xVal, "y": yVal, "z": zVal, "rhs": rhs});

                    console.log(instance.constraints);
                    InputWatcher.reRenderConstraints();
                    InputWatcher.recalculateConstraints();
                });

                $(".problem--swap-input").click(function() {
                    console.log("Input toggle click detected");
                    var $sliders = $("input[type=range]");
                    if ($sliders.length == 0) {
                        console.log("Right now we think that there are no sliders so the current state is text inputs.");
                        console.log("As a result, we're going to try and change the text boxes back into slider inputs.");
                        $("input[type=number]").each(function () {
                            var oldMin = parseInt($(this).data("old-min"));
                            var oldMax = parseInt($(this).data("old-max"));
                            $(this).attr("type", "range").attr("step", "1");
                            $(this).attr("min", oldMin);
                            $(this).attr("max", oldMax);
                        });
                    } else {
                        console.log("Right now there *are* sliders on the page. As a result, we're going to try and change them into text inputs");
                        $sliders.each(function () {
                            var oldMin = $(this).attr("min");
                            var oldMax = $(this).attr("max");
                            $(this).attr("type", "number").attr("step", "any").attr("min", "").attr("max", "");
                            $(this).data("old-min", oldMin).data("old-max", oldMax);
                        });
                    }
                });

            });
        }

    }
}
