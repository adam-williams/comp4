module Maintenance {
    export interface IMaintenanceCheckResponder {
        fire(result: boolean): void // result = true for out of maintenance, false for still in
    }

    export class MaintenanceCheckResponder implements IMaintenanceCheckResponder {
        fire(result: boolean) {
            if (result) {
                window.location.reload();
            }
        }
    }

    export class MaintenanceChecker {
        _callback: MaintenanceCheckResponder;
        _numChecks = 1;

        constructor(callback: MaintenanceCheckResponder) {
            this._callback = callback;

        }

        check() {
            var cb = this._callback;
            var nextDelay = MaintenanceChecker.getNextDelay(this._numChecks);
            jQuery.get("/204", function() {
                cb.fire(true);
            }).fail(function() {
                cb.fire(false);
            });
            this._numChecks = this._numChecks + 1;
            return nextDelay;
        }

        public static getNextDelay(numChecks: number) {
            return Math.floor(0.5 * Math.exp(numChecks));
        }
    }

    export class ModuleLoader {
        static fire():void {
            jQuery(function() {
                // doc ready
                var checker = new MaintenanceChecker(new MaintenanceCheckResponder());
                var count = 1;
                function doCheck() {
                    var nd = checker.check();
                    count = nd;
                    console.log(nd);

                    setTimeout(doCheck, nd * 1000);
                }
                function counter() {
                    var cs = Util.Pluralizer.pluralize("second", count);
                    jQuery(".footer--copyright").text("Checking status in " + count + " " + cs);
                    count-= 1;
                    setTimeout(counter, 1000);
                }
                doCheck();
                counter();
                jQuery(".footer--copyright").attr("title", "0.5 * e^i");
            });
        }
    }
}