module Util {
    export class Pluralizer {
        static pluralize(word: string, amount: number) {
            return (amount == 1 ? word : word + "s");
        }
    }

    export class Dropdown {
        static setOpts(select: JQuery, items: string[]) {
            console.log("We have " + select.attr("name"));
            select.children().remove();
            for (var i = 0; i < items.length; i++) {
                console.log("Adding " + items[i]);
                select.append($("<option></option>").text(items[i]).attr("value", i+1));
            }
        }

        static getCurrentlySelectedItem(select: JQuery) {
            var selected = select.find("option:selected");
            return selected.text();
        }
    }

    export class MathRenderer {
        static renderMath(variable: string, coefficient: number, subscript: number): JQuery {
            // In this function, we create pretty math text to represent a variable coefficient pair
            var cofSpan = $("<span></span>").addClass("math");
            var varSpan = $("<span></span>").addClass("math--math");
            if (coefficient != 1 && coefficient != -1) {
                // We will display the number in front of the variable here
                cofSpan.text(coefficient);
            }
            if (coefficient == -1) {
                cofSpan.text("-");
            }
            if (coefficient != 0) {
                varSpan.text(variable);
            }
            if (subscript !== undefined) {
                varSpan.append($("<sub></sub>").text(subscript));
            }
            return cofSpan.append(varSpan);
        }
        
        static getMathSpan() {
            return jQuery("<span>").addClass("math");
        }

        static getMathFromString(str: string): JQuery {
            var finalElement = MathRenderer.getMathSpan();
            for (var i = 0; i < str.length; i++) {
                if (str[i] >= 'A' && str[i] <= 'z') {
                    finalElement.append(MathRenderer.getMathSpan().addClass("math--math").text(str[i]));
                } else {
                    finalElement.append(str[i]);
                }
            }
            return finalElement;
        }

        static getLetterWithCoefficient(variable: string, coefficient: number, first: boolean): string {
            var retVal = "";
            if (coefficient > 0 && first) {
                retVal = coefficient + "" + variable;
            }
            if (coefficient > 0 && !first) {
                retVal = " + " + coefficient + variable;
            }
            if (coefficient == 1 && first) {
                retVal = variable;
            }
            if (coefficient == 1 && !first) {
                retVal = " + " + variable;
            }
            if (coefficient < 0 && first) {
                retVal = coefficient + variable;
            }
            if (coefficient < 0 && !first) {
                retVal = " - " + (coefficient*-1) + variable;
            }
            if (coefficient == -1 && first) {
                retVal = "-" + variable;
            }
            if (coefficient == -1 && !first) {
                retVal = " - " + variable;
            }
            return retVal;
        }
    }

    export class Singleton {
        static _instance;
        static getInstance() {
            return this._instance;
        }

        static setInstance(instance: Singleton) {
            console.log("set instance");
            this._instance = instance;
        }
    }
}
