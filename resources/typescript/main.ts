/// <reference path="util.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="navigation.ts" />
/// <reference path="maintenance.ts" />
/// <reference path="solver.ts" />
/// <reference path="generator.ts" />
/// <reference path="workings.ts" />

//new Navigation.ModuleLoader().fire();
import a = Navigation;
var data = JSON.parse(jQuery("#modules").html());
function handleModule(name: string) {
    switch(name) {
        // if only I could reflect...
        case "Navigation":
            Navigation.ModuleLoader.fire();
            break;
        case "Maintenance":
            Maintenance.ModuleLoader.fire();
            break;
        case "Solver":
            Solver.ModuleLoader.fire();
            break;
        case "Workings":
            Workings.ModuleLoader.fire();
            break;
        case "Generator":
            Generator.ModuleLoader.fire();
            break;
    }
}
for (var i = 0; i < data.length; i++) {
    handleModule(data[i]);
}
