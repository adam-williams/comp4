module Workings {

    export class ModuleLoader {
        static fire():void {

            $(function () {

                function shakeChangedRef() {
                    $(".tableau--justchanged").addClass("animated").addClass("shake")
                    $(".todo-tableau--theta-row").addClass("animated").addClass("shake").addClass("tableau--theta-row-bold");
                }

                function reanimatePivots() {
                    $(".todo-tableau--pivot-col").removeClass("todo-tableau--pivot-col").addClass("tableau--pivot-col");
                    $(".todo-tableau--pivot-row").removeClass("todo-tableau--pivot-row").addClass("tableau--pivot-row");
                    $(".todo-tableau--justchanged").removeClass("todo-tableau--justchanged").addClass("tableau--justchanged");
                    $(".todo-shake").addClass("animated").addClass("shake");

                    setTimeout(shakeChangedRef, 1000);
                }

                jQuery.fn.reverse = [].reverse;
                var isIwb = false;
                var isWs = 0;
                var iwbToggleBtn = $(".workings--iwb-toggle");
                var totalIterations = iwbToggleBtn.data("iterations");
                $(".to-convert").each(function () {
                    var text = $(this).text();
                    $(this).html("");
                    $(this).append(Util.MathRenderer.getMathFromString(text));
                });
                iwbToggleBtn.click(function() {
                    isIwb = !isIwb;
                    $("footer, .workings--result, nav, .step-block p").toggle();
                    var $workings = $(".workings");
                    $workings.toggleClass("workings--iwb");
                    var $step = $(".step-block");
                    if (isIwb) {
                        $(".tableau").css("width", "100%");
                        $workings.css("margin-top", "4.5em");
                        $(".step-block:not(.workings--initial-step)").hide();
                        var initialStep = $(".workings--initial-step");
                        initialStep.data("visible", "true");
                        $step.append($(".js-only > .workings--iwb-nav").clone());
                        $(".workings--initial-step .workings--iwb-nav .workings--iwb-nav-left").hide();
                        $step.last().find(".workings--iwb-nav-right").hide();
                        $(".workings").css("max-width", "80%");
                        $(".step-id").hide();
                        $(".workings h1 .title").text("Step 1, Iteration " + initialStep.data("iteration") + " of " + totalIterations);
                    } else {
                        $(".workings h1 .title").text("Workings");
                        $(".workings").css("max-width", "68em");
                        $(".tableau").css("width", "initial");
                        $step.show();
                        $(".step-id").show();
                        $(".step-block .workings--iwb-nav").remove();
                        $workings.css("margin-top", "1.5em");
                    }
                });
                $(".worksheet--heading, .worksheet-problem-description").hide();
                $(".workings--worksheet").click(function() {
                    if (isIwb) {
                        iwbToggleBtn.click();
                    }
                    isWs += 1;
                    if (isWs == 1) {
                        $("span.title").text("Simplex worksheet");
                        $(".workings--result").hide();
                        $("table.tableau tr:not(:first-child) td").attr("style", "color: transparent !important; -webkit-animation: none !important; animation: none !important; -moz-animation: none !important;"); $(".solutions .math").hide();
                        $(".workings--print").addClass("animated flash");
                    } else if (isWs == 2) {
                        $(".step-block p").hide();
                    } else if (isWs == 3) {
                        $(".workings--initial-step").hide();
                        $(".step-block h3").hide();
                        $(".worksheet--heading, .worksheet-problem-description").show();
                        $(".tableau--justchanged span").attr("style", "-webkit-animation: none !important; animation: none !important; -moz-animation: none !important;");
                    } else if (isWs == 4) {
                        $("table.tableau tr:not(:first-child) td").attr("style", ""); $(".solutions .math").show();
                        $(".workings--print").removeClass("animated flash");
                        $(".step-block p").show();
                        $(".step-block h3").show();
                        $(".worksheet--heading, .worksheet-problem-description").hide();
                        $(".workings--result").show();
                        $("span.title").text("Workings");
                        $(".workings--initial-step").show();
                        $(".tableau--justchanged span").attr("style", "");
                        isWs = 0;
                    }
                });
                $(".workings--print").click(function() {
                    if (isIwb) {
                        console.log("un-iwb'ing");
                        iwbToggleBtn.click();
                    }
                    window.print();
                });
                $(".workings").on("click", ".workings--iwb-nav-right, .workings--iwb-nav-left", function() {
                    var foundVisible = false;
                    var $list = $(".step-block");
                    if ($(this).hasClass("workings--iwb-nav-left")) {
                        $list = $list.reverse();
                    }
                    $list.each(function() {
                        console.log("Iterate");
                        var $el = $(this);
                        if (foundVisible) {
                            $el.find(".tableau--pivot-col").removeClass("tableau--pivot-col").addClass("todo-tableau--pivot-col");
                            $el.find(".tableau--pivot-row").removeClass("tableau--pivot-row").addClass("todo-tableau--pivot-row");
                            $el.find(".tableau--justchanged").removeClass("tableau--justchanged").addClass("todo-tableau--justchanged").removeClass("animated").removeClass("shake");
                            $el.show();
                            $(".workings h1 .title").text("Step " + $el.data("step-num") + ", Iteration " + $el.data("iteration") + " of " + totalIterations);
                            var stage = $el.find("table").data("stage");
                            if (stage == "check_problem") {
                                var $lastRow = $el.find("table tr").last();
                                $lastRow.find("td").slice(1,-1).each(function() {
                                    var val = $(this).text();
                                    if (val.substr(0,1) == "-") {
                                        $(this).attr("class", "").addClass("negative").addClass("todo-shake");
                                    } else {
                                        $(this).attr("class", "").addClass("positive").addClass("todo-shake");
                                    }
                                });
                            } else if (stage == "calculate_theta") {
                                $el.find(".tableau--theta-row,.todo-tableau--theta-row").attr("class", "").addClass("todo-tableau--theta-row");
                            }
                            setTimeout(reanimatePivots, 5);
                            $el.data("visible", "true");

                            return false;
                        }
                        if ($el.data("visible") == "true") {
                            $el.removeData("visible");
                            $el.hide();
                            console.log("Hidden");
                            console.log("Found visible");
                            foundVisible = true;
                        }
                    });
                });
            });
        }
    }
}
