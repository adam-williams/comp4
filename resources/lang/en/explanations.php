<?php

return array(

	"formulate_constraints" => "The first thing that needs to be done involves turning the objective function and
	                            constraints into our initial tableau. Once you've introduced the slack variables to
                                the 'lighter side' of the equation, you can use all of the information you have to create the initial
                                tableau. It should be relatively clear where each of the numbers come from but more information
                                is available on the algorithm explanation page.",

    "identify_pivot" => "The next thing we need to do is identify the pivot column. We do this by finding the most negative
                         value in the objective function row (denoted by the letter P in this application). By using this column
                         we'll see the fastest improvement. If we were solving this graphically, we'd be 'travelling'
                         along the :variable axis because its value of :value is the most negative.</p><p>If two columns
                         have the same value, the choice is arbitrary. This application just chooses the first.",

    "calculate_theta" => "The next step is to calculate theta values for each row apart from the objective function row.
                          These can be worked out by dividing the row's value by the corresponding item in the pivot column. This might not
                          immediately be clear so it might help to look at the first row as an example. The
                          theta value in that row is :theta and it was calculated by dividing :value by :pivot.",

    "find_pivot_row"   => "To find the pivot row, we need to look at the theta values we calculated in the previous step. Our pivot row
                           is the one which has the <strong>smallest positive</strong> theta value. In this case, the smallest positive value is :theta which
                           belongs to the ':row' row.</p><p>Now that the pivot row has been found, the pivot cell can easily be identified as the cell where the pivot row and pivot column cross.",
    "divide_pivot_change_bv" => "Now that we've identfied the pivot cell we want to change its value to 1. We'll then later use this cell to eliminate the variable from other rows.</p><p>By dividing the entire pivot row by the value of the pivot we'll ensure that the pivot cell's value changes to 1 (since any number divided by itself is one). You can see this in the row operations column below.",
    "eliminate_bv" => "In the final step of the iteration, it's time to eliminate the pivot column's variable from the other rows. By adding or subtracting multiples of the pivot row we can change the values in the other rows to zero. We leave the pivot row as it is and then figure out how many multiples of the pivot row is needed. This information is displayed in the row operations column below.",
    "check_problem" => "Once there are no more negative values in the objective function row, the problem is solved. If this is the case, you can now read off the values for the decision variables and the objective function from the 'Value' column. All other variables will have a value of zero.</p><p>If the problem isn't yet finished, it's now time to start the next iteration.",
    "convert_constraints" => "Before the solving process can begin, the constraints must be converted into equations by adding slack variables to the 'lighter' side. In this case, it's the left hand side since our constraints are all &le; constraints. The slack variables represent the gap between the LHS and the RHS and make the two sides equal to each other.",
    "convert_objective" => "The objective function must also be transformed such that the RHS is equal to zero. This enables the values to be added to the tableau.",
    "perform_iteration" => "In this view, the final tableau of each iteration is shown. For explanations of the steps happening in each iteration, use the normal display mode.",
    "reduced_theta" => "The mark schemes require that the original unmodified rows (before elimination takes place) are included in the answer. Be sure that if you decide to use the reduced tableaux this is kept in mind. In the exam, it might be wise to use an asterisk to show the smallest theta value. Do not use highlighters to mark anything in your answer."
);
