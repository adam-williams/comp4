<?php

use AdamWilliams\DMLT\Eloquent\Entities\Statistic;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        $this->command->info("Adding default stats entries...");
        DB::table("statistics")->delete();
        $toInsert = [];
        foreach (Statistic::$DEFINED_TYPES as $type) {
            $toInsert[] = ["key" => $type, "value" => 0, "created_at" => new DateTime(), "updated_at" => new DateTime()];
        }
        DB::table("statistics")->insert($toInsert);
    }

}
