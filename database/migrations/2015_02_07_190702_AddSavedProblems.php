<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSavedProblems extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('saved_problems', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer("solve_time_millis");
            $table->string("title")->nullable();
            $table->unsignedInteger("user_id")->nullable();
            $table->foreign("user_id")->references("id")->on("users");
            $table->string("guid")->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('saved_problems');
    }

}
