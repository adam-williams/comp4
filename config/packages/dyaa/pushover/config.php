<?php
/** Config options for pushover library. */
return array(
    'token' => getenv('PUSHOVER_APP_TOKEN'),
    'user_key' => getenv('PUSHOVER_USER_KEY'),
);